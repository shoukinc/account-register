package com.b2infosoft.gt;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class CreateSurfaceView extends SurfaceView implements Callback {
	private SurfaceHolder surfaceHolder;
	private SurfaceThread surfaceThread;
	
	public CreateSurfaceView(Context context) {
		super(context);
		surfaceHolder = getHolder();
		surfaceHolder.addCallback(this);
	}
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
		
	}
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		surfaceThread = new SurfaceThread(holder);
		surfaceThread.setRunning(true);
		surfaceThread.start();
	}
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		surfaceThread.setRunning(false);
		while (retry) {
			try{
				surfaceThread.join();
				retry=false;
			}catch(InterruptedException e){
				
			}
		}
	}
}

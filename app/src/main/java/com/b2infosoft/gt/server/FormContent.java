package com.b2infosoft.gt.server;

import android.content.Context;
import android.widget.EditText;
public class FormContent {	
	public String key;
	public String value;
	public String id;
	EditText edit;
	Context context;
	public FormContent(Context context)
	{
		this.context=context;
	}
	public void setEditTextId(String id){
		this.id=id;
	}
	public String getEditTextId(){
		return id;
	}
	public void setEditText(EditText edit){
		this.edit=edit;
	}
	public String getEditTextValue(){
		return edit.getText().toString();
	}
}

package com.b2infosoft.gt.rajesh.adapter;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;

public class ButtonAdapter extends BaseAdapter{
	String list[];
	Context context;
	int WIDHT;
	public ButtonAdapter(Context context,String list[],int WIDHT) {
		this.list=list;
		this.context=context;
		this.WIDHT=WIDHT;
	}	
	@Override
	public int getCount() {		
		return list.length;
	}
	@Override
	public Object getItem(int position) {
		return null;
	}
	@Override
	public long getItemId(int position) {
		return 0;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {				
		final Button button;
		if(convertView==null){
			button=new Button(context);
			button.setHeight(WIDHT);			
			button.setLayoutParams(new GridView.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
			//button.setPadding(8, 8, 8, 8);
			if(position%2==0)
				button.setBackgroundResource(R.color.fg_guard);
			else
				button.setBackgroundResource(R.color.fg_security);				
			button.setTextColor(Color.WHITE);
			button.setFocusable(false);			
			button.setFocusableInTouchMode(false);
			button.setClickable(false);
			button.setTypeface(Admin.getFontRoboto(context), Typeface.BOLD);
			button.setText(Admin.MONTHS[Integer.parseInt(list[position])-1]);			
			button.setId(Integer.parseInt(list[position])-1);
		}else{
			button=(Button)convertView;
		}
		return button;
	}
}

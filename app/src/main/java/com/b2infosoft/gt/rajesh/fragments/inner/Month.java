package com.b2infosoft.gt.rajesh.fragments.inner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.rajesh.adapter.ButtonAdapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Month extends Fragment{
	SharedPreferences settings;

	private TableLayout tableLayout;
	private TableLayout.LayoutParams layoutParams;
	private TableRow tr_head;
	private TextView textView;
	
	private JSONParser jsonParser=new JSONParser();
	private JSONArray jsonArray;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view=inflater.inflate(R.layout.fragments_inner_month_layout, container, false);
		settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);		
		getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));
	
		//String list[]={"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
			String list[]={"JAN"};
		//	String list[]={"JAN","FEB"};
		//	String list[]={"JAN","FEB","MAR","APR"};
		//	String list[]={"JAN","FEB","MAR","APR","MAY"};
		//	String list[]={"JAN","FEB","MAR","APR","MAY","JUN"};
		DisplayMetrics displayMetrics=new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		GridView gridView =(GridView)view.findViewById(R.id.GridView1);
		int WIDTH=displayMetrics.densityDpi;		
		WIDTH-=15;		
		WIDTH/=3;		
		gridView.setColumnWidth(WIDTH);		
		//gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
		gridView.setAdapter(new ButtonAdapter(getActivity(), list,WIDTH));
		
		/*
		tableLayout=(TableLayout)view.findViewById(R.id.log_month_table);
		layoutParams=new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.MATCH_PARENT);
		
		tr_head = new TableRow(getActivity());
		tr_head.setLayoutParams(layoutParams);
		tr_head.setPadding(0, 10, 0, 10);
		tr_head.setBackgroundResource(R.color.bg_log_row_even);
		
		textView=new TextView(getActivity());
		textView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		textView.setText("DATE");
		textView.setTypeface(Typeface.DEFAULT_BOLD);
		textView.setGravity(Gravity.CENTER_HORIZONTAL);		
		textView.setTypeface(Admin.getFontRoboto(getActivity()),Typeface.BOLD);
		tr_head.addView(textView);
		
		textView=new TextView(getActivity());
		textView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
		textView.setText("HOURS");
		textView.setTypeface(Typeface.DEFAULT_BOLD);
		textView.setGravity(Gravity.CENTER_HORIZONTAL);
		textView.setTypeface(Admin.getFontRoboto(getActivity()),Typeface.BOLD);
		tr_head.addView(textView);	
		tableLayout.addView(tr_head);
				
		logInfos();				
		*/
		return view;
	}
	
	private void logInfos(){
		if(!new Network(getActivity()).isConnectingToInternet()){
			showNoInternetConncetAlert();
		}else{
			new MonthInfo().execute(new String[]{
					settings.getString(Admin.USER_ID, "")
				});
		}
	}
	public void showNoInternetConncetAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");
 
        // Setting Dialog Message
        alertDialog.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");
 
        // On pressing Settings button
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	logInfos();
            }
        });
        // Showing Alert Message
        alertDialog.show();
	}	

	private class MonthInfo extends AsyncTask<String, String, String>{
		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> data=new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair(Admin.USER_ID, params[0]));
			
			JSONObject jsonObject =	jsonParser.makeHttpRequest(Admin.getUrlForLogMonthInfo(), "POST", data);
//			Log.d("Response", jsonObject.toString());
			
			try{
				int success=0;
				JSONArray jsonArray=null;
				if(jsonObject.has(Admin.TAG_SUCCESS))
					success=jsonObject.getInt(Admin.TAG_SUCCESS);
				if(success==1){					
					if(jsonObject.has(Admin.LOG))
						jsonArray =jsonObject.getJSONArray(Admin.LOG);					
					if(jsonArray!=null){						
						Month.this.jsonArray = jsonArray;
					}
				}
			}catch(JSONException e){
				
			}			
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);			
			if(Month.this.jsonArray!=null)
				updateLog(Month.this.jsonArray);
		}
	}
	private void updateLog(JSONArray jsonArray){
		for(int i=0;i<jsonArray.length();i++ ){
			try{ 
				JSONObject c = jsonArray.getJSONObject(i);				
				TableRow tr_row = new TableRow(getActivity());
				tr_row.setLayoutParams(layoutParams);									
				tr_row.setPadding(0, 10, 0, 10);
				if((i%2)==0)
					tr_row.setBackgroundResource(R.color.bg_log_row_odd);
				else
					tr_row.setBackgroundResource(R.color.bg_log_row_even);
				
				TextView	text=new TextView(getActivity());										
				text.setText(c.getString(Admin.LOG_MONTH));
				text.setGravity(Gravity.CENTER_HORIZONTAL);		
				text.setTypeface(Admin.getFontRoboto(getActivity()));																
				TextView text1=new TextView(getActivity());										
				text1.setText(c.getString(Admin.LOG_TOTAL_TIME));		
				text1.setGravity(Gravity.CENTER_HORIZONTAL);
				text1.setTypeface(Admin.getFontRoboto(getActivity()));																

				tr_row.addView(text);
				tr_row.addView(text1);
				tableLayout.addView(tr_row);				
			}catch(Exception e){
				
			}
							
		}
	}
}

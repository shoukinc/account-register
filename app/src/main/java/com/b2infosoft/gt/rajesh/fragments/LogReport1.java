package com.b2infosoft.gt.rajesh.fragments;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.b2infosoft.gt.MonthDetails;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.rajesh.adapter.ButtonAdapter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
public class LogReport1 extends Fragment{
	private SharedPreferences settings;
	public static final String TAG=LogReport1.class.getSimpleName();
	private final	JSONParser jsonParser = new JSONParser();
	
	private RelativeLayout main_layout;
	private GridView gridView;
	private TextView textYear;
	private ImageButton button_year_next,button_year_previous;
	private TreeMap< String, JSONArray> treeMap=new TreeMap<String, JSONArray>();
	private DisplayMetrics metrics=new DisplayMetrics();
	private int DEVICE_WIDHT;
	private int SELECT_YEAR;
	public static LogReport1 newInstance() {
		return new LogReport1();
	}
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			View view=inflater.inflate(R.layout.fragments_outer_log_report_layout_1, container, false);			
			getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
			getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));

			DEVICE_WIDHT=metrics.widthPixels;
			DEVICE_WIDHT-=15;
			DEVICE_WIDHT/=3;
			settings=getActivity().getSharedPreferences(Admin.MYSECRATE,0);
			main_layout=view.findViewById(R.id.log_content_view);
			textYear=view.findViewById(R.id.month_selected);
			textYear.setTypeface(Admin.getFontRoboto(getActivity()), Typeface.BOLD);
			gridView=new GridView(getActivity());			
			LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		 	gridView.setLayoutParams(new GridView.LayoutParams(params));
		 	gridView.setNumColumns(3);
		 	gridView.setHorizontalSpacing(5);
		 	gridView.setVerticalSpacing(5);
		 	gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {					
					Button button=(Button)arg1;			
					Bundle bundle = new Bundle();
					bundle.putInt(Admin.LOG_YEAR, SELECT_YEAR);
					bundle.putInt(Admin.LOG_MONTH, Admin.getMonthNumber(button.getText().toString()));
					bundle.putStringArray(Admin.LOG_MONTH_ARRAY, Admin.getStringArray(treeMap.get(String.valueOf(SELECT_YEAR))));
					Intent intent=new Intent(getActivity(),MonthDetails.class);
					intent.putExtras(bundle);
					startActivity(intent);				
				}
			});
		 	button_year_next=(ImageButton)view.findViewById(R.id.month_next);
		 	button_year_next.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					updateYear(String.valueOf(SELECT_YEAR+1));
				}
			});
		 	button_year_previous=(ImageButton)view.findViewById(R.id.month_previous);
		 	button_year_previous.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					updateYear(String.valueOf(SELECT_YEAR-1));
				}
			});
		 	button_year_previous=(ImageButton)view.findViewById(R.id.month_previous);
		 	
		 	new GetLogs().execute();
			return view;
	}
	private void updateYear(String year){
		SELECT_YEAR=Integer.parseInt(year);
		if(treeMap.containsKey(year)){
			gridView.setAdapter(new ButtonAdapter(getActivity(),Admin.getStringArray(treeMap.get(year)), DEVICE_WIDHT));
			if(main_layout.getChildCount()>0){
				main_layout.removeAllViews();
				main_layout.addView(gridView);
			}else{
				main_layout.addView(gridView);					
			}
			textYear.setText("YEAR "+year);
		}
		int YYYY=Integer.parseInt(year);
		if(treeMap.containsKey(String.valueOf(YYYY-1))){
			button_year_previous.setEnabled(true);
		}else{
			button_year_previous.setEnabled(false);			
		}		
		if(treeMap.containsKey(String.valueOf(YYYY+1))){
			button_year_next.setEnabled(true);
		}else{
			button_year_next.setEnabled(false);			
		}		
	}
	private class GetLogs extends AsyncTask<String, String, String>{
		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> pairs=new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
			final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForLogYearMonth(),"POST",pairs);						
			try{
				if(json.has(Admin.TAG_SUCCESS)){
					int success=json.getInt(Admin.TAG_SUCCESS);
					if(success==1){
						if(json.has(Admin.LOG)){
							JSONArray jsonArray = json.getJSONArray(Admin.LOG);						
							for(int i=0;i<jsonArray.length();i++){								
								JSONObject jsonObject=jsonArray.getJSONObject(i);
								if(jsonObject.has(Admin.LOG_YEAR)&&jsonObject.has(Admin.LOG_MONTH)){
									treeMap.put(jsonObject.getString(Admin.LOG_YEAR),jsonObject.getJSONArray(Admin.LOG_MONTH));									
								}
							}
						}
					}
				}
			}catch(Exception e){
				Log.d("Error", e.getMessage());
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(treeMap.size()>0){
				updateYear(treeMap.lastKey());
			}
		}
	}	
}
package com.b2infosoft.gt.rajesh.sign_2;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;

import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.ApiConnection;
import com.b2infosoft.gt.MainActivity;
import com.b2infosoft.gt.OnApiConnectListener;
import com.b2infosoft.gt.ProgressRequestBody;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.SlideMenu;
import com.b2infosoft.gt.Splash;
import com.b2infosoft.gt.rajesh.fragments.ParkingViolation;
import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.LruCache;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import static android.view.View.GONE;

public class CaptureSignature extends ActionBarActivity implements ProgressRequestBody.Listener {
    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel;
    public static String tempDir;
    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;
    View toast_view;
    private String uniqueId;
    private String url = "";
    Context context;
    //private EditText yourName;
    private Hashtable<String, String> data = new Hashtable<String, String>();
    private AlertDialog.Builder alertDialogBuilder;
    private Dialog alertDialog;
    private LinearLayout signature_view;
    private ImageView mImageGoodSign;
    private ProgressBar progress_bar;
    private ProgressBar progress_bar2;
    private FrameLayout mProgressFmLayout;
    private String NOTIFICATION_ID = "";
    private String USER_ID = "";
    private String COMPANY_ID = "";
    private TextView mPerCount;
    private String CHECK_STATUS = "";
    private int CANCEL = 0;


    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CANCEL = 0;
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getSupportActionBar().hide();
        setContentView(R.layout.signature);
        mImageGoodSign = (ImageView) findViewById(R.id.mGoodSignImg);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        progress_bar2 = (ProgressBar) findViewById(R.id.progress_bar2);
        mProgressFmLayout = (FrameLayout) findViewById(R.id.mProgressFmLayout);
        mPerCount = (TextView) findViewById(R.id.mPerCount);
        context = getApplication();
        toast_view = getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        tempDir = Environment.getExternalStorageDirectory() + "/"
                + getResources().getString(R.string.external_dir) + "/";


        alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.custom_dialog_box_2);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        String message = getIntent().getStringExtra("title");
        NOTIFICATION_ID = getIntent().getStringExtra("notification_id");
        if (!TextUtils.isEmpty(message)) {
            url = Admin.submitSignOnNotification();
            SharedPreferences settings = getSharedPreferences(Admin.MYSECRATE, 0);
            USER_ID = settings.getString(Admin.USER_ID, "");
            COMPANY_ID = settings.getString(Admin.COMPANY_ID, "");

        } else {

            url = getIntent().getStringExtra(Admin.URL);
            Log.d(Admin.URL, url);
            Bundle bundle = getIntent().getBundleExtra(Admin.DATA);


            if (bundle != null) {


                CHECK_STATUS = bundle.getString("check");//PUNCH OUT

                if (data.size() > 0) {
                    data.clear();
                }
                for (String key : bundle.keySet()) {
                    data.put(key, String.valueOf(bundle.get(key)));
                }
                Enumeration<String> keys = data.keys();
                while (keys.hasMoreElements()) {
                    String key = (String) keys.nextElement();
                    Log.d(key, data.get(key) + "");
                }
            }
        }

        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir(
                getResources().getString(R.string.external_dir),
                Context.MODE_PRIVATE);
        prepareDirectory();
        uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_"
                + Math.random();
        current = uniqueId + ".png";
        mypath = new File(directory, current);
        signature_view = (LinearLayout) findViewById(R.id.signature_view);
        mContent = (LinearLayout) findViewById(R.id.linearLayout);
        mSignature = new signature(this, null);
        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT);
        mClear = (Button) findViewById(R.id.clear);
        mGetSign = (Button) findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        mCancel = (Button) findViewById(R.id.cancel);
        mView = mContent;


        mClear.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mSignature.clear();
                mGetSign.setEnabled(false);
            }
        });


        mGetSign.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                boolean error = captureSignature();
                if (!error) {
                    mView.setDrawingCacheEnabled(true);
                    mSignature.save(mView);

                }
            }
        });

        mCancel.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
              //  Log.v("log_tag", "Panel Canceled");
               // Bundle b = new Bundle();
               // b.putString("status", "cancel");
               // Intent intent = new Intent();
               // intent.putExtras(b);
               // setResult(RESULT_OK, intent);
                CANCEL = 1;
                finish();
            }
        });

        if(ParkingViolation.IS_SIGN==0){
            new UploadImageWIthForm(null, mypath.getAbsolutePath()).execute();
        }

    }

    @Override
    protected void onDestroy() {
        Log.w("GetSignature", "onDestory");

        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        if (CANCEL != 1) {
            if (ParkingViolation.newInstanceWithCurrent() != null) {
                ParkingViolation.newInstanceWithCurrent().getActivity().onBackPressed();
            }
        }
        super.onDestroy();
    }

    LruCache<String, Bitmap> lruCache;

    @SuppressLint("NewApi")
    public File getDiskCacheDir(Context context, String uniqueName) {
        ContextWrapper wrapper = new ContextWrapper(context);
        final String cachePath = Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState())
                || !Environment.isExternalStorageRemovable() ? wrapper
                .getExternalCacheDir().getPath() : context.getCacheDir()
                .getPath();
        return new File(cachePath + File.separator + uniqueName);
    }

    private boolean captureSignature() {

        return false;
    }

    private String getTodaysDate() {

        final Calendar c = Calendar.getInstance();
        int todaysDate = (c.get(Calendar.YEAR) * 10000)
                + ((c.get(Calendar.MONTH) + 1) * 100)
                + (c.get(Calendar.DAY_OF_MONTH));
        Log.w("DATE:", String.valueOf(todaysDate));
        return (String.valueOf(todaysDate));

    }

    private String getCurrentTime() {

        final Calendar c = Calendar.getInstance();
        int currentTime = (c.get(Calendar.HOUR_OF_DAY) * 10000)
                + (c.get(Calendar.MINUTE) * 100) + (c.get(Calendar.SECOND));
        Log.w("TIME:", String.valueOf(currentTime));
        return (String.valueOf(currentTime));

    }

    private boolean prepareDirectory() {
        try {
            if (makedirs()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this,
                    "Could not initiate File System.. Is Sdcard mounted properly?",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private boolean makedirs() {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory()) {
            File[] files = tempdir.listFiles();
            for (File file : files) {
                if (!file.delete()) {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }

    private Handler mHandler = new Handler();

    @Override
    public void onProgress(final int progress, final ProgressBar progressBar, String timeupdate) {


        if (CaptureSignature.this != null) {
            mHandler.post(new Runnable() {
                public void run() {
                    progress_bar.setMax(100);
                    progress_bar.setProgress(progress);
                    mPerCount.setText(progress + "");
                    Log.d("perc main--", String.valueOf(progress));

                }
            });
        }

/*
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setProgress(progress);
                Log.d("perc main--",String.valueOf(progress));
            }
        });*/


    }

    @SuppressLint("NewApi")
    private class ExportBitmapToFile extends AsyncTask<Intent, Void, Boolean> {
        private Bitmap nBitmap;

        public ExportBitmapToFile(Bitmap bitmap) {
            nBitmap = bitmap;
        }

        @Override
        protected Boolean doInBackground(Intent... arg0) {
            uploadFile(nBitmap);
            return false;
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
        }
    }

    public final static String SERVER_ADDRESS = "http://192.168.0.52/rajesh/security_guard/";

    public static String getUrlForUploadSign() {
        return SERVER_ADDRESS.concat("UploadSign.php");
    }

    public void uploadImage(Bitmap image, String mypath) {
        String charset = "UTF-8";

        try {


       /*     MultipartUtility multipart = new MultipartUtility(url, charset);
            multipart.addHeaderField("User-Agent", "CodeJava");
			multipart.addHeaderField("Test-Header", "Header-Value");




            Enumeration<String> keys =	data.keys();
            while(keys.hasMoreElements()){
                String key = (String)keys.nextElement();
                if(data.get(key).toLowerCase().contains(".png")||data.get(key).toLowerCase().contains(".jpg")){
                    	Bitmap bitmapCamera = createScaledBitmap(data.get(key), 100, 100);
                    multipart.addFilePartFromImage(key, bitmapCamera);
                   // multipart.addFilePart(key,f);

                }else if(data.get(key).toLowerCase().contains(".mp3")){
                    File f=new File(data.get(key));
                    //multipart.addFilePartForVideo(key, f);
                    multipart.addFilePart(key,f);

                }else if(data.get(key).toLowerCase().contains(".mp4")||data.get(key).toLowerCase().contains(".mov")){
                    File f=new File(data.get(key));
                 //   multipart.addFilePartForVideo(key, f);
                    multipart.addFilePart(key,f);
                }else {
                    multipart.addFormField(key, data.get(key));

                }

            }
            File f=new File(mypath);
          	multipart.addFilePart_1("IMAGE", image);
			if(multipart.finish_1()==1){

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
                        mImageGoodSign.setVisibility(View.VISIBLE);
                        progress_bar.setVisibility(GONE);

                        zoom_in();

                        Thread th = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(5000);
                                } catch (InterruptedException e) {

                                }
                                Bundle b = new Bundle();
                                Intent intent = new Intent();
                                intent.putExtras(b);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });
                        th.start();
					}
				});

			}*/


            MultipartBuilder requestBody = new MultipartBuilder().type(MultipartBuilder.FORM);

            int isMedia = 0;
            Enumeration<String> keys = data.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                if (data.get(key).toLowerCase().contains(".png") || data.get(key).toLowerCase().contains(".jpg")) {
                    File f = new File(data.get(key));
                    requestBody.addFormDataPart("mime_type", "image/jpeg")
                            .addFormDataPart(key, f.getName(), RequestBody.create(MediaType.parse("image"), f));
                    isMedia = 1;
                } else if (data.get(key).toLowerCase().contains(".mp3")) {
                    File f = new File(data.get(key));
                    requestBody.addFormDataPart("mime_type", "audio/mp3")
                            .addFormDataPart(key, f.getName(), RequestBody.create(MediaType.parse("audio"), f));
                    isMedia = 2;

                } else if (data.get(key).toLowerCase().contains(".mp4") || data.get(key).toLowerCase().contains(".mov")) {
                    File f = new File(data.get(key));
                    requestBody.addFormDataPart("mime_type", "video/mp4")
                            .addFormDataPart(key, f.getName(), RequestBody.create(MediaType.parse("video"), f));
                    isMedia = 3;
                } else {
                    requestBody.addFormDataPart(key, data.get(key));
                }

            }

            if (!TextUtils.isEmpty(NOTIFICATION_ID)) {
                requestBody.addFormDataPart("notification_id", NOTIFICATION_ID);
                requestBody.addFormDataPart("user_id", USER_ID);
                requestBody.addFormDataPart("company_id", COMPANY_ID);
                File f = new File(mypath);
                requestBody.addFormDataPart("mime_type", "image/jpeg")
                        .addFormDataPart("sign", f.getName(), RequestBody.create(MediaType.parse("image"), f));
            } else {
                if(ParkingViolation.IS_SIGN==1||ParkingViolation.IS_SIGN==2) {
                    File f = new File(mypath);
                    requestBody.addFormDataPart("mime_type", "image/jpeg")
                            .addFormDataPart("IMAGE", f.getName(), RequestBody.create(MediaType.parse("image"), f));
                }
            }
            progress_bar.setTag(isMedia);
            progress_bar.setMax(100);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int media = (int) progress_bar.getTag();
                    if (media > 0) {
                        mProgressFmLayout.setVisibility(View.VISIBLE);
                        progress_bar2.setVisibility(View.GONE);
                    } else {
                        mProgressFmLayout.setVisibility(View.GONE);
                        progress_bar2.setVisibility(View.VISIBLE);
                    }
                }
            });

            ProgressRequestBody progressRequestBody = new ProgressRequestBody(requestBody.build(), CaptureSignature.this, progress_bar, "56546");

            Request request = new Request.Builder()
                    .url(url)
                    .post(progressRequestBody)
                    .build();

            new ApiConnection(new OnApiConnectListener() {
                @Override
                public void onSuccess(String jsonString) {
                    Log.i("uploadFile", "HTTP Response is : "
                            + jsonString + ": ");
                    try {
                        JSONObject jsonObject = new JSONObject(jsonString.toString());
                        if (jsonObject != null && jsonObject.has(Admin.TAG_SUCCESS)) {


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.i("uploadFile", "HTTP Response is : "
                                            + "failed" + ": ");

                                    if (TextUtils.isEmpty(CHECK_STATUS)) {
                                        CHECK_STATUS = "";
                                    }
                                    if (CHECK_STATUS.equalsIgnoreCase("PUNCH OUT")) {
                                        MySharedPreferences.setSharedPreferences(CaptureSignature.this, Admin.PUNCH_STATUS, "0" + "");
                                    }

                                    mImageGoodSign.setVisibility(View.VISIBLE);
                                    progress_bar2.setVisibility(View.GONE);
                                    mProgressFmLayout.setVisibility(View.GONE);

                                    zoom_in();

                                    Thread th = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Thread.sleep(5000);
                                            } catch (InterruptedException e) {

                                            }
                                            Bundle b = new Bundle();
                                            Intent intent = new Intent();
                                            intent.putExtras(b);
                                          //  setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    });
                                    th.start();

                                }
                            });
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (alertDialog != null) {
                                        alertDialog.dismiss();
                                    }
                                    signature_view.setVisibility(View.VISIBLE);
                                    mImageGoodSign.setVisibility(View.GONE);
                                    progress_bar2.setVisibility(View.GONE);
                                    mProgressFmLayout.setVisibility(View.GONE);
                                    Toast.makeText(CaptureSignature.this, "Form uploading failed!", Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                    } catch (JSONException e) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (alertDialog != null) {
                                    alertDialog.dismiss();
                                }
                                signature_view.setVisibility(View.VISIBLE);
                                mImageGoodSign.setVisibility(View.GONE);
                                progress_bar2.setVisibility(View.GONE);
                                mProgressFmLayout.setVisibility(View.GONE);
                                Toast.makeText(CaptureSignature.this, "Form uploading failed!", Toast.LENGTH_SHORT).show();

                            }
                        });

                    }
                }

                @Override
                public void onFailure(String message) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Log.i("uploadFile", "HTTP Response is : "
                                    + "failed" + ": ");
                            if (alertDialog != null) {
                                alertDialog.dismiss();
                            }
                            Toast.makeText(CaptureSignature.this, "Form uploading failed!", Toast.LENGTH_SHORT).show();
                            signature_view.setVisibility(View.VISIBLE);
                            mImageGoodSign.setVisibility(View.GONE);
                            progress_bar2.setVisibility(View.GONE);
                            mProgressFmLayout.setVisibility(View.GONE);

                        }
                    });

                }
            }).connect(request);

        } catch (Exception ex) {
            System.err.println(ex);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (alertDialog != null) {
                        alertDialog.dismiss();
                    }
                    Toast.makeText(CaptureSignature.this, "Form uploading failed!", Toast.LENGTH_SHORT).show();
                    signature_view.setVisibility(View.VISIBLE);
                    mImageGoodSign.setVisibility(View.GONE);
                    progress_bar2.setVisibility(View.GONE);
                    mProgressFmLayout.setVisibility(View.GONE);
                }
            });
        }
    }

    public int uploadFile(Bitmap bitmap) {
        String fileName = "file.png";
        int serverResponseCode = 0;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        {
            try {
                // open a URL connection to the Servlet
                URL url = new URL(getUrlForUploadSign());

                // Open a HTTP connection to the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type",
                        "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);
                dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + fileName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 25, bos);
                byte[] bitmapdata = bos.toByteArray();
                int lng = bitmapdata.length;
                dos.write(bitmapdata);

                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {
                            // messageText.setText(msg);
                            // messageText.setText("File Upload Completed");

                            Toast.makeText(CaptureSignature.this,
                                    "File Upload Complete.", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });
                }
                // close the streams //
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {
                ex.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        // messageText.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(CaptureSignature.this,
                                "MalformedURLException", Toast.LENGTH_SHORT)
                                .show();
                    }
                });

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {

                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        // messageText.setText("Got Exception : see logcat ");
                        Toast.makeText(CaptureSignature.this,
                                "Got Exception : see logcat ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Upload file ", "Exception : " + e.getMessage(), e);
            }
            return serverResponseCode;
        } // End else block
    }

    public class signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void save(View v) {
            //Log.v("log_tag", "Width: " + v.getWidth());
            //Log.v("log_tag", "Height: " + v.getHeight());
            if (mBitmap == null) {
                mBitmap = Bitmap.createBitmap(mContent.getWidth(),
                        mContent.getHeight(), Bitmap.Config.RGB_565);
            }

            Canvas canvas = new Canvas(mBitmap);
            try {
                //new ExportBitmapToFile(mBitmap).execute();

                FileOutputStream mFileOutStream = new FileOutputStream(mypath);
                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mFileOutStream.flush();
                mFileOutStream.close();
                //uploadImage(mBitmap,mypath.getAbsolutePath());

                mProgressFmLayout.setVisibility(VISIBLE);
                new UploadImageWIthForm(mBitmap, mypath.getAbsolutePath()).execute();


            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    class UploadImageWIthForm extends AsyncTask<String, Void, Integer> {
        private Bitmap bitmap;
        private String mypath;

        public UploadImageWIthForm(Bitmap bitmap, String mypath) {
            this.bitmap = bitmap;
            this.mypath = mypath;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            signature_view.setVisibility(GONE);
            mProgressFmLayout.setVisibility(View.VISIBLE);
            alertDialog.show();
        }

        @SuppressWarnings("deprecation")
        @Override
        protected Integer doInBackground(String... params) {

            uploadImage(bitmap, mypath);

            return 0;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

        }
    }


    // Function to get image path from ImagePicker
    public static String getImagePath(Intent data, Context context) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }


    public Bitmap createScaledBitmap(String pathName, int width, int height) {
        final BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, opt);
        opt.inSampleSize = calculateBmpSampleSize(opt, width, height);
        opt.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, opt);
    }

    public int calculateBmpSampleSize(BitmapFactory.Options opt, int width, int height) {
        final int outHeight = opt.outHeight;
        final int outWidth = opt.outWidth;
        int sampleSize = 1;
        if (outHeight > height || outWidth > width) {
            final int heightRatio = Math.round((float) outHeight / (float) height);
            final int widthRatio = Math.round((float) outWidth / (float) width);
            sampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return sampleSize;
    }

    public void zoom_in() {
        if (mImageGoodSign != null) {
            Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
            mImageGoodSign.startAnimation(animation1);
        }
    }

}

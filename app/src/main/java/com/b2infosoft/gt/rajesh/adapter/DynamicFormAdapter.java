package com.b2infosoft.gt.rajesh.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.rajesh.fragments.fragment_list_option;
import com.b2infosoft.gt.rajesh.modals.DynamicFormBean;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mahesh on 12/12/16.
 */

public class DynamicFormAdapter extends BaseAdapter {

    private fragment_list_option frg;
    private LayoutInflater inflater;
    private String BASE_UR = "http://guardtab.com/android_services/ios_icon/";
    private ArrayList<DynamicFormBean.FormsBean> formsList = new ArrayList<>();
    private View.OnClickListener clickListener;

    public DynamicFormAdapter(fragment_list_option frg, ArrayList<DynamicFormBean.FormsBean> formsList) {
        this.frg = frg;
        inflater = frg.getActivity().getLayoutInflater();
        this.formsList = formsList;
    }

    @Override
    public int getCount() {
        return formsList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View view1 = view;
        ViewHolder holder = null;
        if (view1 == null) {
            holder = new ViewHolder();
            view1 = inflater.inflate(R.layout.row_dynamic_form, null, false);
            holder.mImageView = (ImageView) view1.findViewById(R.id.mImageView);
            holder.mItemName = (TextView) view1.findViewById(R.id.mItemName);
            holder.mClickBtn = (LinearLayout) view1.findViewById(R.id.mClickBtn);
            view1.setTag(R.layout.row_dynamic_form, holder);
        } else {
            holder = (ViewHolder) view1.getTag(R.layout.row_dynamic_form);
        }
        holder.mClickBtn.setOnClickListener(clickListener);
        holder.mClickBtn.setTag(i);
        DynamicFormBean.FormsBean formsBean = formsList.get(i);
        try {
            holder.mItemName.setText(formsBean.getForm_name());
            String[] strings=formsBean.getForm_name().split(" ");
            if(strings.length==1){
                holder.mItemName.setText(formsBean.getForm_name()+"\n");
            }else{

            }
            Picasso.with(frg.getActivity())
                    .load(BASE_UR + formsBean.getIcon())
                    .placeholder(R.drawable.menu_schedule)
                    .into(holder.mImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return view1;
    }

    class ViewHolder {
        ImageView mImageView;
        TextView mItemName;
        LinearLayout mClickBtn;
    }

    public ArrayList<DynamicFormBean.FormsBean> getFormsList() {
        return formsList;
    }

    public void setFormsList(ArrayList<DynamicFormBean.FormsBean> formsList) {
        this.formsList = formsList;
    }

    public View.OnClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }
}

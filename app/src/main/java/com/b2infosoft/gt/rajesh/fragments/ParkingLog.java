package com.b2infosoft.gt.rajesh.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.server.JSONParser;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.widget.Toast;

public class ParkingLog extends Fragment{
	private static SharedPreferences settings;	
	public static final String TAG=ParkingLog.class.getSimpleName();
	JSONParser jsonParser = new JSONParser();
	public static ParkingLog newInstance() {
		return new ParkingLog();
	}
	View toast_view;
	Button button_in,button_out;
	EditText vehical_number;
	@SuppressLint("NewApi") @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			View view=inflater.inflate(R.layout.fragments_outer_parking_log_layout, container, false);
			// initialization
			getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));
			toast_view=inflater.inflate(R.layout.custom_toast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
			settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
			vehical_number=(EditText)view.findViewById(R.id.parking_log_vehicle_number);
			button_in=(Button)view.findViewById(R.id.parking_log_vehicle_in);
			button_in.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {					
					if(!isEmpty(vehical_number)){						
						new ParkingLogUpdate().execute(
								new String[] {
										settings.getString(Admin.USER_ID, ""),
										MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
										"1",
										vehical_number.getText().toString().trim()
									}								
								);
					}
				}
			});			
			button_out=(Button)view.findViewById(R.id.parking_log_vehicle_out);
			button_out.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					if(!isEmpty(vehical_number)){
						new ParkingLogUpdate().execute(
								new String[] {
										settings.getString(Admin.USER_ID, ""),
										MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
										"2",
										vehical_number.getText().toString().trim()
									}
								);
					}
				}
			});
			return view;
	}
	public class ParkingLogUpdate extends AsyncTask<String, Void, Integer>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected Integer doInBackground(String... args) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(Admin.USER_ID, args[0]));
			params.add(new BasicNameValuePair(Admin.CLIENT_ID, args[1]));
			params.add(new BasicNameValuePair(Admin.STATUS, args[2]));
			params.add(new BasicNameValuePair(Admin.VEHICLE_NUMBER, args[3]));			
			// getting JSON Object
			// Note that create product url accepts POST method			
			JSONObject json = jsonParser.makeHttpRequest(
					Admin.getUrlForParkingLog(), "POST", params);
			// check log cat from response
			Log.d("Response Holi", json.toString());
			// check for success tag
			try {
				int success = json.getInt(Admin.TAG_SUCCESS);
				if (success == 1) {
					// successfully created product
					// closing this screen
					if(json.has("action")) {

					}
					if (json.has("message")) {

					}
					return 1;
				} else {
					return 0;
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.d("My Exception", e.toString());
			}
			return 0;
		}
		@Override
		protected void onPostExecute(Integer result) {
			if(result==1){
				Toast toast =new Toast(getActivity().getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
				toast.setView(toast_view);
				toast.show();
			}
		}
	}	
	public boolean isEmpty(EditText text){
		String str=text.getText().toString().trim();
		if(TextUtils.isEmpty(str)){
			text.setError("Please Fill Vehicle Number");
			return true;
		}
		return false;
	}
}

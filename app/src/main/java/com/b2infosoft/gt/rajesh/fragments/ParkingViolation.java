package com.b2infosoft.gt.rajesh.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TreeSet;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.custom.FileUtils;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.rajesh.sign_2.CaptureSignature;
import com.jmolsmobile.landscapevideocapture.VideoCaptureActivity;
import com.jmolsmobile.landscapevideocapture.configuration.CaptureConfiguration;
import com.jmolsmobile.landscapevideocapture.configuration.PredefinedCaptureConfigurations;

import de.ailis.pherialize.MixedArray;
import de.ailis.pherialize.Pherialize;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import static android.R.attr.type;

public class ParkingViolation extends Fragment implements View.OnClickListener {
    public static ParkingViolation newInstance() {
        return new ParkingViolation();
    } public static ParkingViolation newInstanceWithCurrent() {
        return violation;
    }

    private SharedPreferences settings;
    private static ParkingViolation violation;
    private ProgressDialog progressDialog;
    public static final String TAG = ParkingViolation.class.getSimpleName();
    private final JSONParser jsonParser = new JSONParser();
    private RelativeLayout mainLayout;
    TextView textView;
    private Button button;
    private ScrollView scrollView;
    private LinearLayout linearLayout;
    View toast_view;
    //	private Hashtable<String, Hashtable<String,Object>> dynamic_form_data	=	new Hashtable<String, Hashtable<String,Object>>();
//	private Hashtable<String, Hashtable<String,Object[]>> dynamic_form_data	=	new Hashtable<String, Hashtable<String,Object[]>>();
    private Hashtable<String, Hashtable<String, Object[]>> dynamic_form_data_left = new Hashtable<String, Hashtable<String, Object[]>>();
    private Hashtable<String, Hashtable<String, Object[]>> dynamic_form_data_right = new Hashtable<String, Hashtable<String, Object[]>>();
    private String imagepath = "";
    private String audiopath = "";
    private String videopath = "";

    private final int RESULT_LOAD_IMAGE = 9090;
    private final int CAMERA_REQUEST = 1888;
    private Uri mCapturedImageURI = null;
    private ImageView mGlobalImage;
    private TextView mGlobalTextView;
    private RelativeLayout mTopLayoutAudio;
    private String strAudioUri;
    private final static int RQS_OPEN_AUDIO_MP3 = 11111;
    public static int VIDEO_REQUEST = 1800;
    private String videoPath = "";
    private MediaPlayer mPlayer = null;
    private RecordAudio recordAudio = null;

    private List<NameValuePair> params_main = new ArrayList<NameValuePair>();


    private final String KEY_STATUSMESSAGE    = "com.jmolsmobile.statusmessage";
    private final String KEY_ADVANCEDSETTINGS = "com.jmolsmobile.advancedsettings";
    private final String KEY_FILENAME         = "com.jmolsmobile.outputfilename";

    private final String[] RESOLUTION_NAMES = new String[]{"1080p", "720p", "480p"};
    private final String[] QUALITY_NAMES    = new String[]{"high", "medium", "low"};

    private String statusMessage = null;
    private String filename      = null;
public static int IS_SIGN=2;// 1 mean sign , 2 mean sign not , 0 mean logout







    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        IS_SIGN=0;
        this.violation=this;
        View view = inflater.inflate(R.layout.fragments_outer_parking_violation_layout, container, false);
        toast_view = inflater.inflate(R.layout.custom_toast, (ViewGroup) view.findViewById(R.id.custom_toast_layout));

        if (savedInstanceState != null) {
            statusMessage = savedInstanceState.getString(KEY_STATUSMESSAGE);
            filename = savedInstanceState.getString(KEY_FILENAME);

        }

        settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
        getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));

        final Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
        mainLayout = (RelativeLayout) view.findViewById(R.id.parking_form_layout);
        scrollView = new ScrollView(getActivity());
        scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        linearLayout = new LinearLayout(getActivity());
        linearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        textView = new TextView(getActivity());

       // textView.setText("Parking Violation".toUpperCase(Locale.getDefault()));
        textView.setText(fragment_list_option.TITLE.toUpperCase(Locale.getDefault()));
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setTypeface(Admin.getFontRoboto(getActivity()));
        textView.setVisibility(View.VISIBLE);
        LayoutParams textparams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        textparams.bottomMargin = 25;
        textparams.topMargin = 15;
        textparams.leftMargin = 0;
        textparams.rightMargin = 0;
        textView.setLayoutParams(textparams);
        linearLayout.addView(textView);
        button = new Button(getActivity());
        LayoutParams buttonparams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        buttonparams.bottomMargin = 25;
        buttonparams.topMargin = 15;
        buttonparams.leftMargin = 25;
        buttonparams.rightMargin = 25;
        button.setLayoutParams(buttonparams);
        button.setPadding(10, 10, 10, 10);
        button.setText("Submit");
        button.setTypeface(Admin.getFontRoboto(getActivity()));
        button.setBackgroundResource(R.drawable.button_background);
        button.setTextColor(Color.parseColor("#ffffff"));
        button.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View v) {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair(Admin.COMPANY_ID, settings.getString(Admin.COMPANY_ID, "")));
                params.add(new BasicNameValuePair(Admin.CLIENT_ID, settings.getString(Admin.CLIENT_ID, "")));
                params.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
                if (fragment_list_option.FORM_TYPE == 1) {
                    params.add(new BasicNameValuePair(Admin.FORM_ID, String.valueOf(fragment_list_option.FORM_ID)));
                }
                int i = 0;
                int j = 0;
                boolean validation = false;
                if (dynamic_form_data_left.size() > 0) {
                    Enumeration<String> enum_key = dynamic_form_data_left.keys();
                    while (enum_key.hasMoreElements()) {
                        String string = (String) enum_key.nextElement();
//							Hashtable<String, Object> dynamic_form_field=(Hashtable<String, Object>)dynamic_form_data.get(string);
                        Hashtable<String, Object[]> dynamic_form_field = (Hashtable<String, Object[]>) dynamic_form_data_left.get(string);
                        if (dynamic_form_field.size() > 0) {
                            Enumeration<String> enum_key_field = dynamic_form_field.keys();
                            while (enum_key_field.hasMoreElements()) {
                                String string2 = (String) enum_key_field.nextElement();


                                if (string2.equalsIgnoreCase(Admin.TEXTFIELD)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    String mandatory = object[1].toString();
                                    EditText text = (EditText) object[0];
                                    String value = text.getText().toString().trim();
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value.length() == 0) {
                                            validation = true;
                                            text.startAnimation(shake);
                                            text.setError("Field cannot be left blank.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, value));
                                    i++;
                                } else if (string2.equalsIgnoreCase(Admin.TEXTAREA)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    EditText text = (EditText) object[0];
                                    String mandatory = object[1].toString();
                                    String value = text.getText().toString().trim();
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value.length() == 0) {
                                            validation = true;
                                            text.startAnimation(shake);
                                            text.setError("Field cannot be left blank.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, value));
                                    i++;
                                } else if (string2.equalsIgnoreCase(Admin.SELECT)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    Spinner spinner = (Spinner) object[0];
                                    String mandatory = object[1].toString();
                                    int value = spinner.getSelectedItemPosition();
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value == 0) {
                                            validation = true;
                                            TextView text = (TextView) spinner.getSelectedView();
                                            text.startAnimation(shake);
                                            text.setError("Field cannot be Unselected.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, spinner.getSelectedItem().toString()));
                                    i++;
                                } else if (string2.equalsIgnoreCase(Admin.RADIO)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    RadioGroup radioGroup = (RadioGroup) object[0];
                                    String mandatory = object[1].toString();
                                    int selected = radioGroup.getCheckedRadioButtonId();
                                    RadioButton radioButton = (RadioButton) getActivity().findViewById(selected);
                                    String s="";
                                    if(radioButton!=null){
                                        s=radioButton.getText().toString();
                                    }

                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (TextUtils.isEmpty(s)) {
                                            Toast.makeText(getActivity(),"Please Select Radio Button Value",Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    }

                                    params.add(new BasicNameValuePair(string, s));
                                    i++;
                                } else if (string2.endsWith(Admin.CHECKBOX)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    Hashtable<String, CheckBox> che = (Hashtable<String, CheckBox>) object[0];
                                    String mandatory = object[1].toString();

                                    Enumeration<CheckBox> e = che.elements();
                                    String selected_check_box = "";
                                    while (e.hasMoreElements()) {
                                        CheckBox checkBox = (CheckBox) e.nextElement();
                                        if (checkBox.isChecked()) {
                                            selected_check_box += checkBox.getText().toString() + ",";
                                        }
                                    }


                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (TextUtils.isEmpty(selected_check_box)) {
                                            Toast.makeText(getActivity(),"Please Select CheckBox Value",Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    }


                                    if (selected_check_box.length() > 0)
                                        selected_check_box = selected_check_box.substring(0, selected_check_box.length() - 1);
                                    params.add(new BasicNameValuePair(string, selected_check_box));
                                    i++;
                                }
                            }
                        }
                    }
                }
                if (dynamic_form_data_right.size() > 0) {
                    Enumeration<String> enum_key = dynamic_form_data_right.keys();
                    while (enum_key.hasMoreElements()) {
                        String string = (String) enum_key.nextElement();
//							Hashtable<String, Object> dynamic_form_field=(Hashtable<String, Object>)dynamic_form_data.get(string);
                        Hashtable<String, Object[]> dynamic_form_field = (Hashtable<String, Object[]>) dynamic_form_data_right.get(string);
                        string = string + "R";
                        if (dynamic_form_field.size() > 0) {
                            Enumeration<String> enum_key_field = dynamic_form_field.keys();
                            while (enum_key_field.hasMoreElements()) {
                                String string2 = (String) enum_key_field.nextElement();


                                if (string2.equalsIgnoreCase(Admin.TEXT_IMAGE)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    String mandatory = object[1].toString();
                                    View IMGV = (View) object[0];
                                    ImageView mFormImage = (ImageView) IMGV.findViewById(R.id.mFormImage);

                                    //ImageView text = (ImageView) object[0];
                                    String value = (String) mFormImage.getTag(R.id.mFormImage);
                                    if(value==null){
                                        value="";
                                    }
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value.length() == 0) {
                                            validation = true;
                                            mFormImage.startAnimation(shake);
                                            Toast.makeText(getActivity(),"Please Select Image",Toast.LENGTH_SHORT).show();
                                            return;
                                            // text.setError("Field cannot be left blank.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, value));
                                    j++;
                                } else if (string2.equalsIgnoreCase(Admin.TEXT_AUDIO)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    String mandatory = object[1].toString();
                                    RelativeLayout text = (RelativeLayout) object[0];
                                    ImageView playBtn = (ImageView) text.getTag(R.id.audioplay);
                                    String value = (String) playBtn.getTag();
                                    if(value==null){
                                        value="";
                                    }
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value.length() == 0) {
                                            validation = true;
                                            text.startAnimation(shake);
                                            Toast.makeText(getActivity(),"Please Select Audio",Toast.LENGTH_SHORT).show();
                                            return;
                                            // text.setError("Field cannot be left blank.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, value));
                                    j++;
                                } else if (string2.equalsIgnoreCase(Admin.TEXT_VIDEO)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    String mandatory = object[1].toString();
                                    ImageView text = (ImageView) object[0];
                                    String value = (String) text.getTag();
                                    if(value==null){
                                        value="";
                                    }
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value.length() == 0) {
                                            validation = true;
                                            text.startAnimation(shake);
                                            Toast.makeText(getActivity(),"Please Select video",Toast.LENGTH_SHORT).show();
                                            return;
                                            // text.setError("Field cannot be left blank.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, value));
                                    j++;
                                } else if (string2.equalsIgnoreCase(Admin.TEXTFIELD)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    String mandatory = object[1].toString();
                                    EditText text = (EditText) object[0];
                                    String value = text.getText().toString().trim();
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value.length() == 0) {
                                            validation = true;
                                            text.startAnimation(shake);
                                            text.setError("Field cannot be left blank.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, value));
                                    j++;
                                } else if (string2.equalsIgnoreCase(Admin.TEXTAREA)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    EditText text = (EditText) object[0];
                                    String mandatory = object[1].toString();
                                    String value = text.getText().toString().trim();
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value.length() == 0) {
                                            validation = true;
                                            text.startAnimation(shake);
                                            text.setError("Field cannot be left blank.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, value));
                                    j++;
                                } else if (string2.equalsIgnoreCase(Admin.SELECT)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    Spinner spinner = (Spinner) object[0];
                                    String mandatory = object[1].toString();
                                    int value = spinner.getSelectedItemPosition();
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (value == 0) {
                                            validation = true;
                                            TextView text = (TextView) spinner.getSelectedView();
                                            text.startAnimation(shake);
                                            text.setError("Field cannot be Unselected.");
                                        }
                                    }
                                    params.add(new BasicNameValuePair(string, spinner.getSelectedItem().toString()));
                                    j++;
                                } else if (string2.equalsIgnoreCase(Admin.RADIO)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    RadioGroup radioGroup = (RadioGroup) object[0];
                                    String mandatory = object[1].toString();

                                    int selected = radioGroup.getCheckedRadioButtonId();
                                    RadioButton radioButton = (RadioButton) getActivity().findViewById(selected);
                                    String s="";
                                    if(radioButton!=null){
                                        s=radioButton.getText().toString();
                                    }
                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (TextUtils.isEmpty(s)) {
                                            Toast.makeText(getActivity(),"Please Select Radio Button Value",Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    }

                                    params.add(new BasicNameValuePair(string, s));
                                    j++;
                                } else if (string2.endsWith(Admin.CHECKBOX)) {
                                    Object object[] = (Object[]) dynamic_form_field.get(string2);
                                    Hashtable<String, CheckBox> che = (Hashtable<String, CheckBox>) object[0];
                                    String mandatory = object[1].toString();

                                    Enumeration<CheckBox> e = che.elements();
                                    String selected_check_box = "";
                                    while (e.hasMoreElements()) {
                                        CheckBox checkBox = (CheckBox) e.nextElement();
                                        if (checkBox.isChecked()) {
                                            selected_check_box += checkBox.getText().toString() + ",";
                                        }
                                    }

                                    if (mandatory.equalsIgnoreCase("Mandatory")) {
                                        if (TextUtils.isEmpty(selected_check_box)) {
                                            Toast.makeText(getActivity(),"Please Select CheckBox Value",Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    }
                                    if (selected_check_box.length() > 0)
                                        selected_check_box = selected_check_box.substring(0, selected_check_box.length() - 1);
                                    params.add(new BasicNameValuePair(string, selected_check_box));
                                    j++;
                                }
                            }
                        }
                    }
                }
                params.add(new BasicNameValuePair(Admin.MAX_FORM_FIELD, String.valueOf(i)));
                params.add(new BasicNameValuePair(Admin.MAX_FORM_FIELD_1, String.valueOf(j)));
                //Log.d("PARAMS",params.toString());

                if (params != null) {
                    if (validation) {
                        //Toast.makeText(getActivity().getBaseContext(),"Please Fill All Text Fields", Toast.LENGTH_SHORT).show();
                    } else {
                        ParkingViolation.this.params_main = params;
                        fillForms();
                    }
                }
            }
        });
        scrollView.addView(linearLayout);
        mainLayout.addView(scrollView);
        getForms();
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_STATUSMESSAGE, statusMessage);
        outState.putString(KEY_FILENAME, filename);

      //  super.onSaveInstanceState(outState);
    }



    private void fillForms() {
        if (!new Network(getActivity()).isConnectingToInternet()) {
            showNoInternetConncetAlert();
        } else {
            //new FillForm().execute(new List[]{params_main});
            Intent intent = new Intent(getActivity(), CaptureSignature.class);
            Bundle bundle = new Bundle();
            Iterator<NameValuePair> iterator = params_main.iterator();
            while (iterator.hasNext()) {
                NameValuePair pair = (NameValuePair) iterator.next();
                //Log.d(pair.getName(), pair.getValue());
                bundle.putString(pair.getName().trim(), pair.getValue());
            }
            intent.putExtra(Admin.DATA, bundle);

            if (fragment_list_option.FORM_TYPE == 1) {
                intent.putExtra(Admin.URL, Admin.getUrlForDynamicFillTwoColumnData());
            } else {
                intent.putExtra(Admin.URL, Admin.getUrlForParkingViolationFormFill());
            }
           // getActivity().startActivityForResult(intent, Admin.REQUEST_CODE_LOG_OUT);
            getActivity().startActivity(intent);
        }
    }

    private void getForms() {
        if (!new Network(getActivity()).isConnectingToInternet()) {
            showNoInternetConncetAlert_1();
        } else {
            new GetForm().execute(new String[]{settings.getString(Admin.COMPANY_ID, "")});
        }
    }

    public void showNoInternetConncetAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                fillForms();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void showNoInternetConncetAlert_1() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                getForms();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    JSONObject form_left = null;
    JSONObject form_right = null;


    private class GetForm extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            try {
                // Building Parameters
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair(Admin.COMPANY_ID, args[0]));
                Log.v(Admin.TAG_SUCCESS, "Request => " + params.toString());
                //final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForParkingViolationForm(),"GET", params);

                params.add(new BasicNameValuePair("form_id", String.valueOf(fragment_list_option.FORM_ID)));
                JSONObject json2 = new JSONObject();
                if (fragment_list_option.FORM_TYPE == 1) {
                    json2 = jsonParser.makeHttpRequest(Admin.getUrlForDynamicTwoColoum(), "POST", params);
                } else if (fragment_list_option.FORM_TYPE == 2) {
                    json2 = jsonParser.makeHttpRequest(Admin.getUrlForDynamicMultiColoum(), "POST", params);
                } else {
                    json2 = jsonParser.makeHttpRequest(Admin.getUrlForParkingViolationForm(), "GET", params);
                }
                final JSONObject json = json2;

                Log.d(Admin.TAG_SUCCESS, "Server Responce  => " + json.toString());
                // check log cat from response
                if (json.has(Admin.FORM_LEFT)) {
                    try {
                        form_left = json.getJSONObject(Admin.FORM_LEFT);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        form_left=new JSONObject();
                    }
                }
                if (json.has(Admin.FORM_RIGHT)) {
                    try {
                        form_right = json.getJSONObject(Admin.FORM_RIGHT);
                    } catch (Exception e) {
                        form_right = null;
                    }
                }

                if(json.has("sign")){
                    String str=json.getString("sign");
                    if(!TextUtils.isEmpty(str)){
                        IS_SIGN=Integer.valueOf(str);
                    }
                }

/*
                Iterator<String> iterator= json.keys();
				TreeSet<String> set=new TreeSet<String>();
				while(iterator.hasNext()){
					String key=iterator.next();
					set.add(key);
				}
*/
                Iterator<String> iterator_left = form_left.keys();
                ArrayList<String> set_left = new ArrayList<String>();
                while (iterator_left.hasNext()) {
                    String key = iterator_left.next();
                    set_left.add(key);
                }
                Iterator<String> iterator_right = null;
                if (form_right != null) {
                    iterator_right = form_right.keys();
                }
                ArrayList<String> set_right = new ArrayList<String>();
                if (iterator_right != null) {
                    while (iterator_right.hasNext()) {
                        String key = iterator_right.next();
                        set_right.add(key);
                    }
                }

//				final Iterator<String>	iterator1=set.iterator();
                final Iterator<String> iterator1_left = set_left.iterator();
                final Iterator<String> iterator1_right = set_right.iterator();
                /*
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(json.has(Admin.HEADING)){
							try {
								textView.setText(json.getString(Admin.HEADING));
								json.remove(Admin.HEADING);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				});
				*/
                //json.remove("success");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int ID = 1001;
                        while (iterator1_left.hasNext()) {
                            try {
//								Hashtable<String, Object> data_dynamic=new Hashtable<String, Object>();
                                Hashtable<String, Object[]> data_dynamic = new Hashtable<String, Object[]>();
                                String str = iterator1_left.next();
                                LinearLayout layout_inner = new LinearLayout(getActivity());
                                layout_inner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                layout_inner.setOrientation(LinearLayout.VERTICAL);
                                layout_inner.setPadding(0, 5, 0, 5);
                                //layout_inner.setBackgroundResource(R.color.bg_log_row_even);
                                layout_inner.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                if (form_left.has(str)) {
                                    String data = form_left.getString(str);
                                    MixedArray mixedArray = Pherialize.unserialize(data).toArray();
                                    if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXT_IMAGE)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        final TextView textfield = new TextView(getActivity());
                                        textfield.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        textfield.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
                                        textfield.setSingleLine(true);
                                        textfield.setId(ID++);
                                        textfield.setOnClickListener(imageClick);
                                        textfield.setTag(textfield);
                                        textfield.setBackgroundResource((R.drawable.white_border));
                                        textfield.setPadding(5, 15, 5, 15);

                                        textfield.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(textfield);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
                                        data_dynamic.put(Admin.TEXT_IMAGE, new Object[]{textfield, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXT_AUDIO)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        final TextView textfield = new TextView(getActivity());
                                        textfield.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        textfield.setHint(mixedArray.getString(1).toUpperCase(Locale.getDefault()));
                                        textfield.setSingleLine(true);
                                        textfield.setOnClickListener(audioClick);
                                        textfield.setTag(textfield);
                                        textfield.setId(ID++);
                                        textfield.setBackgroundResource((R.drawable.white_border));
                                        textfield.setPadding(5, 15, 5, 15);

                                        textfield.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(textfield);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
                                        data_dynamic.put(Admin.TEXT_AUDIO, new Object[]{textfield, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXT_VIDEO)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        final TextView textfield = new TextView(getActivity());
                                        textfield.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        textfield.setHint(mixedArray.getString(1).toUpperCase(Locale.getDefault()));
                                        textfield.setSingleLine(true);
                                        textfield.setOnClickListener(videoClick);
                                        textfield.setTag(textfield);
                                        textfield.setId(ID++);
                                        textfield.setBackgroundResource((R.drawable.white_border));
                                        textfield.setPadding(5, 15, 5, 15);
                                        textfield.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(textfield);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
                                        data_dynamic.put(Admin.TEXT_VIDEO, new Object[]{textfield, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXTFIELD)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        final EditText textfield = new EditText(getActivity());
                                        textfield.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        textfield.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
                                        textfield.setSingleLine(true);
                                        textfield.setId(ID++);
                                        textfield.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                textfield.setError(null);
                                            }

                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                                          int after) {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                        textfield.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(textfield);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
                                        data_dynamic.put(Admin.TEXTFIELD, new Object[]{textfield, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXTAREA)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        final EditText textarea = new EditText(getActivity());
                                        textarea.setSingleLine(false);
                                        textarea.setId(ID++);
                                        textarea.setTypeface(Admin.getFontRoboto(getActivity()));
                                        textarea.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                                        textarea.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        textarea.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
                                        textarea.addTextChangedListener(new TextWatcher() {

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                textarea.setError(null);
                                            }

                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                                          int after) {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                        layout_inner.addView(textarea);
//										data_dynamic.put(Admin.TEXTAREA, textarea);
                                        data_dynamic.put(Admin.TEXTAREA, new Object[]{textarea, mixedArray.getString(3)});

                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.SELECT)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        String string_array[] = mixedArray.getString(2).split("\n");
                                        ArrayList<String> spinnerArray = new ArrayList<String>();
                                        spinnerArray.add(("Select " + mixedArray.getString(0)));
                                        for (String s : string_array) {
                                            spinnerArray.add(s);
                                        }
                                        Spinner spinner = new Spinner(getActivity());
                                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
                                        spinner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        spinner.setAdapter(arrayAdapter);
                                        spinner.setId(ID++);
                                        layout_inner.addView(spinner);
//										data_dynamic.put(Admin.SELECT,spinner);
                                        data_dynamic.put(Admin.SELECT, new Object[]{spinner, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.CHECKBOX)) {
                                        String string_array[] = mixedArray.getString(2).split("\n");
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        int check = 0;
                                        Hashtable<String, CheckBox> checkboxes_Hashtable = new Hashtable<String, CheckBox>();
                                        for (String s : string_array) {
                                            CheckBox checkBox = new CheckBox(getActivity());


                                            final float scale = getActivity().getResources().getDisplayMetrics().density;
                                            checkBox.setPadding(checkBox.getPaddingLeft() + (int)(10.0f * scale + 0.5f),
                                                    checkBox.getPaddingTop(),
                                                    checkBox.getPaddingRight(),
                                                    checkBox.getPaddingBottom());


                                            checkBox.setText(s.trim());
                                            checkBox.setId(ID++);
                                            //checkBox.setPadding(0, 0, 10, 0);

                                            checkBox.setTypeface(Admin.getFontRoboto(getActivity()));
                                            layout_inner.addView(checkBox);
                                            checkboxes_Hashtable.put(String.valueOf(check++).concat(Admin.CHECKBOX), checkBox);
                                        }
//										data_dynamic.put(Admin.CHECKBOX,checkboxes_Hashtable);
                                        data_dynamic.put(Admin.CHECKBOX, new Object[]{checkboxes_Hashtable, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.RADIO)) {
                                        String string_array[] = mixedArray.getString(2).split("\n");
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        RadioGroup radioGroup = new RadioGroup(getActivity());
                                        radioGroup.setOrientation(RadioGroup.VERTICAL);
                                        for (String s : string_array) {
                                            RadioButton radio = new RadioButton(getActivity());
                                            radio.setText(s.trim());
                                            radio.setId(ID++);
                                            //radio.setChecked(true);
                                            radio.setTypeface(Admin.getFontRoboto(getActivity()));
                                            final float scale = getActivity().getResources().getDisplayMetrics().density;
                                            radio.setPadding(radio.getPaddingLeft() + (int)(5.0f * scale + 0.5f),
                                                    radio.getPaddingTop(),
                                                    radio.getPaddingRight(),
                                                    radio.getPaddingBottom());
                                            radioGroup.addView(radio);
                                        }
                                        radioGroup.setId(ID++);
                                        layout_inner.addView(radioGroup);
//										data_dynamic.put(Admin.RADIO, radioGroup);
                                        data_dynamic.put(Admin.RADIO, new Object[]{radioGroup, mixedArray.getString(3)});
                                    }
                                }
                                //dynamic_form_data.put(str, data_dynamic);
                                dynamic_form_data_left.put(str, data_dynamic);
                                linearLayout.addView(layout_inner);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        LinearLayout layout_ = new LinearLayout(getActivity());
                        layout_.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                        layout_.setOrientation(LinearLayout.VERTICAL);
                        layout_.setPadding(15, 5, 15, 5);
                        linearLayout.addView(layout_);
                        while (iterator1_right.hasNext()) {
                       // for (String s : iterator1_right.) {
                            try {
//								Hashtable<String, Object> data_dynamic=new Hashtable<String, Object>();
                                Hashtable<String, Object[]> data_dynamic = new Hashtable<String, Object[]>();
                                String str = iterator1_right.next();
                                LinearLayout layout_inner = new LinearLayout(getActivity());
                                layout_inner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                layout_inner.setOrientation(LinearLayout.VERTICAL);
                                layout_inner.setPadding(0, 5, 0, 5);
                                layout_inner.setBackgroundColor(Color.parseColor("#dddcdc"));
                                if (form_right.has(str)) {
                                    String data = form_right.getString(str);
                                    MixedArray mixedArray = Pherialize.unserialize(data).toArray();
                                    //Log.d("Mixed Array",mixedArray.toString());
                                    if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXT_IMAGE)) {

                                        View IMGV = getActivity().getLayoutInflater().inflate(R.layout.form_image_row, null, false);
                                        TextView txt = (TextView) IMGV.findViewById(R.id.mFiledName);
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        ImageView textfield = (ImageView) IMGV.findViewById(R.id.mselectAudio);
                                        ImageView mFormImage = (ImageView) IMGV.findViewById(R.id.mFormImage);
                                        ImageView mDeleteImg = (ImageView) IMGV.findViewById(R.id.mDeleteImg);
                                        textfield.setId(ID++);

                                        textfield.setOnClickListener(imageClick);
                                        mFormImage.setOnClickListener(imageClick);
                                        mDeleteImg.setOnClickListener(imageDeleteClick);

//                                        textfield.setTag(mFormImage);
//                                        mFormImage.setTag(mFormImage);
//                                        mDeleteImg.setTag(mFormImage);

                                        textfield.setTag(IMGV);
                                        mFormImage.setTag(IMGV);
                                        mDeleteImg.setTag(IMGV);

                                        textfield.setPadding(5, 15, 5, 15);
                                        layout_inner.addView(IMGV);
                                        data_dynamic.put(Admin.TEXT_IMAGE, new Object[]{IMGV, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXT_AUDIO)) {
                                        View IMGV = getActivity().getLayoutInflater().inflate(R.layout.form_audio_row, null, false);
                                        TextView txt = (TextView) IMGV.findViewById(R.id.mFiledName);
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        RelativeLayout mTopLayout = (RelativeLayout) IMGV.findViewById(R.id.toplayout);
                                        ImageView mClickAudioselect = (ImageView) IMGV.findViewById(R.id.mselectAudio);
                                        ImageView audioplay = (ImageView) IMGV.findViewById(R.id.audioplay);
                                        ImageView mDeleteAudio = (ImageView) IMGV.findViewById(R.id.mDeleteAudio);
                                        mTopLayout.setVisibility(View.INVISIBLE);
                                        mClickAudioselect.setOnClickListener(audioClick);
                                      //  mTopLayout.setOnClickListener(audioClick);
                                        mDeleteAudio.setOnClickListener(audioDeleteClick);

                                        audioplay.setOnClickListener(playClickListener);
                                        mClickAudioselect.setTag(mTopLayout);
                                        mDeleteAudio.setTag(mTopLayout);
                                        mTopLayout.setTag(mTopLayout);
                                        mTopLayout.setTag(R.id.audioplay, audioplay);
                                        mTopLayout.setTag(R.id.mselectAudio, mClickAudioselect);
                                        layout_inner.addView(IMGV);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
                                        data_dynamic.put(Admin.TEXT_AUDIO, new Object[]{mTopLayout, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXT_VIDEO)) {
                                        View IMGV = getActivity().getLayoutInflater().inflate(R.layout.form_video_row, null, false);
                                        TextView txt = (TextView) IMGV.findViewById(R.id.mFiledName);
                                        ImageView videoplay = (ImageView) IMGV.findViewById(R.id.mFormImage);
                                        ImageView selectVideo = (ImageView) IMGV.findViewById(R.id.mselectAudio);
                                        ImageView mDeleteVideo = (ImageView) IMGV.findViewById(R.id.mDeleteVideo);

                                        videoplay.setVisibility(View.INVISIBLE);
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        selectVideo.setId(ID++);

                                        selectVideo.setOnClickListener(videoClick);
                                        videoplay.setOnClickListener(videoPlayClick);
                                        mDeleteVideo.setOnClickListener(videoDeleteClick);
                                        selectVideo.setTag(videoplay);
                                        mDeleteVideo.setTag(videoplay);
                                        mDeleteVideo.setTag(R.id.mselectAudio,selectVideo);

                                        layout_inner.addView(IMGV);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
                                        data_dynamic.put(Admin.TEXT_VIDEO, new Object[]{videoplay, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXTFIELD)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        final EditText textfield = new EditText(getActivity());
                                        textfield.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        textfield.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
                                        textfield.setSingleLine(true);
                                        textfield.setId(ID++);
                                        textfield.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                textfield.setError(null);
                                            }

                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                                          int after) {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                        textfield.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(textfield);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
                                        data_dynamic.put(Admin.TEXTFIELD, new Object[]{textfield, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.TEXTAREA)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        final EditText textarea = new EditText(getActivity());
                                        textarea.setSingleLine(false);
                                        textarea.setId(ID++);
                                        textarea.setTypeface(Admin.getFontRoboto(getActivity()));
                                        textarea.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                                        textarea.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        textarea.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
                                        textarea.addTextChangedListener(new TextWatcher() {

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                textarea.setError(null);
                                            }

                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                                          int after) {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                        layout_inner.addView(textarea);
//										data_dynamic.put(Admin.TEXTAREA, textarea);
                                        data_dynamic.put(Admin.TEXTAREA, new Object[]{textarea, mixedArray.getString(3)});

                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.SELECT)) {
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        String string_array[] = mixedArray.getString(2).split("\n");
                                        ArrayList<String> spinnerArray = new ArrayList<String>();
                                        spinnerArray.add(("Select " + mixedArray.getString(0)));
                                        for (String s : string_array) {
                                            spinnerArray.add(s);
                                        }
                                        Spinner spinner = new Spinner(getActivity());
                                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
                                        spinner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        spinner.setAdapter(arrayAdapter);
                                        spinner.setId(ID++);
                                        layout_inner.addView(spinner);
//										data_dynamic.put(Admin.SELECT,spinner);
                                        data_dynamic.put(Admin.SELECT, new Object[]{spinner, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.CHECKBOX)) {
                                        String string_array[] = mixedArray.getString(2).split("\n");
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        int check = 0;
                                        Hashtable<String, CheckBox> checkboxes_Hashtable = new Hashtable<String, CheckBox>();
                                        for (String s : string_array) {
                                            CheckBox checkBox = new CheckBox(getActivity());
                                            checkBox.setText(s.trim());
                                            checkBox.setId(ID++);
                                            //checkBox.setPadding(0, 0, 10, 0);

                                            final float scale = getActivity().getResources().getDisplayMetrics().density;
                                            checkBox.setPadding(checkBox.getPaddingLeft() + (int)(5.0f * scale + 0.5f),
                                                    checkBox.getPaddingTop(),
                                                    checkBox.getPaddingRight(),
                                                    checkBox.getPaddingBottom());
                                            checkBox.setTypeface(Admin.getFontRoboto(getActivity()));
                                            layout_inner.addView(checkBox);
                                            checkboxes_Hashtable.put(String.valueOf(check++).concat(Admin.CHECKBOX), checkBox);
                                        }
//										data_dynamic.put(Admin.CHECKBOX,checkboxes_Hashtable);
                                        data_dynamic.put(Admin.CHECKBOX, new Object[]{checkboxes_Hashtable, mixedArray.getString(3)});
                                    } else if (mixedArray.getString(1).equalsIgnoreCase(Admin.RADIO)) {
                                        String string_array[] = mixedArray.getString(2).split("\n");
                                        TextView txt = new TextView(getActivity());
                                        txt.setText(mixedArray.getString(0));
                                        txt.setTypeface(Typeface.DEFAULT_BOLD);
                                        txt.setId(ID++);
                                        txt.setTypeface(Admin.getFontRoboto(getActivity()));
                                        layout_inner.addView(txt);
                                        RadioGroup radioGroup = new RadioGroup(getActivity());
                                        radioGroup.setOrientation(RadioGroup.VERTICAL);
                                        for (String s : string_array) {
                                            RadioButton radio = new RadioButton(getActivity());
                                            radio.setText(s.trim());
                                            radio.setId(ID++);
                                           // radio.setChecked(true);

                                            final float scale = getActivity().getResources().getDisplayMetrics().density;
                                            radio.setPadding(radio.getPaddingLeft() + (int)(5.0f * scale + 0.5f),
                                                    radio.getPaddingTop(),
                                                    radio.getPaddingRight(),
                                                    radio.getPaddingBottom());

                                            radio.setTypeface(Admin.getFontRoboto(getActivity()));
                                            radioGroup.addView(radio);
                                        }
                                        radioGroup.setId(ID++);
                                        layout_inner.addView(radioGroup);
//										data_dynamic.put(Admin.RADIO, radioGroup);
                                        data_dynamic.put(Admin.RADIO, new Object[]{radioGroup, mixedArray.getString(3)});
                                    }
                                }
//								dynamic_form_data.put(str, data_dynamic);
                                dynamic_form_data_right.put(str, data_dynamic);
                                linearLayout.addView(layout_inner);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Error : ", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            linearLayout.addView(button);
        }
    }

    private class FillForm extends AsyncTask<List<NameValuePair>, String, Integer> {
        int success = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Update...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(List<NameValuePair>... params) {
            final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForParkingViolationFormFill(),
                    "GET", params[0]);
            Log.d("PARKING VIO ", json.toString());
            try {
                if (json.has(Admin.TAG_SUCCESS)) {
                    return json.getInt(Admin.TAG_SUCCESS);
                } else {
                    return 0;
                }
            } catch (Exception e) {
                Log.d("Error", e.getMessage());
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                progressDialog.setMessage("Successfully Update");
                //Log.d("FILL","Successful");
                Toast toast = new Toast(getActivity().getApplicationContext());
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.setView(toast_view);
                toast.show();
            } else {
                progressDialog.setMessage("Unsuccessfully Update");
            }
            progressDialog.dismiss();
        }
    }

    public void refreshFrom() {
		/*
			Toast toast =new Toast(getActivity().getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.setView(toast_view);
			toast.show();
		*/
        if (dynamic_form_data_left.size() > 0) {
            Enumeration<String> enum_key = dynamic_form_data_left.keys();
            while (enum_key.hasMoreElements()) {
                String string = (String) enum_key.nextElement();
                Hashtable<String, Object[]> dynamic_form_field = (Hashtable<String, Object[]>) dynamic_form_data_left.get(string);
                if (dynamic_form_field.size() > 0) {
                    Enumeration<String> enum_key_field = dynamic_form_field.keys();
                    while (enum_key_field.hasMoreElements()) {
                        String string2 = (String) enum_key_field.nextElement();
                        if (string2.equalsIgnoreCase(Admin.TEXTFIELD)) {
                            Object object[] = (Object[]) dynamic_form_field.get(string2);
                            EditText text = (EditText) object[0];
                            text.setText("");
                        } else if (string2.equalsIgnoreCase(Admin.TEXTAREA)) {
                            Object object[] = (Object[]) dynamic_form_field.get(string2);
                            EditText text = (EditText) object[0];
                            text.setText("");
                        } else if (string2.equalsIgnoreCase(Admin.SELECT)) {
                            Object object[] = (Object[]) dynamic_form_field.get(string2);
                            Spinner spinner = (Spinner) object[0];
                            //spinner.setSelected(false);
                            spinner.setSelection(0);
                        } else if (string2.equalsIgnoreCase(Admin.RADIO)) {
                            //RadioGroup radioGroup=(RadioGroup)dynamic_form_field.get(string2);
                            //int selected=radioGroup.getCheckedRadioButtonId();
                            //RadioButton radioButton=(RadioButton)getActivity().findViewById(selected);
                        } else if (string2.endsWith(Admin.CHECKBOX)) {
                            Object object[] = (Object[]) dynamic_form_field.get(string2);
                            @SuppressWarnings("unchecked")
                            Hashtable<String, CheckBox> che = (Hashtable<String, CheckBox>) object[0];
                            Enumeration<CheckBox> e = che.elements();
                            while (e.hasMoreElements()) {
                                CheckBox checkBox = (CheckBox) e.nextElement();
                                if (checkBox.isChecked()) {
                                    checkBox.setChecked(false);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (dynamic_form_data_right != null) {
            if (dynamic_form_data_right.size() > 0) {
                Enumeration<String> enum_key = dynamic_form_data_right.keys();
                while (enum_key.hasMoreElements()) {
                    String string = (String) enum_key.nextElement();
                    Hashtable<String, Object[]> dynamic_form_field = (Hashtable<String, Object[]>) dynamic_form_data_right.get(string);
                    if (dynamic_form_field.size() > 0) {
                        Enumeration<String> enum_key_field = dynamic_form_field.keys();
                        while (enum_key_field.hasMoreElements()) {
                            String string2 = (String) enum_key_field.nextElement();

                            if (string2.equalsIgnoreCase(Admin.TEXT_IMAGE)) {
                                Object object[] = (Object[]) dynamic_form_field.get(string2);
                                View  view = (View) object[0];
                                ImageView text = (ImageView) view.findViewById(R.id.mFormImage);
                                //ImageView text = (ImageView) object[0];
                                text.setTag(R.id.mFormImage,"");
                                text.setImageResource(0);
                                text.setImageDrawable(null);
                            } else if (string2.equalsIgnoreCase(Admin.TEXT_AUDIO)) {
                                Object object[] = (Object[]) dynamic_form_field.get(string2);
                                String mandatory = object[1].toString();
                                RelativeLayout text = (RelativeLayout) object[0];
                                ImageView selectAu = (ImageView) text.getTag(R.id.mselectAudio);
                                selectAu.setVisibility(View.VISIBLE);
                                text.setTag("");
                                text.setVisibility(View.GONE);
                            } else if (string2.equalsIgnoreCase(Admin.TEXT_VIDEO)) {
                                Object object[] = (Object[]) dynamic_form_field.get(string2);
                                ImageView text = (ImageView) object[0];
                                text.setVisibility(View.GONE);
                                text.setTag("");
                                text.setImageResource(0);
                                text.setImageDrawable(null);

                            }
                         else   if (string2.equalsIgnoreCase(Admin.TEXTFIELD)) {
                                Object object[] = (Object[]) dynamic_form_field.get(string2);
                                EditText text = (EditText) object[0];
                                text.setText("");
                            } else if (string2.equalsIgnoreCase(Admin.TEXTAREA)) {
                                Object object[] = (Object[]) dynamic_form_field.get(string2);
                                EditText text = (EditText) object[0];
                                text.setText("");
                            } else if (string2.equalsIgnoreCase(Admin.SELECT)) {
                                Object object[] = (Object[]) dynamic_form_field.get(string2);
                                Spinner spinner = (Spinner) object[0];
                                //spinner.setSelected(false);
                                spinner.setSelection(0);
                            } else if (string2.equalsIgnoreCase(Admin.RADIO)) {
                                //RadioGroup radioGroup=(RadioGroup)dynamic_form_field.get(string2);
                                //int selected=radioGroup.getCheckedRadioButtonId();
                                //RadioButton radioButton=(RadioButton)getActivity().findViewById(selected);
                            } else if (string2.endsWith(Admin.CHECKBOX)) {
                                Object object[] = (Object[]) dynamic_form_field.get(string2);
                                @SuppressWarnings("unchecked")
                                Hashtable<String, CheckBox> che = (Hashtable<String, CheckBox>) object[0];
                                Enumeration<CheckBox> e = che.elements();
                                while (e.hasMoreElements()) {
                                    CheckBox checkBox = (CheckBox) e.nextElement();
                                    if (checkBox.isChecked()) {
                                        checkBox.setChecked(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
if(getActivity()!=null&&ParkingViolation.this!=null) {
    port();
    Bitmap bitmap = null;
    imagepath = "";
    if (requestCode == RESULT_LOAD_IMAGE && resultCode == getActivity().RESULT_OK && null != data) {
        try {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imagepath = cursor.getString(columnIndex);
            cursor.close();
            try {
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                // bitmap = BitmapFactory.decodeFile(imagepath, bitmapOptions);
                bitmap = createScaledBitmap(getImagePath(data, getActivity()), mGlobalImage.getWidth(), mGlobalImage.getHeight());

                   /* ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                    File f = new File(imagepath);
                    f.createNewFile();
                    FileOutputStream fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());
                    fo.close();*/


                mGlobalImage.setImageBitmap(bitmap);
                mGlobalImage.setTag(R.id.mFormImage, imagepath);

                // mGlobalTextView.setText(imagepath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    } else if (requestCode == CAMERA_REQUEST && resultCode == getActivity().RESULT_OK) {

        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(mCapturedImageURI, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndex(filePathColumn[0]);
            imagepath = cursor.getString(column_index);
            cursor.close();
            try {
                // BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                //  bitmap = BitmapFactory.decodeFile(imagepath, bitmapOptions);

                Bitmap bitmapCamera = createScaledBitmap(imagepath, mGlobalImage.getWidth(), mGlobalImage.getHeight());

                   /* ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmapCamera.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                    File f = new File(imagepath);
                    f.createNewFile();
                    FileOutputStream fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());
                    fo.close();
*/
                mGlobalImage.setImageBitmap(bitmapCamera);
                mGlobalImage.setTag(R.id.mFormImage, imagepath);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    } else if (requestCode == RQS_OPEN_AUDIO_MP3 && resultCode == getActivity().RESULT_OK) {

        Uri uri = data.getData();

        String path = FileUtils.getPath(getActivity(), uri);
        mTopLayoutAudio.setVisibility(View.VISIBLE);
        mTopLayoutAudio.setTag(R.id.toplayout, path);
        ImageView playBtn = (ImageView) mTopLayoutAudio.getTag(R.id.audioplay);

        playBtn.setTag(path);
        strAudioUri = path;
    } else if (requestCode == VIDEO_REQUEST
            && resultCode == Activity.RESULT_OK) {

        if (videoPath != null && videoPath.trim().length() != 0) {

            mGlobalImage.setTag(videoPath);
            mGlobalImage.setVisibility(View.VISIBLE);
            Bitmap finalBitmap = ThumbnailUtils.createVideoThumbnail(videoPath,
                    MediaStore.Images.Thumbnails.MINI_KIND);
            mGlobalImage.setImageBitmap(finalBitmap);
        }

    } else if (resultCode == getActivity().RESULT_OK || requestCode == 101) {

        filename = data.getStringExtra(VideoCaptureActivity.EXTRA_OUTPUT_FILENAME);
        statusMessage = String.format(getString(R.string.status_capturesuccess), filename);
        final Bitmap thumbnail = getThumbnail();
        // Toast.makeText(getActivity(),filename+"",Toast.LENGTH_SHORT).show();

        if(TextUtils.isEmpty(filename)||mGlobalImage==null){
            return;
        }



        mGlobalImage.setTag(filename);
        mGlobalImage.setVisibility(View.VISIBLE);
        if (thumbnail != null) {
            mGlobalImage.setImageBitmap(thumbnail);
        }

    } else {
        refreshFrom();
        //  getActivity().onBackPressed();


    }
}

    }
    private Bitmap getThumbnail() {
        if (filename == null) return null;
        return ThumbnailUtils.createVideoThumbnail(filename, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
    }

    public void select_Image() {

        final CharSequence[] items1 = {"Use Camera", "Choose Existing"};
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setTitle("Choose Option :");

        builder1.setItems(items1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (items1[item].equals("Choose Existing")) {

                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    getActivity().startActivityForResult(i, RESULT_LOAD_IMAGE);
                } else if (items1[item].equals("Use Camera")) {
                    Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
                    if (isSDPresent) {
                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "guardtabimage");
                        mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
                        getActivity().startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(),
                                "No SD card sorry!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        AlertDialog alert1 = builder1.create();
        alert1.show();

    }


    @Override
    public void onClick(View view) {

    }

    View.OnClickListener imageClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Toast.makeText(getActivity(), "Select an image", Toast.LENGTH_SHORT).show();
            View IMGV= (View) view.getTag();
            ImageView mFormImage = (ImageView) IMGV.findViewById(R.id.mFormImage);
            mGlobalImage = mFormImage;
            select_Image();
        }
    };

    View.OnClickListener imageDeleteClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View  view = (View) v.getTag();
            ImageView imageView = (ImageView) view.findViewById(R.id.mFormImage);
           // ImageView imageView= (ImageView) view.getTag();
            imageView.setTag(R.id.mFormImage,"");
            imageView.setImageResource(0);
            imageView.setImageDrawable(null);

        }
    };

    View.OnClickListener audioClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mGlobalTextView = null;
            Toast.makeText(getActivity(), "Select an audio", Toast.LENGTH_SHORT).show();
            mTopLayoutAudio = (RelativeLayout) view.getTag();

            customDialog(mTopLayoutAudio);
           // audioSelect();
        }
    };

    View.OnClickListener audioDeleteClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            View v22 = (View) v.getTag();
            v22.setVisibility(View.GONE);
            ImageView selectAu = (ImageView) v22.getTag(R.id.mselectAudio);

            selectAu.setVisibility(View.GONE);
            ImageView selectAu1 = (ImageView) v22.getTag(R.id.mselectAudio);
            selectAu1.setVisibility(View.VISIBLE);
            View playBtn = (View) v22.getTag(R.id.audioplay);
            playBtn.setTag("");

        }
    };
    View.OnClickListener videoClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mGlobalTextView = null;
         //   Toast.makeText(getActivity(), "Select an video", Toast.LENGTH_SHORT).show();

            mGlobalImage = (ImageView) view.getTag();
            mGlobalImage.setVisibility(View.VISIBLE);
           // getVideo();

            startVideoCaptureActivity();
        }
    };


    public void audioSelect() {

        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        getActivity().startActivityForResult(i, RQS_OPEN_AUDIO_MP3);


    }

    public void getVideo() {


        Boolean isSDPresent = android.os.Environment
                .getExternalStorageState().equals(
                        android.os.Environment.MEDIA_MOUNTED);
        if (isSDPresent) {
            String path = Environment.getExternalStorageDirectory() + "";
          //  videoPath = path + "/Vi" + System.currentTimeMillis() + ".mp4";
            videoPath = path + "/Vi" + System.currentTimeMillis() + ".mov";
            File mediaFile = new File(videoPath);
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            Uri videoUri = Uri.fromFile(mediaFile);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
            intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT,41943040);
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,240);
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
            getActivity().startActivityForResult(intent, VIDEO_REQUEST);

        } else {
            Toast.makeText(getActivity(), "no sd card", Toast.LENGTH_SHORT).show();
        }

    }

    View.OnClickListener playClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                String uri = (String) view.getTag();

                if (mPlayer != null) {
                    if (mPlayer.isPlaying()) {
                        mPlayer.stop();
                        mPlayer = null;
                    }
                } else {


                    mPlayer = new MediaPlayer();
                    try {
                        mPlayer.setDataSource(uri);
                        mPlayer.prepare();
                        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer arg0) {
                                mPlayer.start();
                            }
                        });
                    } catch (IOException e) {
                    }
                }

            } catch (Exception e) {

                e.printStackTrace();
            }

        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        violation=null;
        if (mPlayer != null) {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
                mPlayer = null;
            }
        }
        IS_SIGN=2;
    }

    // Function to get image path from ImagePicker
    public static String getImagePath(Intent data, Context context) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }


    public Bitmap createScaledBitmap(String pathName, int width, int height) {
        final BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, opt);
        opt.inSampleSize = calculateBmpSampleSize(opt, width, height);
        opt.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, opt);
       // return decodeFile(new File(pathName));
    }

    public int calculateBmpSampleSize(BitmapFactory.Options opt, int width, int height) {
        final int outHeight = opt.outHeight;
        final int outWidth = opt.outWidth;
        int sampleSize = 1;
        if (outHeight > height || outWidth > width) {
            final int heightRatio = Math.round((float) outHeight / (float) height);
            final int widthRatio = Math.round((float) outWidth / (float) width);
            sampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return sampleSize;
    }

    private Bitmap decodeFile(File f){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=70;

            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    View.OnClickListener videoPlayClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showimgandVideo(view);
        }
    };

    View.OnClickListener videoDeleteClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ImageView imageView= (ImageView) view.getTag();
            ImageView image= (ImageView) view.getTag(R.id.mselectAudio);
            //image.setTag("");
            image.setVisibility(View.VISIBLE);

            imageView.setTag("");
            imageView.setVisibility(View.GONE);
            imageView.setImageResource(0);
            imageView.setImageDrawable(null);
        }
    };

    public void showimgandVideo(View view) {

        String str = (String) view.getTag();
        if(!TextUtils.isEmpty(str)){
        if (new File(str).exists()) {
            String mimeType = getMimeType(str);
            Intent mediaIntent = new Intent(Intent.ACTION_VIEW);
            mediaIntent.setDataAndType(Uri.fromFile(new File(str)), mimeType);
            try {
                getActivity().startActivity(mediaIntent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();

            }
        }
        }

    }


    public String getMimeType(String url) {
        String extension = url.substring(url.lastIndexOf("."));
        String mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extension);
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                mimeTypeMap);
        return mimeType;
    }

    public void customDialog(View view) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.form_audio_record_row);
        dialog.setTitle("Audio Record");

        // set the custom dialog components - text, image and button
        ImageView mRecordStart = (ImageView) dialog.findViewById(R.id.mRecordStart);
        ImageView mRecordStop = (ImageView) dialog.findViewById(R.id.mRecordStop);
     final    TextView mText = (TextView) dialog.findViewById(R.id.mText);
        Button mRecordSave = (Button) dialog.findViewById(R.id.mRecordSave);
        Button mRecordCancel = (Button) dialog.findViewById(R.id.mRecordCancel);

        mRecordStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    mText.setText("Recording...");
                    View v22 = (View) v.getTag();
                    startrecord(v22);


            }
        });
        mRecordStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recordAudio!=null){
                    mText.setText("Stop");
                    recordAudio.stopRecording(false,v);
                }
            }
        });
        mRecordSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(recordAudio!=null){
                    View v22 = (View) v.getTag();
                    v22.setVisibility(View.VISIBLE);
                    ImageView selectAu = (ImageView) v22.getTag(R.id.mselectAudio);
                    selectAu.setVisibility(View.GONE);
                    ImageView playBtn = (ImageView) v22.getTag(R.id.audioplay);
                    recordAudio.stopRecording(true,playBtn);
                }
            }
        });
        mRecordCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(recordAudio!=null){

                    View v22 = (View) v.getTag();
                    ImageView selectAu = (ImageView) v22.getTag(R.id.mselectAudio);
                    selectAu.setVisibility(View.VISIBLE);
                    v22.setVisibility(View.GONE);
                    ImageView playBtn = (ImageView) v22.getTag(R.id.audioplay);
                    playBtn.setTag("");
                    recordAudio.stopRecording(false,v22);
                }
            }
        });
        mRecordStart.setTag(view);
        mRecordStop.setTag(view);
        mRecordCancel.setTag(view);
        mRecordSave.setTag(view);
        dialog.show();

    }

    private void startrecord(View view) {

        recordAudio = new RecordAudio(getActivity());
        recordAudio.startRecording(view);
    }

    public class RecordAudio {
        private final String LOG_TAG = RecordAudio.class.getName();
        private String mFileName = null;

        private MediaRecorder mRecorder = null;

        public RecordAudio(Context con) {

            String path = Environment.getExternalStorageDirectory() + "";
            mFileName = path + "/Au" + System.currentTimeMillis() + ".mp3";

        }


    private void startRecording(View view) {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        view.setTag(R.id.toplayout,mFileName);
        try {
            mRecorder.prepare();
            Log.e(LOG_TAG, "prepare() failed");
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        try {
            mRecorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private void stopRecording(boolean issend,View view) {
        try {
            if (mRecorder != null) {
                try {
                    mRecorder.stop();
                    mRecorder.release();
                    mRecorder = null;
                } catch (RuntimeException stopException) {
                    //handle cleanup here
                }

            }
            if(issend){
                view.setTag(mFileName);
            }else{

            }

        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

}



    private void startVideoCaptureActivity() {
        land();
        final CaptureConfiguration config = createCaptureConfiguration();
        final String filename = "";

        final Intent intent = new Intent(getActivity(), VideoCaptureActivity.class);
        intent.putExtra(VideoCaptureActivity.EXTRA_CAPTURE_CONFIGURATION, config);
        intent.putExtra(VideoCaptureActivity.EXTRA_OUTPUT_FILENAME, filename);
        startActivityForResult(intent, 101);
    }


    private CaptureConfiguration createCaptureConfiguration() {
        final PredefinedCaptureConfigurations.CaptureResolution resolution = getResolution(2);
        final PredefinedCaptureConfigurations.CaptureQuality quality = getQuality(2);
        int fileDuration = CaptureConfiguration.NO_DURATION_LIMIT;
        try {
            fileDuration = Integer.valueOf("");
        } catch (final Exception e) {
            //NOP
        }
        int filesize = CaptureConfiguration.NO_FILESIZE_LIMIT;
        try {
            filesize = Integer.valueOf("");
        } catch (final Exception e2) {
            //NOP
        }
        boolean showTimer = true;
        boolean allowFrontCamera = true;
        return new CaptureConfiguration(resolution, quality, fileDuration, filesize, showTimer, allowFrontCamera);
    }


    private PredefinedCaptureConfigurations.CaptureResolution getResolution(int position) {
        final PredefinedCaptureConfigurations.CaptureResolution[] resolution = new PredefinedCaptureConfigurations.CaptureResolution[]{PredefinedCaptureConfigurations.CaptureResolution.RES_1080P,
                PredefinedCaptureConfigurations.CaptureResolution.RES_720P, PredefinedCaptureConfigurations.CaptureResolution.RES_480P};
        return resolution[position];
    }

    private PredefinedCaptureConfigurations.CaptureQuality getQuality(int position) {
        final PredefinedCaptureConfigurations.CaptureQuality[] quality = new PredefinedCaptureConfigurations.CaptureQuality[]{PredefinedCaptureConfigurations.CaptureQuality.HIGH, PredefinedCaptureConfigurations.CaptureQuality.MEDIUM,
                PredefinedCaptureConfigurations.CaptureQuality.LOW};
        return quality[position];
    }

    public void land(){
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
    } public void port(){
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
    }


    @Override
    public void onResume() {
        super.onResume();
        port();
    }
}
package com.b2infosoft.gt.rajesh.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.rajesh.adapter.DynamicFormAdapter;
import com.b2infosoft.gt.rajesh.fragments.dashboard.DashboardNew;
import com.b2infosoft.gt.rajesh.fragments.dashboard.SessionStart;
import com.b2infosoft.gt.rajesh.modals.DynamicFormBean;
import com.b2infosoft.gt.server.JSONParser;
import com.google.gson.Gson;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link fragment_list_option#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_list_option extends Fragment implements View.OnClickListener {

    private GridView mGridView;
    private DynamicFormAdapter adapter;
    private String client_id = "";
    private JSONParser jsonParser = new JSONParser();
    private DynamicFormBean lastSeenModel = null;
    private ArrayList<DynamicFormBean.FormsBean> formsList = new ArrayList<>();
    public static int FORM_ID = 0;
    public static int FORM_TYPE = 0;
    public static String TITLE = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_list_option, container, false);
        client_id = MySharedPreferences.getSharedPreferences(getActivity(), "company_id");
        initiateUi(view);
        if (!new Network(getActivity()).isConnectingToInternet()) {
            showNoInternetConncetAlert();
        } else {
            new DynamicFormAsync().execute();
        }
        return view;
    }

    public void initiateUi(View view) {
        mGridView = (GridView) view.findViewById(R.id.list_item);
        adapter = new DynamicFormAdapter(fragment_list_option.this, formsList);
        adapter.setClickListener(this);
        mGridView.setAdapter(adapter);
    }

    public void notifyAdapter() {

        adapter.setFormsList(formsList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (R.id.mClickBtn == view.getId()) {
            int pos = (int) view.getTag();
            FORM_ID = formsList.get(pos).getForm_id();
            TITLE= formsList.get(pos).getForm_name();
            if ("daily_activity".equalsIgnoreCase(formsList.get(pos).getType())) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content_frame, DailyLog.newInstance(),
                                DailyLog.TAG).addToBackStack(null).commit();

            } else if ("multi column".equalsIgnoreCase(formsList.get(pos).getType())) {
                FORM_TYPE=2;
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_frame, DailyReport_1.newInstance(), DailyReport_1.TAG).addToBackStack(null).commit();

            } else if ("two column".equalsIgnoreCase(formsList.get(pos).getType())) {
                FORM_TYPE=1;
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.content_frame, ParkingViolation.newInstance(), ParkingViolation.TAG).addToBackStack(null).commitAllowingStateLoss();
            } else {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content_frame, DashboardNew.newInstance(),
                                DashboardNew.TAG).addToBackStack(null).commit();
            }

        }
    }


    class DynamicFormAsync extends AsyncTask<String, String, String> {
        private ProgressDialog progressBar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(getActivity()!=null) {
                progressBar = new ProgressDialog(getActivity());
                progressBar.setMessage("Wait...");
                progressBar.show();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String response = "";
                ArrayList localArrayList = new ArrayList();

                localArrayList.add(new BasicNameValuePair("company_id", client_id));
                JSONObject localJSONObject = jsonParser.makeHttpRequest(Admin.getUrlForDynamicFormItem(), "POST", localArrayList);
                Gson gson = new Gson();
                lastSeenModel = gson.fromJson(localJSONObject.toString(), DynamicFormBean.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(progressBar!=null) {
                progressBar.dismiss();
            }

            if (lastSeenModel != null && lastSeenModel.getSuccess() == 1) {
                formsList = lastSeenModel.getForms();
                notifyAdapter();
            }
        }
    }

    public static Fragment newInstance() {
        return new fragment_list_option();
    }

    public void showNoInternetConncetAlert() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle("No Internet Connection");
        localBuilder.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");
        localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                if (!new Network(getActivity()).isConnectingToInternet()) {
                    showNoInternetConncetAlert();
                }
            }
        });
        localBuilder.show();
    }

}

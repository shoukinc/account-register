package com.b2infosoft.gt.rajesh.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.widget.ImageView;

public class RoundImage {
	static Context mcontext;
	
	public void set(){
		 ImageView im = new ImageView(mcontext);
		 im.setImageBitmap(getRoundedShape(decodeFile(mcontext, 0),200));		
	}
	public static Bitmap decodeFile(Context context,int resId) {
		try {
				// decode image size
				mcontext=context;
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeResource(mcontext.getResources(), resId, o);
				// Find the correct scale value. It should be the power of 2.
				final int REQUIRED_SIZE = 200;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true){
					 if (width_tmp / 2 < REQUIRED_SIZE
					 || height_tmp / 2 < REQUIRED_SIZE)
					 break;
					 width_tmp /= 2;
					 height_tmp /= 2;
					 scale++;
				}
				// decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				return BitmapFactory.decodeResource(mcontext.getResources(), resId, o2);			
			} catch (Exception e) {				
		}
		return null;
	}
	public static Bitmap getRoundedShape(Bitmap scaleBitmapImage,int width) {
		 // TODO Auto-generated method stub
		 int targetWidth = width;
		 int targetHeight = width;
		 Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
		 targetHeight,Bitmap.Config.ARGB_8888);

		 Canvas canvas = new Canvas(targetBitmap);
		 Path path = new Path();
		 path.addCircle(((float) targetWidth - 1) / 2,
		 ((float) targetHeight - 1) / 2,
		 (Math.min(((float) targetWidth),
		 ((float) targetHeight)) / 2),
		 Path.Direction.CCW);
		 canvas.clipPath(path);
		 Bitmap sourceBitmap = scaleBitmapImage;
		 canvas.drawBitmap(sourceBitmap,
		 new Rect(0, 0, sourceBitmap.getWidth(),
		 sourceBitmap.getHeight()),
		 new Rect(0, 0, targetWidth,
		 targetHeight), null);
		 return targetBitmap;
	}
}
package com.b2infosoft.gt.rajesh.fragments.dashboard;


import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.SlideMenu;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.server.JSONParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class PunchIn extends Fragment implements LocationListener {
    public static final String TAG = PunchIn.class.getSimpleName();
    private static SharedPreferences settings;
    JSONParser jsonParser = new JSONParser();
    private double latitude;
    private String location = "";
    private LocationManager locationManager;
    private double longitude;
    private View mView;
    private ProgressBar progressBar;
    private String provider;
    private Button start;
    private Button choose_client;

    private void navigate(final Fragment paramFragment, final String paramString)
    {
        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {
                SlideMenu.setActiveFragment(PunchIn.this.getFragmentManager(), paramFragment, paramString);
            }
        });
    }

    public static PunchIn newInstance()
    {
        return new PunchIn();
    }

    public String getCompleteAddressString(double paramDouble1, double paramDouble2)
    {
        String localObject = "";
        Geocoder localGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        try
        {
            List localList = localGeocoder.getFromLocation(paramDouble1, paramDouble2, 1);
            if (localList != null)
            {
                Address localAddress = (Address)localList.get(0);
                StringBuilder localStringBuilder = new StringBuilder("");
                for (int i = 0; i < localAddress.getMaxAddressLineIndex(); i++) {
                    localStringBuilder.append(localAddress.getAddressLine(i)).append("\n");
                }
                String str = localStringBuilder.toString();
                localObject = str;
            }
            return localObject;
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
        return localObject;
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
    {
        settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
        this.locationManager = ((LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE));
        Criteria localCriteria = new Criteria();
        localCriteria.setSpeedRequired(false);
        localCriteria.setAccuracy(1);
        localCriteria.setCostAllowed(true);
        localCriteria.setBearingAccuracy(3);
        localCriteria.setAltitudeRequired(false);
        this.provider = this.locationManager.getBestProvider(localCriteria, false);
        View view = paramLayoutInflater.inflate(R.layout.fragment_dashboard_new_punch_in_layout, paramViewGroup, false);
        this.progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        this.mView = view.findViewById(R.id.punch_in_view);
        this.start = (Button) view.findViewById(R.id.punch_in_button);
        this.choose_client = (Button) view.findViewById(R.id.choose_client);
        choose_client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PunchIn.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
            }
        });
        this.start.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                if (PunchIn.this.locationManager.isProviderEnabled("gps"))
                {
                    Location localLocation = PunchIn.this.locationManager.getLastKnownLocation(PunchIn.this.provider);
                    if (localLocation != null)
                    {
                        PunchIn.this.latitude = localLocation.getLatitude();
                        PunchIn.this.longitude= localLocation.getLongitude();
                        PunchIn.this.location = PunchIn.this.getCompleteAddressString(PunchIn.this.latitude, PunchIn.this.longitude);
                        PunchIn.CheckIn localCheckIn = new PunchIn.CheckIn();
                        String[] arrayOfString = new String[7];
                        arrayOfString[0] = settings.getString("user_id", "");
                        arrayOfString[1] = String.valueOf(PunchIn.this.latitude);
                        arrayOfString[2] = String.valueOf(PunchIn.this.longitude);
                        arrayOfString[3] = "PUNCH IN";
                        arrayOfString[4] = PunchIn.this.location;
                        arrayOfString[5] = MySharedPreferences.getSharedPreferences(PunchIn.this.getActivity(), "client_id");
                        arrayOfString[6] = "1";
                        localCheckIn.execute(arrayOfString);
                        return;
                    }
                    AlertDialog.Builder localBuilder = new AlertDialog.Builder(PunchIn.this.getActivity());
                    localBuilder.setTitle("Alert");
                    localBuilder.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
                    localBuilder.setNegativeButton("No", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
                        {
                            paramAnonymous2DialogInterface.dismiss();
                        }
                    });
                    localBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
                        {
                            PunchIn.CheckIn localCheckIn = new PunchIn.CheckIn();
                            String[] arrayOfString = new String[7];
                            arrayOfString[0] = settings.getString("user_id", "");
                            arrayOfString[1] = String.valueOf(PunchIn.this.latitude);
                            arrayOfString[2] = String.valueOf(PunchIn.this.longitude);
                            arrayOfString[3] = "PUNCH IN";
                            arrayOfString[4] = PunchIn.this.location;
                            arrayOfString[5] = MySharedPreferences.getSharedPreferences(PunchIn.this.getActivity(), "client_id");
                            arrayOfString[6] = "1";
                            localCheckIn.execute(arrayOfString);
                        }
                    });
                    localBuilder.create().show();
                    return;
                }
                PunchIn.this.showSettingsAlert();
            }
        });
        return view;
    }

    public void onLocationChanged(Location paramLocation)
    {
        this.latitude = paramLocation.getLatitude();
        this.longitude = paramLocation.getLongitude();
        this.location = getCompleteAddressString(this.latitude, this.longitude);
    }

    public void onProviderDisabled(String paramString) {}

    public void onProviderEnabled(String paramString)
    {
        onLocationChanged(this.locationManager.getLastKnownLocation(paramString));
    }

    public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle) {}

    public void showNoInternetConncetAlert()
    {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle("No Internet Connection");
        localBuilder.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");
        localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
                if (!new Network(PunchIn.this.getActivity()).isConnectingToInternet()) {
                    PunchIn.this.showNoInternetConncetAlert();
                }
            }
        });
        localBuilder.show();
    }

    public void showSettingsAlert()
    {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle("GPS is settings");
        localBuilder.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        localBuilder.setPositiveButton("Settings", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
                Intent localIntent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                PunchIn.this.getActivity().startActivityForResult(localIntent, 1);
            }
        });
        localBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
                paramAnonymousDialogInterface.cancel();
            }
        });
        localBuilder.show();
    }

    private class CheckIn extends AsyncTask<String, String, String> {
        private String action = "";
        private String message = "";

        private CheckIn() {}

        protected String doInBackground(String... paramVarArgs)
        {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(new BasicNameValuePair("user_id", paramVarArgs[0]));
            localArrayList.add(new BasicNameValuePair("company_id", settings.getString("company_id", "")));
            localArrayList.add(new BasicNameValuePair("latitude", paramVarArgs[1]));
            localArrayList.add(new BasicNameValuePair("longitude", paramVarArgs[2]));
            localArrayList.add(new BasicNameValuePair("check", paramVarArgs[3]));
            localArrayList.add(new BasicNameValuePair("location", paramVarArgs[4]));
            localArrayList.add(new BasicNameValuePair("client_id", paramVarArgs[5]));
            localArrayList.add(new BasicNameValuePair("check_out_status", paramVarArgs[6]));
            JSONObject localJSONObject = PunchIn.this.jsonParser.makeHttpRequest(Admin.getUrlForCheck_In_Out(), "POST", localArrayList);
            Log.d("Response on start", localJSONObject.toString());
            try
            {
                if (localJSONObject.getInt("success") == 1)
                {
                    if (localJSONObject.has("action")) {
                        this.action = localJSONObject.getString("action");
                    }
                    if (localJSONObject.has("client_id")) {
                        PunchIn.this.getActivity().getSharedPreferences("rajesh", 0).edit().putString("client_id", localJSONObject.getString("client_id")).commit();
                    }
                    if (localJSONObject.has("message")) {
                        this.message = localJSONObject.getString("message");
                    }
                }
            }
            catch (JSONException localJSONException)
            {
                    localJSONException.printStackTrace();
                    Log.d("My Exception", localJSONException.toString());
            }
            return null;
        }

        protected void onPostExecute(String paramString)
        {
            PunchIn.this.progressBar.setVisibility(View.GONE);
            PunchIn.this.mView.setVisibility(View.VISIBLE);
            if (this.action.equalsIgnoreCase("PUNCH IN")) {
                new EmpStatus().execute(new String[0]);
            } else if (!this.action.equalsIgnoreCase("PUNCH OUT")) {
                Toast.makeText(PunchIn.this.getActivity(), this.message, Toast.LENGTH_SHORT).show();
            }
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            PunchIn.this.progressBar.setVisibility(View.VISIBLE);
            PunchIn.this.mView.setVisibility(View.GONE);
        }
    }

    private class EmpStatus extends AsyncTask<String, String, JSONObject>{
        private EmpStatus() {}

        protected JSONObject doInBackground(String... paramVarArgs)
        {
            List<NameValuePair> pairs = new ArrayList();
            pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
            return PunchIn.this.jsonParser.makeHttpRequest(Admin.getUrlForEmpStatus(), "POST", pairs);
        }

        protected void onPostExecute(JSONObject object)
        {
            PunchIn.this.progressBar.setVisibility(View.GONE);
            PunchIn.this.mView.setVisibility(View.VISIBLE);

            if(!TextUtils.isEmpty( object.toString())) {
                Log.d("My EmpStatus", object.toString());
            }


            int schedule = 0;
            int punch = 0;
            int client = 0;
            if (object != null) {
                try {
                    if (object.has(Admin.TAG_SUCCESS)) {
                        int success = object.getInt(Admin.TAG_SUCCESS);
                    }
                    if (object.has(Admin.SCHEDULE_STATUS)) {
                        schedule = object.getInt(Admin.SCHEDULE_STATUS);
                    }
                    if (object.has(Admin.CLIENT_STATUS)) {
                        client = object.getInt(Admin.CLIENT_STATUS);
                    }
                    if (object.has(Admin.PUNCH_STATUS)) {
                        punch = object.getInt(Admin.PUNCH_STATUS);
                    }

                    String client_name_ = "";
                    String client_id_ = "";
                    if (client == 1) {
                        if (object.has(Admin.CLIENT_NAME)) {
                            client_name_ = object.getString(Admin.CLIENT_NAME);
                            MySharedPreferences.setSharedPreferences(PunchIn.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        }
                        if (object.has(Admin.CLIENT_ID)) {
                            MySharedPreferences.setSharedPreferences(PunchIn.this.getActivity(), Admin.CLIENT_ID, object.getString(Admin.CLIENT_ID));
                        }
                        PunchIn.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                        MySharedPreferences.setSharedPreferences(getActivity(), Admin.PUNCH_STATUS, punch+"");
                    } else {
                        MySharedPreferences.setSharedPreferences(PunchIn.this.getActivity(), Admin.CLIENT_ID, client_id_);
                        MySharedPreferences.setSharedPreferences(PunchIn.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        MySharedPreferences.setSharedPreferences(getActivity(), Admin.PUNCH_STATUS, punch+"");
                        PunchIn.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON", e.toString());
                }
            }
            /*
                System.out.println("SCHEDULE  "+TAG+schedule);
                System.out.println("PUNCH "+TAG+punch);
                System.out.println("CLIENT "+TAG+client);
            */
           /* if (schedule == 1 && client == 0) {
                PunchIn.this.navigate(ScheduleActive.newInstance(), ScheduleActive.TAG);
            } else*/ if (schedule != 0 || client != 0) {
                switch (punch) {
                    case 0:
                        //PunchIn.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                        break;
                    case 1:
                        PunchIn.this.navigate(SessionStart.newInstance(), SessionStart.TAG);
                        break;
                    case 2:
                        PunchIn.this.navigate(SessionBreak.newInstance(), SessionBreak.TAG);
                        break;
                    case 3:
                        if (client != 1) {
                            PunchIn.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
                            break;
                        } else {
                            PunchIn.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                            break;
                        }
                    default:
                        break;
                }
            } else {
                PunchIn.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
            }
            if (punch == 1) {
                MySharedPreferences.setSharedPreferences(PunchIn.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_START);
            } else {
                MySharedPreferences.setSharedPreferences(PunchIn.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_END);
            }
        }

        protected void onPreExecute()
        {
            PunchIn.this.progressBar.setVisibility(View.VISIBLE);
            PunchIn.this.mView.setVisibility(View.GONE);
        }
    }
}
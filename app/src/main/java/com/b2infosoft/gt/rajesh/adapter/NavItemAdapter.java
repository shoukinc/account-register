package com.b2infosoft.gt.rajesh.adapter;

import java.util.List;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.rajesh.modals.NavItems;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavItemAdapter extends ArrayAdapter<NavItems>{
	Context context;
	int resourceLayouts;
	List<NavItems> navItems;
	DisplayMetrics metrics;
	public NavItemAdapter(Context context, int resourceLayouts,
			 List<NavItems> navItems) {
			super(context, resourceLayouts, navItems);
			this.context=context;
			this.resourceLayouts=resourceLayouts;
			this.navItems=navItems;
	}
	public NavItemAdapter(Context context, int resourceLayouts,List<NavItems> navItems,DisplayMetrics metrics) {
			super(context, resourceLayouts, navItems);
			this.context=context;
			this.resourceLayouts=resourceLayouts;
			this.navItems=navItems;
			this.metrics=metrics;
	}
	@SuppressLint("ViewHolder") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view=View.inflate(context, resourceLayouts, null);				
		ImageView nav_icon=(ImageView)view.findViewById(R.id.nav_icon);
		TextView nav_title=(TextView)view.findViewById(R.id.nav_title);
		
		NavItems navItem=navItems.get(position);		
		if(position==0){
			//nav_icon.setImageBitmap(RoundImage.getRoundedShape(RoundImage.decodeFile(context, navItem.getResIcon()),36));
			nav_icon.setImageResource(navItem.getResIcon());			
			//nav_icon.setBackgroundResource(R.drawable.frame);
		}else{
			nav_icon.setImageResource(navItem.getResIcon());			
		}
		nav_title.setText(navItem.getTitle());
		nav_title.setTypeface(Admin.getFontRoboto(context));		
		int WIDTH=metrics.widthPixels;
		int HEIGHT=metrics.heightPixels;
		if(WIDTH<=320 && HEIGHT<=480){
			nav_title.setTextSize(9);
	    }else if( (WIDTH>320 && WIDTH<=480)&& (HEIGHT>480 && HEIGHT<=800)){
			nav_title.setTextSize(9);
	    }else if((WIDTH>480 && WIDTH<=600)&& (HEIGHT>800 && HEIGHT<=1024)){
			nav_title.setTextSize(9);
	    }else if((WIDTH>600 && WIDTH<=800)&& (HEIGHT>1024 && HEIGHT<=1280)){
			nav_title.setTextSize(9);
	    }else if((WIDTH>800 && WIDTH<=1080)&& (HEIGHT>1280 && HEIGHT<=1920)){
			nav_title.setTextSize(9);
	    }else{
			nav_title.setTextSize(9);
	    }
		return view;
	}
}

package com.b2infosoft.gt.rajesh.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.rajesh.sign_1.FingerPaint;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class DailyLog extends Fragment{
	private static SharedPreferences settings;	
	public static final String TAG=DailyLog.class.getSimpleName();
	JSONParser jsonParser = new JSONParser();
	public static DailyLog newInstance() {
		return new DailyLog();
	}
	Button button_submit,sign_emp;
	EditText logs;
	View toast_view;
	private Dialog alertDialog;

	private ImageView mImageGoodSign;
	private ProgressBar progress_bar;
	private LinearLayout mTopLayout;


	@SuppressLint("NewApi") @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			View view=inflater.inflate(R.layout.fragments_outer_daily_log_layout, container, false);
			// initialization

		mImageGoodSign=(ImageView)view.findViewById(R.id.mGoodSignImg);
		progress_bar=(ProgressBar)view.findViewById(R.id.progress_bar);
		mTopLayout=(LinearLayout) view.findViewById(R.id.mTopLayout);

		alertDialog = new Dialog(getActivity());
		alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		alertDialog.setContentView(R.layout.custom_dialog_box_2);
		alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


		getActivity().setTitle((MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME)).toUpperCase(Locale.getDefault()));
			toast_view=inflater.inflate(R.layout.custom_toast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
			settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
			logs=(EditText)view.findViewById(R.id.daily_log_details);
			button_submit=(Button)view.findViewById(R.id.daily_log_submit);
			button_submit.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {					
					if(!isEmpty(logs)){

						progress_bar.setVisibility(VISIBLE);
						new DailyLogUpdate().execute(
								new String[] {
										settings.getString(Admin.USER_ID, ""),
										MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
										"1",
										logs.getText().toString().trim(),
										MySharedPreferences.getSharedPreferences(getActivity(), Admin.COMPANY_ID)
								}
								);
					}
				}
			});
			sign_emp=(Button)view.findViewById(R.id.daily_sign_employee);
			sign_emp.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {					
					//Intent intent = new Intent(getActivity().getBaseContext(),DrawingActivity.class);
					Intent intent = new Intent(getActivity().getBaseContext(),FingerPaint.class);					
					startActivity(intent);					
				}
			});
			return view;
	}
	public class DailyLogUpdate extends AsyncTask<String, Void, Integer>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			alertDialog.show();
		}
		@Override
		protected Integer doInBackground(String... args) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(Admin.USER_ID, args[0]));
			params.add(new BasicNameValuePair(Admin.CLIENT_ID, args[1]));
			params.add(new BasicNameValuePair(Admin.STATUS, args[2]));
			params.add(new BasicNameValuePair(Admin.LOG, args[3]));
			params.add(new BasicNameValuePair(Admin.COMPANY_ID, args[4]));
			// getting JSON Object
			// Note that create product url accepts POST method			
			JSONObject json = jsonParser.makeHttpRequest(
					Admin.getUrlForDailyLog(), "POST", params);
			// check log cat from response
			try {
				int success = json.getInt(Admin.TAG_SUCCESS);
				if (success == 1) {
					return 1;
				} else {
					return 0;
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.d("My Exception", e.toString());
			}
			return 0;
		}
		@Override
		protected void onPostExecute(Integer result) {
			if(result==1){

				logs.setText("");
				mTopLayout.setVisibility(GONE);
				mImageGoodSign.setVisibility(VISIBLE);
				progress_bar.setVisibility(GONE);
				zoom_in();

				Thread th = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(4000);
						} catch (InterruptedException e) {

						}
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {

								try {
									alertDialog.dismiss();
									getActivity().onBackPressed();
								} catch (Exception e) {
									e.printStackTrace();
								}

							}
						});

					}
				});
				th.start();


				/*
					Toast toast =new Toast(getActivity().getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
					toast.setView(toast_view);
					toast.show();
				*/
			}
			super.onPostExecute(result);

		}
	}
	public boolean isEmpty(EditText text){
		String str=text.getText().toString().trim();
		if(TextUtils.isEmpty(str)){
			text.setError("Please Fill Daily Report");
			return true;
		}
		return false;
	}

	public void zoom_in() {
		if(mImageGoodSign!=null) {
			Animation animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in);
			mImageGoodSign.startAnimation(animation1);
		}
	}
}

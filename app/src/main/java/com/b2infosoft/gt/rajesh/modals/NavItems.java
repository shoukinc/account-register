package com.b2infosoft.gt.rajesh.modals;

public class NavItems {
	private String title;
	private int resIcon;
	public NavItems(String title, int resIcon) {
		super();
		this.title = title;
		this.resIcon = resIcon;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getResIcon() {
		return resIcon;
	}
	public void setResIcon(int resIcon) {
		this.resIcon = resIcon;
	}
}

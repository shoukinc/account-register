package com.b2infosoft.gt.rajesh.fragments;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.server.JSONParser;

import de.ailis.pherialize.MixedArray;
import de.ailis.pherialize.Pherialize;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class DailyReport extends Fragment {

	public static DailyReport newInstance() {
		return new DailyReport();
	}

	private SharedPreferences settings;
	private ProgressDialog progressDialog;
	public static final String TAG = DailyReport.class.getSimpleName();
	private final JSONParser jsonParser = new JSONParser();
	private RelativeLayout mainLayout;
	TextView textView;
	private Button button;
	private Button button_out;
	private ScrollView scrollView;
	private LinearLayout linearLayout;
	private Hashtable<String, Hashtable<String, Object[]>> dynamic_form_data = new Hashtable<String, Hashtable<String, Object[]>>();
	private List<NameValuePair> params_main = new ArrayList<NameValuePair>();
	private EditText vehicle_no;
	View toast_view;
	private Dialog alertDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(
				R.layout.fragments_outer_daily_report_layout, container, false);
		toast_view=inflater.inflate(R.layout.custom_toast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
		settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
		getActivity().setTitle(
				MySharedPreferences.getSharedPreferences(getActivity(),
						Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));

		alertDialog = new Dialog(getActivity());
		alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		alertDialog.setContentView(R.layout.custom_dialog_box_1);
		alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		final Animation shake = AnimationUtils.loadAnimation(getActivity(),
				R.anim.shake);
		mainLayout = (RelativeLayout) view
				.findViewById(R.id.daily_report_form_layout);
		scrollView = new ScrollView(getActivity());
		scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		linearLayout = new LinearLayout(getActivity());
		linearLayout.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		textView = new TextView(getActivity());
		textView.setText("PARKING LOG");
		textView.setGravity(Gravity.CENTER_HORIZONTAL);
		textView.setTypeface(Admin.getFontRoboto(getActivity()));
		LayoutParams textparams = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		textparams.bottomMargin = 25;
		textparams.topMargin = 15;
		textparams.leftMargin = 0;
		textparams.rightMargin = 0;
		textView.setLayoutParams(textparams);
		linearLayout.addView(textView);

		vehicle_no = new EditText(getActivity());
		vehicle_no.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		vehicle_no.setHint("VEHICLE NUMBER");
		vehicle_no.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				vehicle_no.setError(null);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		linearLayout.addView(vehicle_no);

		button = new Button(getActivity());
		LayoutParams buttonparams = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		buttonparams.bottomMargin = 25;
		buttonparams.topMargin = 15;
		buttonparams.leftMargin = 25;
		buttonparams.rightMargin = 25;
		button.setLayoutParams(buttonparams);
		button.setPadding(10, 10, 10, 10);
		button.setText("IN");
		button.setTypeface(Admin.getFontRoboto(getActivity()));
		button.setBackgroundResource(R.drawable.button_background);
		button.setTextColor(Color.parseColor("#ffffff"));
		button.setOnClickListener(new View.OnClickListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(View v) {
				boolean validation = false;

				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair(Admin.COMPANY_ID, settings
						.getString(Admin.COMPANY_ID, "")));
				params.add(new BasicNameValuePair(Admin.CLIENT_ID, settings
						.getString(Admin.CLIENT_ID, "")));
				params.add(new BasicNameValuePair(Admin.USER_ID, settings
						.getString(Admin.USER_ID, "")));
				params.add(new BasicNameValuePair(Admin.ACTION,"0"));
				String vehicle_=vehicle_no.getText().toString().trim();
				params.add(new BasicNameValuePair(Admin.VEHICLE_NO, vehicle_));								
				if(vehicle_.length()==0){
					validation=true;
					vehicle_no.setError("Please Enter Vehicle Number");
				}			
				int i = 0;									
				if (dynamic_form_data.size() > 0 && !validation) {
					Enumeration<String> enum_key = dynamic_form_data.keys();
					while (enum_key.hasMoreElements()) {
						String string = (String) enum_key.nextElement();
						// Hashtable<String, Object>
						// dynamic_form_field=(Hashtable<String,
						// Object>)dynamic_form_data.get(string);
						Hashtable<String, Object[]> dynamic_form_field = (Hashtable<String, Object[]>) dynamic_form_data
								.get(string);
						if (dynamic_form_field.size() > 0) {
							Enumeration<String> enum_key_field = dynamic_form_field
									.keys();
							while (enum_key_field.hasMoreElements()) {
								String string2 = (String) enum_key_field
										.nextElement();
								if (string2.equalsIgnoreCase(Admin.TEXTFIELD)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									String mandatory = object[1].toString();
									EditText text = (EditText) object[0];
									String value = text.getText().toString()
											.trim();
									if (mandatory.equalsIgnoreCase("Mandatory")) {
										if (value.length() == 0) {
											validation = true;
											text.startAnimation(shake);
											text.setError("Field cannot be left blank.");
											// text.setError(Html.fromHtml("<font color='green'>Please Fill Field</font>"));
										}
									}
									params.add(new BasicNameValuePair(string,
											value));
									i++;
								} else if (string2
										.equalsIgnoreCase(Admin.TEXTAREA)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									EditText text = (EditText) object[0];
									String mandatory = object[1].toString();
									String value = text.getText().toString()
											.trim();
									if (mandatory.equalsIgnoreCase("Mandatory")) {
										if (value.length() == 0) {
											validation = true;
											text.startAnimation(shake);
											text.setError("Field cannot be left blank.");
										}
									}
									params.add(new BasicNameValuePair(string,
											value));
									i++;
								} else if (string2
										.equalsIgnoreCase(Admin.SELECT)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									Spinner spinner = (Spinner) object[0];
									String mandatory = object[1].toString();
									int value = spinner
											.getSelectedItemPosition();
									if (mandatory.equalsIgnoreCase("Mandatory")) {
										if (value == 0) {
											validation = true;
											TextView text = (TextView) spinner
													.getSelectedView();
											text.setError("Field cannot be Unselect.");
											text.startAnimation(shake);
										}
									}
									params.add(new BasicNameValuePair(string,
											spinner.getSelectedItem()
													.toString()));
									i++;
								} else if (string2
										.equalsIgnoreCase(Admin.RADIO)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									RadioGroup radioGroup = (RadioGroup) object[0];
									String mandatory = object[1].toString();

									int selected = radioGroup
											.getCheckedRadioButtonId();
									RadioButton radioButton = (RadioButton) getActivity()
											.findViewById(selected);
									params.add(new BasicNameValuePair(string,
											radioButton.getText().toString()));
									i++;
								} else if (string2.endsWith(Admin.CHECKBOX)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									Hashtable<String, CheckBox> che = (Hashtable<String, CheckBox>) object[0];
									String mandatory = object[1].toString();

									Enumeration<CheckBox> e = che.elements();
									String selected_check_box = "";
									while (e.hasMoreElements()) {
										CheckBox checkBox = (CheckBox) e
												.nextElement();
										if (checkBox.isChecked()) {
											selected_check_box += checkBox
													.getText().toString() + ",";
										}
									}
									if (selected_check_box.length() > 0)
										selected_check_box = selected_check_box
												.substring(0,
														selected_check_box
																.length() - 1);
									params.add(new BasicNameValuePair(string,
											selected_check_box));
									i++;
								}
							}
						}
					}
				}
				params.add(new BasicNameValuePair(Admin.MAX_FORM_FIELD, String
						.valueOf(i)));
				if (params != null) {
					if (validation) {
					} else {
						DailyReport.this.params_main = params;
						fillForms();
					}
				}
			}
		});

		button_out = new Button(getActivity());
		LayoutParams buttonparams_1 = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		buttonparams_1.bottomMargin = 25;
		buttonparams_1.topMargin = 15;
		buttonparams_1.leftMargin = 25;
		buttonparams_1.rightMargin = 25;
		button_out.setLayoutParams(buttonparams);
		button_out.setPadding(10, 10, 10, 10);
		button_out.setText("OUT");
		button_out.setTypeface(Admin.getFontRoboto(getActivity()));
		button_out.setBackgroundResource(R.drawable.button_background);
		button_out.setTextColor(Color.parseColor("#ffffff"));
		button_out.setOnClickListener(new View.OnClickListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(View v) {
				boolean validation = false;

				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair(Admin.COMPANY_ID, settings
						.getString(Admin.COMPANY_ID, "")));
				params.add(new BasicNameValuePair(Admin.CLIENT_ID, settings
						.getString(Admin.CLIENT_ID, "")));
				params.add(new BasicNameValuePair(Admin.USER_ID, settings
						.getString(Admin.USER_ID, "")));

				params.add(new BasicNameValuePair(Admin.ACTION,"1"));
				String vehicle_=vehicle_no.getText().toString().trim();
				params.add(new BasicNameValuePair(Admin.VEHICLE_NO, vehicle_));								
				if(vehicle_.length()==0){
					validation=true;
					vehicle_no.setError("Please Enter Vehicle Number");
				}				
 				int i = 0;
				if (dynamic_form_data.size() > 0 && !validation) {
					Enumeration<String> enum_key = dynamic_form_data.keys();
					while (enum_key.hasMoreElements()) {
						String string = (String) enum_key.nextElement();
						// Hashtable<String, Object>
						// dynamic_form_field=(Hashtable<String,
						// Object>)dynamic_form_data.get(string);
						Hashtable<String, Object[]> dynamic_form_field = (Hashtable<String, Object[]>) dynamic_form_data
								.get(string);
						if (dynamic_form_field.size() > 0) {
							Enumeration<String> enum_key_field = dynamic_form_field
									.keys();
							while (enum_key_field.hasMoreElements()) {
								String string2 = (String) enum_key_field
										.nextElement();
								if (string2.equalsIgnoreCase(Admin.TEXTFIELD)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									String mandatory = object[1].toString();
									EditText text = (EditText) object[0];
									String value = text.getText().toString()
											.trim();
									if (mandatory.equalsIgnoreCase("Mandatory")) {
										if (value.length() == 0) {
											validation = true;
											text.startAnimation(shake);
											text.setError("Field cannot be left blank.");
											// text.setError(Html.fromHtml("<font color='green'>Please Fill Field</font>"));
										}
									}
									params.add(new BasicNameValuePair(string,
											value));
									i++;
								} else if (string2
										.equalsIgnoreCase(Admin.TEXTAREA)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									EditText text = (EditText) object[0];
									String mandatory = object[1].toString();
									String value = text.getText().toString()
											.trim();
									if (mandatory.equalsIgnoreCase("Mandatory")) {
										if (value.length() == 0) {
											validation = true;
											text.startAnimation(shake);
											text.setError("Field cannot be left blank.");
										}
									}
									params.add(new BasicNameValuePair(string,
											value));
									i++;
								} else if (string2
										.equalsIgnoreCase(Admin.SELECT)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									Spinner spinner = (Spinner) object[0];
									String mandatory = object[1].toString();
									int value = spinner
											.getSelectedItemPosition();
									if (mandatory.equalsIgnoreCase("Mandatory")) {
										if (value == 0) {
											validation = true;
											TextView text = (TextView) spinner
													.getSelectedView();
											text.setError("Field cannot be Unselect.");
											text.startAnimation(shake);
										}
									}
									params.add(new BasicNameValuePair(string,
											spinner.getSelectedItem()
													.toString()));
									i++;
								} else if (string2
										.equalsIgnoreCase(Admin.RADIO)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									RadioGroup radioGroup = (RadioGroup) object[0];
									String mandatory = object[1].toString();

									int selected = radioGroup
											.getCheckedRadioButtonId();
									RadioButton radioButton = (RadioButton) getActivity()
											.findViewById(selected);
									params.add(new BasicNameValuePair(string,
											radioButton.getText().toString()));
									i++;
								} else if (string2.endsWith(Admin.CHECKBOX)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									Hashtable<String, CheckBox> che = (Hashtable<String, CheckBox>) object[0];
									String mandatory = object[1].toString();

									Enumeration<CheckBox> e = che.elements();
									String selected_check_box = "";
									while (e.hasMoreElements()) {
										CheckBox checkBox = (CheckBox) e
												.nextElement();
										if (checkBox.isChecked()) {
											selected_check_box += checkBox
													.getText().toString() + ",";
										}
									}
									if (selected_check_box.length() > 0)
										selected_check_box = selected_check_box
												.substring(0,
														selected_check_box
																.length() - 1);
									params.add(new BasicNameValuePair(string,
											selected_check_box));
									i++;
								}
							}
						}
					}
				}
				params.add(new BasicNameValuePair(Admin.MAX_FORM_FIELD, String
						.valueOf(i)));
				if (params != null) {
					if (validation) {
					} else {
						DailyReport.this.params_main = params;
						fillForms();
					}
				}
			}
		});
		scrollView.addView(linearLayout);
		mainLayout.addView(scrollView);
		getForms();
		return view;
	}

	@SuppressWarnings("unchecked")
	private void fillForms() {
		if (!new Network(getActivity()).isConnectingToInternet()) {
			showNoInternetConncetAlert();
		} else {
			new FillForm().execute(new List[] { params_main });
		}
	}

	private void getForms() {
		if (!new Network(getActivity()).isConnectingToInternet()) {
			showNoInternetConncetAlert_1();
		} else {
			new GetForm().execute(new String[] { settings.getString(
					Admin.COMPANY_ID, "") });
		}
	}

	public void showNoInternetConncetAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
		// Setting Dialog Title
		alertDialog.setTitle("No Internet Connection");

		// Setting Dialog Message
		alertDialog
				.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");

		// On pressing Settings button
		alertDialog.setPositiveButton("Retry",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						fillForms();
					}
				});
		// Showing Alert Message
		alertDialog.show();
	}

	public void showNoInternetConncetAlert_1() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
		// Setting Dialog Title
		alertDialog.setTitle("No Internet Connection");

		// Setting Dialog Message
		alertDialog
				.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");

		// On pressing Settings button
		alertDialog.setPositiveButton("Retry",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						getForms();
					}
				});
		// Showing Alert Message
		alertDialog.show();
	}

	private class GetForm extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... args) {
			try {
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				//params.add(new BasicNameValuePair(Admin.COMPANY_ID, args[0]));
				//final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForParkingDailyForm(), "GET", params);

				params.add(new BasicNameValuePair("form_id", String.valueOf(fragment_list_option.FORM_ID)));
				 JSONObject json2 = new JSONObject();
				if(fragment_list_option.FORM_TYPE==1){
					json2 = jsonParser.makeHttpRequest(Admin.getUrlForDynamicTwoColoum(), "POST", params);
				} else if(fragment_list_option.FORM_TYPE==2){
					json2 = jsonParser.makeHttpRequest(Admin.getUrlForDynamicMultiColoum(), "POST", params);
				}else{
					json2 = jsonParser.makeHttpRequest(Admin.getUrlForParkingDailyForm(), "GET", params);
				}
				final JSONObject json = json2;
				// check log cat from response

				Iterator<String> iterator = json.keys();
				TreeSet<String> set = new TreeSet<String>();
				while (iterator.hasNext()) {
					String key = iterator.next();
					set.add(key);
					Log.d(Admin.TAG_SUCCESS, key);
				}
				final Iterator<String> iterator1 = set.iterator();

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (json.has(Admin.HEADING)) {
							try {
								textView.setText(json.getString(Admin.HEADING));
								json.remove(Admin.HEADING);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				});
				json.remove("success");

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						int ID = 1001;
						while (iterator1.hasNext()) {
							try {
								// Hashtable<String, Object> data_dynamic=new
								// Hashtable<String, Object>();
								Hashtable<String, Object[]> data_dynamic = new Hashtable<String, Object[]>();
								String str = iterator1.next();
								LinearLayout layout_inner = new LinearLayout(
										getActivity());
								layout_inner.setLayoutParams(new LayoutParams(
										LayoutParams.MATCH_PARENT,
										LayoutParams.WRAP_CONTENT));
								layout_inner
										.setOrientation(LinearLayout.VERTICAL);
								layout_inner.setPadding(0, 5, 0, 5);
								if (json.has(str)) {
									String data = json.getString(str);
									MixedArray mixedArray = Pherialize
											.unserialize(data).toArray();
									if (mixedArray.getString(1)
											.equalsIgnoreCase(Admin.TEXTFIELD)) {
										TextView txt = new TextView(
												getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin
												.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										final EditText textfield = new EditText(
												getActivity());
										textfield
												.setLayoutParams(new LayoutParams(
														LayoutParams.MATCH_PARENT,
														LayoutParams.WRAP_CONTENT));
										textfield.setHint(mixedArray.getString(
												2).toUpperCase(
												Locale.getDefault()));
										textfield.setSingleLine(true);
										textfield.setId(ID++);
										textfield
												.addTextChangedListener(new TextWatcher() {
													@Override
													public void onTextChanged(
															CharSequence s,
															int start,
															int before,
															int count) {
														textfield
																.setError(null);
													}

													@Override
													public void beforeTextChanged(
															CharSequence s,
															int start,
															int count, int after) {

													}

													@Override
													public void afterTextChanged(
															Editable s) {

													}
												});
										textfield.setTypeface(Admin
												.getFontRoboto(getActivity()));
										layout_inner.addView(textfield);
										// data_dynamic.put(Admin.TEXTFIELD,
										// textfield);
										data_dynamic
												.put(Admin.TEXTFIELD,
														new Object[] {
																textfield,
																mixedArray
																		.getString(3) });
									} else if (mixedArray.getString(1)
											.equalsIgnoreCase(Admin.TEXTAREA)) {
										TextView txt = new TextView(
												getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin
												.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										final EditText textarea = new EditText(
												getActivity());
										textarea.setSingleLine(false);
										textarea.setId(ID++);
										textarea.setTypeface(Admin
												.getFontRoboto(getActivity()));
										textarea.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
										textarea.setLayoutParams(new LayoutParams(
												LayoutParams.MATCH_PARENT,
												LayoutParams.WRAP_CONTENT));
										textarea.setHint(mixedArray
												.getString(2).toUpperCase(
														Locale.getDefault()));
										textarea.addTextChangedListener(new TextWatcher() {

											@Override
											public void onTextChanged(
													CharSequence s, int start,
													int before, int count) {
												textarea.setError(null);
											}

											@Override
											public void beforeTextChanged(
													CharSequence s, int start,
													int count, int after) {

											}

											@Override
											public void afterTextChanged(
													Editable s) {

											}
										});
										layout_inner.addView(textarea);
										// data_dynamic.put(Admin.TEXTAREA,
										// textarea);
										data_dynamic
												.put(Admin.TEXTAREA,
														new Object[] {
																textarea,
																mixedArray
																		.getString(3) });

									} else if (mixedArray.getString(1)
											.equalsIgnoreCase(Admin.SELECT)) {
										TextView txt = new TextView(
												getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin
												.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										String string_array[] = mixedArray
												.getString(2).split("\n");
										ArrayList<String> spinnerArray = new ArrayList<String>();
										spinnerArray
												.add(("Select " + mixedArray
														.getString(0)));
										for (String s : string_array) {
											spinnerArray.add(s);
										}
										Spinner spinner = new Spinner(
												getActivity());
										ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
												getActivity(),
												android.R.layout.simple_spinner_dropdown_item,
												spinnerArray);
										spinner.setLayoutParams(new LayoutParams(
												LayoutParams.MATCH_PARENT,
												LayoutParams.WRAP_CONTENT));
										spinner.setAdapter(arrayAdapter);
										spinner.setId(ID++);
										layout_inner.addView(spinner);
										// data_dynamic.put(Admin.SELECT,spinner);
										data_dynamic
												.put(Admin.SELECT,
														new Object[] {
																spinner,
																mixedArray
																		.getString(3) });
									} else if (mixedArray.getString(1)
											.equalsIgnoreCase(Admin.CHECKBOX)) {
										String string_array[] = mixedArray
												.getString(2).split("\n");
										TextView txt = new TextView(
												getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin
												.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										int check = 0;
										Hashtable<String, CheckBox> checkboxes_Hashtable = new Hashtable<String, CheckBox>();
										for (String s : string_array) {
											CheckBox checkBox = new CheckBox(
													getActivity());
											checkBox.setText(s.trim());
											checkBox.setId(ID++);
											checkBox.setPadding(0, 0, 10, 0);
											checkBox.setTypeface(Admin
													.getFontRoboto(getActivity()));
											layout_inner.addView(checkBox);
											checkboxes_Hashtable
													.put(String
															.valueOf(check++)
															.concat(Admin.CHECKBOX),
															checkBox);
										}
										// data_dynamic.put(Admin.CHECKBOX,checkboxes_Hashtable);
										data_dynamic
												.put(Admin.CHECKBOX,
														new Object[] {
																checkboxes_Hashtable,
																mixedArray
																		.getString(3) });
									} else if (mixedArray.getString(1)
											.equalsIgnoreCase(Admin.RADIO)) {
										String string_array[] = mixedArray
												.getString(2).split("\n");
										TextView txt = new TextView(
												getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin
												.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										RadioGroup radioGroup = new RadioGroup(
												getActivity());
										radioGroup
												.setOrientation(RadioGroup.VERTICAL);
										for (String s : string_array) {
											RadioButton radio = new RadioButton(
													getActivity());
											radio.setText(s.trim());
											radio.setId(ID++);
											radio.setChecked(true);
											radio.setTypeface(Admin
													.getFontRoboto(getActivity()));
											radioGroup.addView(radio);
										}
										radioGroup.setId(ID++);
										layout_inner.addView(radioGroup);
										// data_dynamic.put(Admin.RADIO,
										// radioGroup);
										data_dynamic.put(
												Admin.RADIO,
												new Object[] { radioGroup,
														mixedArray.getString(3) });
									}
								}
								dynamic_form_data.put(str, data_dynamic);
								linearLayout.addView(layout_inner);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				});
				Log.d("Create Response", json.toString());
			} catch (Exception e) {
				Log.d("Error : ", e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			linearLayout.addView(button);
			linearLayout.addView(button_out);
		}
	}

	private class FillForm extends
			AsyncTask<List<NameValuePair>, String, String> {
		int success = 0;
		@Override
		protected void onPreExecute() {
			//super.onPreExecute();
		/*
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage("Update...");
			progressDialog.setIndeterminate(false);
			progressDialog.setCancelable(true);
			progressDialog.show();
		*/
			alertDialog.show();
		}

		@Override
		protected String doInBackground(List<NameValuePair>... params) {
			final JSONObject json = jsonParser.makeHttpRequest(
					Admin.getUrlForParkingDailyFormFill(), "GET", params[0]);
			try {
				if (json.has(Admin.TAG_SUCCESS)) {
					success = json.getInt(Admin.TAG_SUCCESS);
				}
			} catch (Exception e) {
				Log.d("Error", e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			//super.onPostExecute(result);
			if (success == 1) {
				vehicle_no.setText("");
				//progressDialog.setMessage("Successfully Update");
				/*
				Toast toast =new Toast(getActivity().getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
				toast.setView(toast_view);
				toast.show();
				*/
				if (dynamic_form_data.size() > 0) {
					Enumeration<String> enum_key = dynamic_form_data.keys();
					while (enum_key.hasMoreElements()) {
						String string = (String) enum_key.nextElement();
						Hashtable<String, Object[]> dynamic_form_field = (Hashtable<String, Object[]>) dynamic_form_data
								.get(string);
						if (dynamic_form_field.size() > 0) {
							Enumeration<String> enum_key_field = dynamic_form_field
									.keys();
							while (enum_key_field.hasMoreElements()) {
								String string2 = (String) enum_key_field
										.nextElement();
								if (string2.equalsIgnoreCase(Admin.TEXTFIELD)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									EditText text = (EditText) object[0];
									text.setText("");
								} else if (string2
										.equalsIgnoreCase(Admin.TEXTAREA)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									EditText text = (EditText) object[0];
									text.setText("");
								} else if (string2
										.equalsIgnoreCase(Admin.SELECT)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									Spinner spinner = (Spinner) object[0];
									// spinner.setSelected(false);
									spinner.setSelection(0);
								} else if (string2
										.equalsIgnoreCase(Admin.RADIO)) {
									// RadioGroup
									// radioGroup=(RadioGroup)dynamic_form_field.get(string2);
									// int
									// selected=radioGroup.getCheckedRadioButtonId();
									// RadioButton
									// radioButton=(RadioButton)getActivity().findViewById(selected);
								} else if (string2.endsWith(Admin.CHECKBOX)) {
									Object object[] = (Object[]) dynamic_form_field
											.get(string2);
									@SuppressWarnings("unchecked")
									Hashtable<String, CheckBox> che = (Hashtable<String, CheckBox>) object[0];
									Enumeration<CheckBox> e = che.elements();
									while (e.hasMoreElements()) {
										CheckBox checkBox = (CheckBox) e
												.nextElement();
										if (checkBox.isChecked()) {
											checkBox.setChecked(false);
										}
									}
								}
							}
						}
					}
				}
			} else {
				if(progressDialog!=null) {
					progressDialog.setMessage("Unsuccessfully Update");
				}
			}
			//progressDialog.dismiss();
			alertDialog.dismiss();
		}
	}
	public void refreshFrom() {	
		vehicle_no.setText("");
		if (dynamic_form_data.size() > 0) {
			Enumeration<String> enum_key = dynamic_form_data.keys();
			while (enum_key.hasMoreElements()) {
				String string = (String) enum_key.nextElement();
				Hashtable<String, Object[]> dynamic_form_field = (Hashtable<String, Object[]>) dynamic_form_data
						.get(string);
				if (dynamic_form_field.size() > 0) {
					Enumeration<String> enum_key_field = dynamic_form_field
							.keys();
					while (enum_key_field.hasMoreElements()) {
						String string2 = (String) enum_key_field.nextElement();
						if (string2.equalsIgnoreCase(Admin.TEXTFIELD)) {
							Object object[] = (Object[]) dynamic_form_field
									.get(string2);
							EditText text = (EditText) object[0];
							text.setText("");
						} else if (string2.equalsIgnoreCase(Admin.TEXTAREA)) {
							Object object[] = (Object[]) dynamic_form_field
									.get(string2);
							EditText text = (EditText) object[0];
							text.setText("");
						} else if (string2.equalsIgnoreCase(Admin.SELECT)) {
							Object object[] = (Object[]) dynamic_form_field
									.get(string2);
							Spinner spinner = (Spinner) object[0];
							// spinner.setSelected(false);
							spinner.setSelection(0);
						} else if (string2.equalsIgnoreCase(Admin.RADIO)) {
							// RadioGroup
							// radioGroup=(RadioGroup)dynamic_form_field.get(string2);
							// int
							// selected=radioGroup.getCheckedRadioButtonId();
							// RadioButton
							// radioButton=(RadioButton)getActivity().findViewById(selected);
						} else if (string2.endsWith(Admin.CHECKBOX)) {
							Object object[] = (Object[]) dynamic_form_field
									.get(string2);
							@SuppressWarnings("unchecked")
							Hashtable<String, CheckBox> che = (Hashtable<String, CheckBox>) object[0];
							Enumeration<CheckBox> e = che.elements();
							while (e.hasMoreElements()) {
								CheckBox checkBox = (CheckBox) e.nextElement();
								if (checkBox.isChecked()) {
									checkBox.setChecked(false);
								}
							}
						}
					}
				}
			}
		}
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// super.onActivityResult(requestCode, resultCode, data);
		refreshFrom();
	}
}
package com.b2infosoft.gt.rajesh.fragments;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.server.JSONParser;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
public class LogReport2 extends Fragment{
	private SharedPreferences settings;
	public static final String TAG=LogReport2.class.getSimpleName();
	private final	JSONParser jsonParser = new JSONParser();
	private JSONArray jsonArray;
	private TableLayout tableLayout;
	private TextView textYear;
	private ImageButton button_year_next,button_year_previous;
	private TableLayout.LayoutParams layout_params_table_row;
	
	private int SELECT_YEAR;
	private int SELECT_MONTH;
	private String []foundMonths;
	
	public static LogReport2 newInstance() {
		return new LogReport2();
	}
	public static LogReport2 newInstance(String year,String month) {		
		return new LogReport2();
	}
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			View view=inflater.inflate(R.layout.fragments_outer_log_report_layout_2, container, false);			
			Bundle bundle = this.getArguments();
			getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));

			this.SELECT_YEAR=bundle.getInt(Admin.LOG_YEAR);
			this.SELECT_MONTH=bundle.getInt(Admin.LOG_MONTH);
			this.foundMonths=bundle.getStringArray(Admin.LOG_MONTH_ARRAY);
			settings=getActivity().getSharedPreferences(Admin.MYSECRATE,0);
			tableLayout=view.findViewById(R.id.log_month_table_1);
			layout_params_table_row=new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);			
	
			textYear=view.findViewById(R.id.month_selected_1);
			textYear.setTypeface(Admin.getFontRoboto(getActivity()), Typeface.BOLD);
		 	button_year_next=(ImageButton)view.findViewById(R.id.month_next_1);
		 	button_year_next.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					if(Arrays.asList(foundMonths).contains(String.valueOf(SELECT_MONTH))){						
						int index=Arrays.asList(foundMonths).indexOf(String.valueOf(SELECT_MONTH));							
						if(0<=index&&index<foundMonths.length){							
							new GetMonthLogs().execute(new String[]{		 			
									foundMonths[index+1],
						 			String.valueOf(SELECT_YEAR)		 					 			
						 	});
						 	updateMonth(String.valueOf(SELECT_YEAR), foundMonths[index+1]);							
						}
					}
				}
			});
		 	button_year_previous=(ImageButton)view.findViewById(R.id.month_previous_1);
		 	button_year_previous.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					if(Arrays.asList(foundMonths).contains(String.valueOf(SELECT_MONTH))){						
						int index=Arrays.asList(foundMonths).indexOf(String.valueOf(SELECT_MONTH));							
						if(0 < index && index<=foundMonths.length){							
							new GetMonthLogs().execute(new String[]{		 			
									foundMonths[index-1],
						 			String.valueOf(SELECT_YEAR)		 					 			
						 	});
						 	updateMonth(String.valueOf(SELECT_YEAR), foundMonths[index-1]);							
						}
					}					
				}
			});
		 	new GetMonthLogs().execute(new String[]{		 			
		 			String.valueOf(SELECT_MONTH),
		 			String.valueOf(SELECT_YEAR)		 					 			
		 	});
		 	updateMonth(String.valueOf(SELECT_YEAR), String.valueOf(SELECT_MONTH));
		 	return view;
	}
	private void updateMonth(String year,String month){	
		Log.d("Update", year+" : "+month);
		SELECT_YEAR=Integer.parseInt(year);
		SELECT_MONTH=Integer.parseInt(month);
		textYear.setText(Admin.getMonthName(SELECT_MONTH)+" "+String.valueOf(SELECT_YEAR));
		if(Arrays.asList(foundMonths).contains(String.valueOf(SELECT_MONTH))){
			int index=Arrays.asList(foundMonths).indexOf(String.valueOf(SELECT_MONTH));
			if(index==0){
				button_year_previous.setEnabled(false);
			}else{
				button_year_previous.setEnabled(true);
			}
			if(index<foundMonths.length-1){
				button_year_next.setEnabled(true);
			}else{
				button_year_next.setEnabled(false);
			}
		}
	}
	private void updateLog(JSONArray jsonArray){			
		if(tableLayout.getChildCount()>0){
			tableLayout.removeAllViews();
		}		
		for(int i=0;i<jsonArray.length();i++ ){			
			try{
				JSONObject c = jsonArray.getJSONObject(i);			
				TableRow tr_row = new TableRow(getActivity());
				tr_row.setLayoutParams(layout_params_table_row);										
				tr_row.setPadding(0, 10, 0, 10);												
				if((i%2)==0)
					tr_row.setBackgroundResource(R.color.bg_log_row_odd);
				else
					tr_row.setBackgroundResource(R.color.bg_log_row_even);										
				TextView	text=new TextView(getActivity());	
				text.setText(c.getString(Admin.LOG_DATE)+"\n"+c.getString(Admin.LOG_FROM_TO));
				text.setGravity(Gravity.CENTER_HORIZONTAL);				
				text.setTypeface(Admin.getFontRoboto(getActivity()));																				
				TextView text1=new TextView(getActivity());										
				
				text1.setText(c.getString(Admin.LOG_TOTAL_TIME));		
				text1.setGravity(Gravity.CENTER_HORIZONTAL);
				text1.setTypeface(Admin.getFontRoboto(getActivity()));																
							
				tr_row.addView(text);
				tr_row.addView(text1);				
				tableLayout.addView(tr_row);							
				
			}catch(Exception e){
				e.printStackTrace();
				Log.d("Exception", e.toString());
			}		
		}
	}
	private class GetMonthLogs extends AsyncTask<String, String, String>{
		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> pairs=new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
			pairs.add(new BasicNameValuePair(Admin.LOG_MONTH, params[0]));
			pairs.add(new BasicNameValuePair(Admin.LOG_YEAR, params[1]));
			final JSONObject jsonObject = jsonParser.makeHttpRequest(Admin.getUrlForLogYearMonthDetails(),"POST",pairs);						
			try{
				int success=0;
				JSONArray jsonArray=null;
				if(jsonObject.has(Admin.TAG_SUCCESS))
					success=jsonObject.getInt(Admin.TAG_SUCCESS);
				if(success==1){					
					if(jsonObject.has(Admin.LOG))
						jsonArray =jsonObject.getJSONArray(Admin.LOG);					
					if(jsonArray!=null){						
						LogReport2.this.jsonArray = jsonArray;
					}
				}
			}catch(JSONException e){
				
			}			
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(LogReport2.this.jsonArray!=null)
			{
				getActivity().runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						updateLog(LogReport2.this.jsonArray);						
					}
				});
			}
		}
	}	
}
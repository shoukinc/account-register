package com.b2infosoft.gt.rajesh.modals;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahesh on 12/12/16.
 */

public class DynamicFormBean {


    /**
     * forms : [{"form_id":0,"form_name":"Daily Activity","type":"daily_activity","icon":"icon9.png"},{"form_id":"1","form_name":"Car Parking","type":"multi column","icon":"icon1"},{"form_id":"2","form_name":"My Two Column Form","type":"two column","icon":"icon2"},{"form_id":"137","form_name":"Parking Voilation","type":"two column","icon":"icon3"},{"form_id":"138","form_name":"Incident Report","type":"two column","icon":"icon4"},{"form_id":"139","form_name":"Parking Log","type":"multi column","icon":"icon5"},{"form_id":"599","form_name":"Parking Voilation","type":"two column","icon":"icon6"},{"form_id":"600","form_name":"Incident Report","type":"two column","icon":"icon7"},{"form_id":"601","form_name":"Parking Log","type":"multi column","icon":"icon8"},{"form_id":"1061","form_name":"Parking Voilation","type":"two column","icon":"icon9"},{"form_id":"1062","form_name":"Incident Report","type":"two column","icon":"icon5.png"},{"form_id":"1063","form_name":"Parking Log","type":"multi column","icon":"icon4.png"},{"form_id":"1409","form_name":"creative log","type":"two column","icon":""},{"form_id":"1411","form_name":"Test Form","type":"two column","icon":""},{"form_id":"1412","form_name":"Data recorder ","type":"two column","icon":""}]
     * success : 1
     */

    private int success;
    private ArrayList<FormsBean> forms;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public ArrayList<FormsBean> getForms() {
        return forms;
    }

    public void setForms(ArrayList<FormsBean> forms) {
        this.forms = forms;
    }

    public static class FormsBean {
        /**
         * form_id : 0
         * form_name : Daily Activity
         * type : daily_activity
         * icon : icon9.png
         */

        private int form_id;
        private String form_name;
        private String type;
        private String icon;

        public int getForm_id() {
            return form_id;
        }

        public void setForm_id(int form_id) {
            this.form_id = form_id;
        }

        public String getForm_name() {
            return form_name;
        }

        public void setForm_name(String form_name) {
            this.form_name = form_name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }
}

package com.b2infosoft.gt.rajesh.fragments.dashboard;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.BuildConfig;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.SlideMenu;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.services.LocationUpdate;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class DashboardNew extends Fragment {
    public static final String TAG = "Dashboard";
    private static SharedPreferences settings;
    JSONParser jsonParser = new JSONParser();
    private ProgressBar progressBar;

    private void navigate(final Fragment paramFragment, final String paramString)
    {
        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {
                SlideMenu.setActiveFragment(DashboardNew.this.getFragmentManager(), paramFragment, paramString);
            }
        });
    }

    public static DashboardNew newInstance()
    {
        return new DashboardNew();
    }

    public boolean isServiceRunning(String paramString)
    {
        Iterator localIterator = ((ActivityManager)getActivity().getSystemService(Context.ACTIVITY_SERVICE)).getRunningServices(Integer.MAX_VALUE).iterator();
        while (localIterator.hasNext()) {
            if (((ActivityManager.RunningServiceInfo)localIterator.next()).service.getClassName().equals(paramString)) {
                return true;
            }
        }
        return false;
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
    {
        settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
        View localView = paramLayoutInflater.inflate(R.layout.fragment_dashboard_new_layout, paramViewGroup, false);
        this.progressBar = ((ProgressBar)localView.findViewById(R.id.progress_bar));
        //new EmpStatus(null).execute(new String[0]);
        new EmpStatus().execute(new String[0]);
        return localView;
    }

    String str1="";
    private class EmpStatus extends AsyncTask<String, String, JSONObject>
    {
        private EmpStatus() {}
        protected JSONObject doInBackground(String... paramVarArgs)
        {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Admin.USER_ID,settings.getString(Admin.USER_ID, "")));
            return DashboardNew.this.jsonParser.makeHttpRequest(Admin.getUrlForEmpStatus(), "POST", params);
        }

        protected void onPostExecute(JSONObject object)
        {

            progressBar.setVisibility(View.GONE);

            int schedule = 0;
            int punch = 0;
            int client = 0;
            if (object != null) {
                try {
                    if (object.has(Admin.TAG_SUCCESS)) {
                        int success = object.getInt(Admin.TAG_SUCCESS);
                    }
                    if (object.has(Admin.SCHEDULE_STATUS)) {
                        schedule = object.getInt(Admin.SCHEDULE_STATUS);
                    }
                    if (object.has(Admin.CLIENT_STATUS)) {
                        client = object.getInt(Admin.CLIENT_STATUS);
                    }
                    if (object.has(Admin.PUNCH_STATUS)) {
                        punch = object.getInt(Admin.PUNCH_STATUS);
                    }

                    String client_name_ = "";
                    String client_id_ = "";
                    if (client == 1) {
                        if (object.has(Admin.CLIENT_NAME)) {
                            client_name_ = object.getString(Admin.CLIENT_NAME);
                            MySharedPreferences.setSharedPreferences(DashboardNew.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        }
                        if (object.has(Admin.CLIENT_ID)) {
                            MySharedPreferences.setSharedPreferences(DashboardNew.this.getActivity(), Admin.CLIENT_ID, object.getString(Admin.CLIENT_ID));
                        }
                        DashboardNew.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                        MySharedPreferences.setSharedPreferences(DashboardNew.this.getActivity(), Admin.PUNCH_STATUS, punch+"");
                    } else {
                        MySharedPreferences.setSharedPreferences(DashboardNew.this.getActivity(), Admin.CLIENT_ID, client_id_);
                        MySharedPreferences.setSharedPreferences(DashboardNew.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        MySharedPreferences.setSharedPreferences(DashboardNew.this.getActivity(), Admin.PUNCH_STATUS, punch+"");
                        DashboardNew.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON", e.toString());
                }
            }
/*
            System.out.println("SCHEDULE  "+TAG+schedule);
            System.out.println("PUNCH "+TAG+punch);
            System.out.println("CLIENT "+TAG+client);
*/
            /*if (schedule == 1 && client == 0) {
                DashboardNew.this.navigate(ScheduleActive.newInstance(), ScheduleActive.TAG);
            } else */if (schedule != 0 || client != 0) {
                switch (punch) {
                    case 0:
                        DashboardNew.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                        break;
                    case 1:
                        DashboardNew.this.navigate(SessionStart.newInstance(), SessionStart.TAG);
                        break;
                    case 2:
                        DashboardNew.this.navigate(SessionBreak.newInstance(), SessionBreak.TAG);
                        break;
                    case 3:
                       // if (client != 1) {
                            DashboardNew.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
                         /*   break;
                        } else {*/
                           // DashboardNew.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                            break;
                        //}
                    default:
                        break;
                }
            } else {
                DashboardNew.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
            }
            if (punch == 1) {
                MySharedPreferences.setSharedPreferences(DashboardNew.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_START);
            } else {
                MySharedPreferences.setSharedPreferences(DashboardNew.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_END);
            }
        }
        protected void onPreExecute()
        {
            progressBar.setVisibility(View.VISIBLE);
        }
    }
}

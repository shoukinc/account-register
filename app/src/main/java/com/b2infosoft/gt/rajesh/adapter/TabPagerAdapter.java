package com.b2infosoft.gt.rajesh.adapter;

import com.b2infosoft.gt.rajesh.fragments.inner.Logs;
import com.b2infosoft.gt.rajesh.fragments.inner.Month;
import com.b2infosoft.gt.rajesh.fragments.inner.Year;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabPagerAdapter extends FragmentPagerAdapter{
	
	public TabPagerAdapter(FragmentManager fm) {
		super(fm);		
	}
	@Override
	public Fragment getItem(int postion) {
		switch (postion) {
		case 0:
			return new Logs();
		case 1:
			return new Month();
		case 2:
			return new Year();
		}
		return null;
	}

	@Override
	public int getCount() {
		return 3;
	}
	
}

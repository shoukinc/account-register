package com.b2infosoft.gt.rajesh.fragments.dashboard;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.SlideMenu;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.rajesh.sign_2.CaptureSignature;
import com.b2infosoft.gt.server.JSONParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class SessionBreak extends Fragment implements LocationListener {
    public static final String TAG = SessionBreak.class.getSimpleName();
    private static SharedPreferences settings;
    private TextView clock;
    private int hours_1;
    JSONParser jsonParser = new JSONParser();
    private double latitude;
    private String location = "";
    private LocationManager locationManager;
    private double longitude;
    private View mView;
    private int minutes_1;
    private ProgressBar progressBar;
    private String provider;
    private Button resume_session;
    private int seconds_1;
    private Button session_end;

    private void navigate(final Fragment paramFragment, final String paramString) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                SlideMenu.setActiveFragment(SessionBreak.this.getFragmentManager(), paramFragment, paramString);
            }
        });
    }

    public static SessionBreak newInstance() {
        return new SessionBreak();
    }

    private void update() {
        this.seconds_1++;
        if (this.seconds_1 >= 60) {
            this.seconds_1 = 0;
            if (this.minutes_1 < 59) {
                this.minutes_1++;
            } else if (this.hours_1 < 23) {
                this.minutes_1 = 0;
                this.hours_1++;
            } else {
                this.minutes_1 = 0;
                this.hours_1 = 0;
            }
        }
        this.clock.setText(String.format("%02d:%02d", new Object[]{Integer.valueOf(this.hours_1), Integer.valueOf(this.minutes_1)}));
    }

    public String getCompleteAddressString(double paramDouble1, double paramDouble2) {
        String localObject = "";
        Geocoder localGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List localList = localGeocoder.getFromLocation(paramDouble1, paramDouble2, 1);
            if (localList != null) {
                Address localAddress = (Address) localList.get(0);
                StringBuilder localStringBuilder = new StringBuilder("");
                for (int i = 0; i < localAddress.getMaxAddressLineIndex(); i++) {
                    localStringBuilder.append(localAddress.getAddressLine(i)).append("\n");
                }
                String str = localStringBuilder.toString();
                localObject = str;
            }
            return localObject;
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return localObject;
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {
        settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
        this.locationManager = ((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE));
        Criteria localCriteria = new Criteria();
        localCriteria.setSpeedRequired(false);
        localCriteria.setAccuracy(1);
        localCriteria.setCostAllowed(true);
        localCriteria.setBearingAccuracy(3);
        localCriteria.setAltitudeRequired(false);
        this.provider = this.locationManager.getBestProvider(localCriteria, false);
        View view = paramLayoutInflater.inflate(R.layout.fragment_dashboard_new_session_break_layout, paramViewGroup, false);
        this.progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        this.mView = view.findViewById(R.id.punch_in_view);
        this.clock = (TextView) view.findViewById(R.id.dashboard_clock);
        this.resume_session = (Button) view.findViewById(R.id.resume_session_button);
        this.resume_session.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                if (SessionBreak.this.locationManager.isProviderEnabled("gps")) {
                    Location localLocation = SessionBreak.this.locationManager.getLastKnownLocation(SessionBreak.this.provider);
                    if (localLocation != null) {
                        SessionBreak.this.latitude = localLocation.getLatitude();
                        SessionBreak.this.longitude = localLocation.getLongitude();
                        SessionBreak.this.location = SessionBreak.this.getCompleteAddressString(SessionBreak.this.latitude, SessionBreak.this.longitude);
                        AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(SessionBreak.this.getActivity());
                        localBuilder2.setTitle("Alert");
                        localBuilder2.setMessage("Do you want to Resume session?");
                        localBuilder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                SessionBreak.CheckIn localCheckIn = new SessionBreak.CheckIn();
                                String[] arrayOfString = new String[7];
                                arrayOfString[0] = settings.getString("user_id", "");
                                arrayOfString[1] = String.valueOf(SessionBreak.this.latitude);
                                arrayOfString[2] = String.valueOf(SessionBreak.this.longitude);
                                arrayOfString[3] = "PUNCH IN";
                                arrayOfString[4] = SessionBreak.this.location;
                                arrayOfString[5] = MySharedPreferences.getSharedPreferences(SessionBreak.this.getActivity(), "client_id");
                                arrayOfString[6] = "1";
                                localCheckIn.execute(arrayOfString);
                            }
                        });
                        localBuilder2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                paramAnonymous2DialogInterface.dismiss();
                            }
                        });
                        localBuilder2.show();
                        return;
                    }
                    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(SessionBreak.this.getActivity());
                    localBuilder1.setTitle("Alert");
                    localBuilder1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
                    localBuilder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            paramAnonymous2DialogInterface.dismiss();
                        }
                    });
                    localBuilder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            SessionBreak.CheckIn localCheckIn = new SessionBreak.CheckIn();
                            String[] arrayOfString = new String[7];
                            arrayOfString[0] = settings.getString("user_id", "");
                            arrayOfString[1] = String.valueOf(SessionBreak.this.latitude);
                            arrayOfString[2] = String.valueOf(SessionBreak.this.longitude);
                            arrayOfString[3] = "PUNCH IN";
                            arrayOfString[4] = SessionBreak.this.location;
                            arrayOfString[5] = MySharedPreferences.getSharedPreferences(SessionBreak.this.getActivity(), "client_id");
                            arrayOfString[6] = "1";
                            localCheckIn.execute(arrayOfString);
                        }
                    });
                    localBuilder1.create().show();
                    return;
                }
                SessionBreak.this.showSettingsAlert();
            }
        });
        this.session_end = ((Button) view.findViewById(R.id.session_end_button));
        this.session_end.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                if (SessionBreak.this.locationManager.isProviderEnabled("gps")) {
                    Location localLocation = SessionBreak.this.locationManager.getLastKnownLocation(SessionBreak.this.provider);
                    if (localLocation != null) {
                        SessionBreak.this.latitude = localLocation.getLatitude();
                        SessionBreak.this.longitude = localLocation.getLongitude();
                        SessionBreak.this.location = SessionBreak.this.getCompleteAddressString(SessionBreak.this.latitude, SessionBreak.this.longitude);
                        AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(SessionBreak.this.getActivity());
                        localBuilder2.setTitle("Alert");
                        localBuilder2.setMessage("Do you want to End Session?");
                        localBuilder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                Intent localIntent = new Intent(SessionBreak.this.getActivity(), CaptureSignature.class);
                                Bundle localBundle = new Bundle();
                                localBundle.putString("user_id", settings.getString("user_id", ""));
                                localBundle.putString("company_id", settings.getString("company_id", ""));
                                localBundle.putString("latitude", SessionBreak.this.latitude + "");
                                localBundle.putString("longitude", SessionBreak.this.longitude + "");
                                localBundle.putString("check", "PUNCH OUT");
                                localBundle.putString("location", SessionBreak.this.location);
                                localBundle.putString("client_id", MySharedPreferences.getSharedPreferences(SessionBreak.this.getActivity(), "client_id"));
                                localBundle.putString("check_out_status", "4");
                                localIntent.putExtra("data", localBundle);
                                localIntent.putExtra("url", Admin.getUrlForCheck_In_Out());
                                SessionBreak.this.getActivity().startActivityForResult(localIntent, 1);
                            }
                        });
                        localBuilder2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                paramAnonymous2DialogInterface.dismiss();
                            }
                        });
                        localBuilder2.show();
                        return;
                    }
                    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(SessionBreak.this.getActivity());
                    localBuilder1.setTitle("Alert");
                    localBuilder1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
                    localBuilder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            paramAnonymous2DialogInterface.dismiss();
                        }
                    });
                    localBuilder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            Intent localIntent = new Intent(SessionBreak.this.getActivity(), CaptureSignature.class);
                            Bundle localBundle = new Bundle();
                            localBundle.putString("user_id", settings.getString("user_id", ""));
                            localBundle.putString("company_id", settings.getString("company_id", ""));
                            localBundle.putString("latitude", SessionBreak.this.latitude + "");
                            localBundle.putString("longitude", SessionBreak.this.longitude + "");
                            localBundle.putString("check", "PUNCH OUT");
                            localBundle.putString("location", SessionBreak.this.location);
                            localBundle.putString("client_id", MySharedPreferences.getSharedPreferences(SessionBreak.this.getActivity(), "client_id"));
                            localBundle.putString("check_out_status", "4");
                            localIntent.putExtra("data", localBundle);
                            localIntent.putExtra("url", Admin.getUrlForCheck_In_Out());
                            SessionBreak.this.getActivity().startActivityForResult(localIntent, 1);
                        }
                    });
                    localBuilder1.create().show();
                    return;
                }
                SessionBreak.this.showSettingsAlert();
            }
        });
        new TimerTime().execute(new String[0]);
        return view;
    }

    public void onLocationChanged(Location paramLocation) {
        this.latitude = paramLocation.getLatitude();
        this.longitude = paramLocation.getLongitude();
        this.location = getCompleteAddressString(this.latitude, this.longitude);
    }

    public void onPause() {
        super.onPause();
    }

    public void onProviderDisabled(String paramString) {
    }

    public void onProviderEnabled(String paramString) {
        onLocationChanged(this.locationManager.getLastKnownLocation(paramString));
    }

    public void onResume() {
        super.onResume();
        new EmpStatus().execute(new String[0]);
    }

    public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle) {
    }

    public void onStop() {
        super.onStop();
    }

    public void showNoInternetConncetAlert() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle("No Internet Connection");
        localBuilder.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");
        localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                if (!new Network(SessionBreak.this.getActivity()).isConnectingToInternet()) {
                    SessionBreak.this.showNoInternetConncetAlert();
                }
            }
        });
        localBuilder.show();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle("GPS is settings");
        localBuilder.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        localBuilder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                Intent localIntent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                SessionBreak.this.getActivity().startActivityForResult(localIntent, 1);
            }
        });
        localBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                paramAnonymousDialogInterface.cancel();
            }
        });
        localBuilder.show();
    }

    private class CheckIn extends AsyncTask<String, String, String> {
        private String action = "";
        private String message = "";

        private CheckIn() {
        }

        protected String doInBackground(String... paramVarArgs) {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(new BasicNameValuePair("user_id", paramVarArgs[0]));
            localArrayList.add(new BasicNameValuePair("company_id", settings.getString("company_id", "")));
            localArrayList.add(new BasicNameValuePair("latitude", paramVarArgs[1]));
            localArrayList.add(new BasicNameValuePair("longitude", paramVarArgs[2]));
            localArrayList.add(new BasicNameValuePair("check", paramVarArgs[3]));
            localArrayList.add(new BasicNameValuePair("location", paramVarArgs[4]));
            localArrayList.add(new BasicNameValuePair("client_id", paramVarArgs[5]));
            localArrayList.add(new BasicNameValuePair("check_out_status", paramVarArgs[6]));
            JSONObject localJSONObject = SessionBreak.this.jsonParser.makeHttpRequest(Admin.getUrlForCheck_In_Out(), "POST", localArrayList);
          //  Log.d("Response Holi", localJSONObject.toString());
            try {
                if (localJSONObject.getInt("success") == 1) {
                    if (localJSONObject.has("action")) {
                        this.action = localJSONObject.getString("action");
                    }
                    if (localJSONObject.has("client_id")) {
                        SessionBreak.this.getActivity().getSharedPreferences("rajesh", 0).edit().putString("client_id", localJSONObject.getString("client_id")).commit();
                    }
                    if (localJSONObject.has("message")) {
                        this.message = localJSONObject.getString("message");
                    }
                }
            } catch (JSONException localJSONException) {

                localJSONException.printStackTrace();
                Log.d("My Exception", localJSONException.toString());

            }
            return null;
        }

        protected void onPostExecute(String paramString) {
            SessionBreak.this.progressBar.setVisibility(View.GONE);
            SessionBreak.this.mView.setVisibility(View.VISIBLE);
            new EmpStatus().execute(new String[0]);
            if (!this.action.equalsIgnoreCase("PUNCH IN") && !this.action.equalsIgnoreCase("PUNCH OUT")) {
                Toast.makeText(SessionBreak.this.getActivity(), this.message, Toast.LENGTH_SHORT).show();
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
            SessionBreak.this.progressBar.setVisibility(View.VISIBLE);
            SessionBreak.this.mView.setVisibility(View.GONE);
        }
    }

    private class EmpStatus extends AsyncTask<String, String, JSONObject> {
        private EmpStatus() {
        }

        protected JSONObject doInBackground(String... paramVarArgs) {
            List<NameValuePair> pairs = new ArrayList();
            pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
            return SessionBreak.this.jsonParser.makeHttpRequest(Admin.getUrlForEmpStatus(), "POST", pairs);
        }

        protected void onPostExecute(JSONObject object) {
            SessionBreak.this.progressBar.setVisibility(View.GONE);
            SessionBreak.this.mView.setVisibility(View.VISIBLE);

            int schedule = 0;
            int punch = 0;
            int client = 0;
            if (object != null) {
                try {
                    if (object.has(Admin.TAG_SUCCESS)) {
                        int success = object.getInt(Admin.TAG_SUCCESS);
                    }
                    if (object.has(Admin.SCHEDULE_STATUS)) {
                        schedule = object.getInt(Admin.SCHEDULE_STATUS);
                    }
                    if (object.has(Admin.CLIENT_STATUS)) {
                        client = object.getInt(Admin.CLIENT_STATUS);
                    }
                    if (object.has(Admin.PUNCH_STATUS)) {
                        punch = object.getInt(Admin.PUNCH_STATUS);
                    }

                    String client_name_ = "";
                    String client_id_ = "";
                    if (client == 1) {
                        if (object.has(Admin.CLIENT_NAME)) {
                            client_name_ = object.getString(Admin.CLIENT_NAME);
                            MySharedPreferences.setSharedPreferences(SessionBreak.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        }
                        if (object.has(Admin.CLIENT_ID)) {
                            MySharedPreferences.setSharedPreferences(SessionBreak.this.getActivity(), Admin.CLIENT_ID, object.getString(Admin.CLIENT_ID));
                        }
                        SessionBreak.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                    } else {
                        MySharedPreferences.setSharedPreferences(SessionBreak.this.getActivity(), Admin.CLIENT_ID, client_id_);
                        MySharedPreferences.setSharedPreferences(SessionBreak.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        SessionBreak.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON", e.toString());
                }
            }
            System.out.println("SCHEDULE  " + TAG + schedule);
            System.out.println("PUNCH " + TAG + punch);
            System.out.println("CLIENT " + TAG + client);

            if (schedule == 1 && client == 0) {
                SessionBreak.this.navigate(ScheduleActive.newInstance(), ScheduleActive.TAG);
            } else if (schedule != 0 || client != 0) {
                switch (punch) {
                    case 0:
                        SessionBreak.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                        break;
                    case 1:
                        SessionBreak.this.navigate(SessionStart.newInstance(), SessionStart.TAG);
                        break;
                    case 2:
                        //SessionBreak.this.navigate(SessionBreak.newInstance(), SessionBreak.TAG);
                        break;
                    case 3:
                        if (client != 1) {
                            SessionBreak.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
                            break;
                        } else {
                            SessionBreak.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                            break;
                        }
                    default:
                        break;
                }
            } else {
                SessionBreak.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
            }
            if (punch == 1) {
                MySharedPreferences.setSharedPreferences(SessionBreak.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_START);
            } else {
                MySharedPreferences.setSharedPreferences(SessionBreak.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_END);
            }
        }

        protected void onPreExecute() {
            SessionBreak.this.progressBar.setVisibility(View.VISIBLE);
            SessionBreak.this.mView.setVisibility(View.GONE);
        }
    }

    private class TimerTime extends AsyncTask<String, String, String> {
        private TimerTime() {
        }
        protected String doInBackground(String... paramVarArgs) {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(new BasicNameValuePair("user_id", settings.getString("user_id", "")));
            localArrayList.add(new BasicNameValuePair("company_id", settings.getString("company_id", "")));
            localArrayList.add(new BasicNameValuePair("client_id", settings.getString("client_id", "")));
            final JSONObject localJSONObject = SessionBreak.this.jsonParser.makeHttpRequest(Admin.getUrlForTotalTime(), "POST", localArrayList);
            try {
                if ((localJSONObject.has("success")) && (localJSONObject.getInt("success") == 1) && (localJSONObject.has("total_time"))) {
                    final String str = localJSONObject.getString("total_time");
                    SessionBreak.this.getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            String[] arrayOfString = str.split(":");
                            int i = Integer.parseInt(arrayOfString[0]);
                            int j = Integer.parseInt(arrayOfString[1]);
                            int k = Integer.parseInt(arrayOfString[2]);
                            SessionBreak.this.hours_1 = i;
                            SessionBreak.this.minutes_1 = j;
                            SessionBreak.this.seconds_1 = k;
                            try {
                                if (localJSONObject.has("status")) {
                                    int m = localJSONObject.getInt("status");
                                    SessionBreak.this.update();
                                }
                                return;
                            } catch (JSONException localJSONException) {

                            }
                        }
                    });
                }
                return null;
            } catch (JSONException localJSONException) {
                    Log.d("Exception  : ", localJSONException.getMessage());
            }
            return null;
        }

        protected void onPostExecute(String paramString) {
            super.onPostExecute(paramString);
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
}

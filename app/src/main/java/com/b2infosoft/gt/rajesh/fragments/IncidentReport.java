package com.b2infosoft.gt.rajesh.fragments;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.rajesh.sign_2.CaptureSignature;

import de.ailis.pherialize.MixedArray;
import de.ailis.pherialize.Pherialize;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
public class IncidentReport extends Fragment{	
	public static IncidentReport newInstance() {
		return new IncidentReport();
	}
	private SharedPreferences settings;
	private	ProgressDialog progressDialog;
	public static final String TAG=IncidentReport.class.getSimpleName();
	private final	JSONParser jsonParser=new JSONParser();
	private RelativeLayout mainLayout;
	TextView textView;
	private Button button;
	private ScrollView scrollView;
	private LinearLayout linearLayout;
	View toast_view;
//	private Hashtable<String, Hashtable<String,Object>> dynamic_form_data	=	new Hashtable<String, Hashtable<String,Object>>();
//	private Hashtable<String, Hashtable<String,Object[]>> dynamic_form_data	=	new Hashtable<String, Hashtable<String,Object[]>>();
	private Hashtable<String, Hashtable<String,Object[]>> dynamic_form_data_left	=	new Hashtable<String, Hashtable<String,Object[]>>();
	private Hashtable<String, Hashtable<String,Object[]>> dynamic_form_data_right	=	new Hashtable<String, Hashtable<String,Object[]>>();

	private List<NameValuePair> params_main = new ArrayList<NameValuePair>();	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			View view=inflater.inflate(R.layout.fragments_outer_incident_report_layout, container, false);
			toast_view=inflater.inflate(R.layout.custom_toast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));		
			getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));

			settings=getActivity().getSharedPreferences(Admin.MYSECRATE,0); 		
			final Animation shake=AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
			mainLayout=(RelativeLayout)view.findViewById(R.id.incident_report_form_layout);			
			scrollView=new ScrollView(getActivity());
			scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
			linearLayout=new LinearLayout(getActivity());
			linearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			linearLayout.setOrientation(LinearLayout.VERTICAL);
			textView=new TextView(getActivity());
			textView.setText("Incident Report".toUpperCase(Locale.getDefault()));
			//textView.setAllCaps(true);
			textView.setGravity(Gravity.CENTER_HORIZONTAL);
			textView.setTypeface(Admin.getFontRoboto(getActivity()));
			textView.setVisibility(View.VISIBLE);
			LayoutParams textparams=new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			textparams.bottomMargin=25;
			textparams.topMargin=15;
			textparams.leftMargin=0;
			textparams.rightMargin=0;
			textView.setLayoutParams(textparams);
			linearLayout.addView(textView);			
			button=new Button(getActivity());
			LayoutParams buttonparams=new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			buttonparams.bottomMargin=25;
			buttonparams.topMargin=15;
			buttonparams.leftMargin=25;
			buttonparams.rightMargin=25;
			button.setLayoutParams(buttonparams);		
			button.setPadding(10, 10, 10, 10);
			button.setText("Submit");
			button.setBackgroundResource(R.drawable.button_background);
			button.setTextColor(Color.parseColor("#ffffff"));
			button.setOnClickListener(new View.OnClickListener() {			
				@SuppressWarnings("unchecked")
				@Override
				public void onClick(View v) {
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair(Admin.COMPANY_ID, settings.getString(Admin.COMPANY_ID, "")));
					params.add(new BasicNameValuePair(Admin.CLIENT_ID, settings.getString(Admin.CLIENT_ID, "")));
					params.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
					int i=0;
					int j=0;
					boolean validation=false;
					if(dynamic_form_data_left.size()>0){
						Enumeration<String> enum_key=dynamic_form_data_left.keys();
						while (enum_key.hasMoreElements()) {
							String string = (String) enum_key.nextElement();
//							Hashtable<String, Object> dynamic_form_field=(Hashtable<String, Object>)dynamic_form_data.get(string);
							Hashtable<String, Object[]> dynamic_form_field=(Hashtable<String, Object[]>)dynamic_form_data_left.get(string);
							if(dynamic_form_field.size()>0){
								Enumeration<String> enum_key_field=dynamic_form_field.keys();
								while (enum_key_field.hasMoreElements()) {
									String string2 = (String) enum_key_field.nextElement();
									if(string2.equalsIgnoreCase(Admin.TEXTFIELD)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										String mandatory=object[1].toString();
										EditText text=(EditText)object[0];
										String value=text.getText().toString().trim();
										if(mandatory.equalsIgnoreCase("Mandatory")){
											if(value.length()==0){
												validation=true;
												text.startAnimation(shake);
												text.setError("Field cannot be left blank.");
											}
										}
										params.add(new BasicNameValuePair(string,value));
										i++;
									}else if(string2.equalsIgnoreCase(Admin.TEXTAREA)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										EditText text=(EditText)object[0];
										String mandatory=object[1].toString();
										String value=text.getText().toString().trim();
										if(mandatory.equalsIgnoreCase("Mandatory")){
											if(value.length()==0){
												validation=true;
												text.startAnimation(shake);
												text.setError("Field cannot be left blank.");
											}
										}
										params.add(new BasicNameValuePair(string, value));
										i++;
									}else if(string2.equalsIgnoreCase(Admin.SELECT)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										Spinner spinner=(Spinner)object[0];
										String mandatory=object[1].toString();
										int value=spinner.getSelectedItemPosition();
										if(mandatory.equalsIgnoreCase("Mandatory")){
											if(value==0){
												validation=true;
												TextView text=(TextView)spinner.getSelectedView();
												text.startAnimation(shake);
												text.setError("Field cannot be Unselected.");
											}
										}
										params.add(new BasicNameValuePair(string,spinner.getSelectedItem().toString()));
										i++;
									}else if(string2.equalsIgnoreCase(Admin.RADIO)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										RadioGroup radioGroup=(RadioGroup)object[0];
										String mandatory=object[1].toString();

										int selected=radioGroup.getCheckedRadioButtonId();
										RadioButton radioButton=(RadioButton)getActivity().findViewById(selected);
										params.add(new BasicNameValuePair(string, radioButton.getText().toString()));
										i++;
									}else if(string2.endsWith(Admin.CHECKBOX)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										Hashtable<String, CheckBox> che=(Hashtable<String, CheckBox>)object[0];
										String mandatory=object[1].toString();

										Enumeration<CheckBox> e=che.elements();
										String selected_check_box="";
										while (e.hasMoreElements()) {
											CheckBox checkBox = (CheckBox) e.nextElement();
											if(checkBox.isChecked()){
												selected_check_box+=checkBox.getText().toString()+",";
											}
										}
										if(selected_check_box.length()>0)
											selected_check_box=selected_check_box.substring(0, selected_check_box.length()-1);
										params.add(new BasicNameValuePair(string, selected_check_box));
										i++;
									}
								}
							}
						}
					}
					if(dynamic_form_data_right.size()>0){
						Enumeration<String> enum_key=dynamic_form_data_right.keys();
						while (enum_key.hasMoreElements()) {
							String string = (String) enum_key.nextElement();
//							Hashtable<String, Object> dynamic_form_field=(Hashtable<String, Object>)dynamic_form_data.get(string);
							Hashtable<String, Object[]> dynamic_form_field=(Hashtable<String, Object[]>)dynamic_form_data_right.get(string);
							string= string+"R";
							if(dynamic_form_field.size()>0){
								Enumeration<String> enum_key_field=dynamic_form_field.keys();
								while (enum_key_field.hasMoreElements()) {
									String string2 = (String) enum_key_field.nextElement();
									if(string2.equalsIgnoreCase(Admin.TEXTFIELD)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										String mandatory=object[1].toString();
										EditText text=(EditText)object[0];
										String value=text.getText().toString().trim();
										if(mandatory.equalsIgnoreCase("Mandatory")){
											if(value.length()==0){
												validation=true;
												text.startAnimation(shake);
												text.setError("Field cannot be left blank.");
											}
										}
										params.add(new BasicNameValuePair(string,value));
										j++;
									}else if(string2.equalsIgnoreCase(Admin.TEXTAREA)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										EditText text=(EditText)object[0];
										String mandatory=object[1].toString();
										String value=text.getText().toString().trim();
										if(mandatory.equalsIgnoreCase("Mandatory")){
											if(value.length()==0){
												validation=true;
												text.startAnimation(shake);
												text.setError("Field cannot be left blank.");
											}
										}
										params.add(new BasicNameValuePair(string, value));
										j++;
									}else if(string2.equalsIgnoreCase(Admin.SELECT)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										Spinner spinner=(Spinner)object[0];
										String mandatory=object[1].toString();
										int value=spinner.getSelectedItemPosition();
										if(mandatory.equalsIgnoreCase("Mandatory")){
											if(value==0){
												validation=true;
												TextView text=(TextView)spinner.getSelectedView();
												text.startAnimation(shake);
												text.setError("Field cannot be Unselect.");
											}
										}
										params.add(new BasicNameValuePair(string,spinner.getSelectedItem().toString()));
										j++;
									}else if(string2.equalsIgnoreCase(Admin.RADIO)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										RadioGroup radioGroup=(RadioGroup)object[0];
										String mandatory=object[1].toString();

										int selected=radioGroup.getCheckedRadioButtonId();
										RadioButton radioButton=(RadioButton)getActivity().findViewById(selected);
										params.add(new BasicNameValuePair(string, radioButton.getText().toString()));
										j++;
									}else if(string2.endsWith(Admin.CHECKBOX)){
										Object object[]=(Object[])dynamic_form_field.get(string2);
										Hashtable<String, CheckBox> che=(Hashtable<String, CheckBox>)object[0];
										String mandatory=object[1].toString();

										Enumeration<CheckBox> e=che.elements();
										String selected_check_box="";
										while (e.hasMoreElements()) {
											CheckBox checkBox = (CheckBox) e.nextElement();
											if(checkBox.isChecked()){
												selected_check_box+=checkBox.getText().toString()+",";
											}
										}
										if(selected_check_box.length()>0)
											selected_check_box=selected_check_box.substring(0, selected_check_box.length()-1);
										params.add(new BasicNameValuePair(string, selected_check_box));
										j++;
									}
								}
							}
						}
					}
					params.add(new BasicNameValuePair(Admin.MAX_FORM_FIELD, String.valueOf(i)));
					params.add(new BasicNameValuePair(Admin.MAX_FORM_FIELD_1, String.valueOf(j)));
					if(params!=null){
						if(validation){

						}else{
							IncidentReport.this.params_main=params;
							fillForms();
						}
					}
				}
			});
			scrollView.addView(linearLayout);
			mainLayout.addView(scrollView);		
			getForms();
			return view;
	}
	private void fillForms(){
		if(!new Network(getActivity()).isConnectingToInternet()){
			showNoInternetConncetAlert();
		}else{
			//new FillForm().execute(new List[]{params_main});
			Intent intent =new Intent(getActivity(),CaptureSignature.class);
			Bundle bundle =new Bundle();
			Iterator<NameValuePair> iterator=  params_main.iterator();			
			while(iterator.hasNext()){
				NameValuePair pair = (NameValuePair)iterator.next();				
				//Log.d(pair.getName(), pair.getValue());				
				bundle.putString(pair.getName().trim(), pair.getValue());				
			}
			intent.putExtra(Admin.DATA,bundle);
			intent.putExtra(Admin.URL, Admin.getUrlForParkingIncidentFormFill());
			getActivity().startActivityForResult(intent, Admin.REQUEST_CODE_LOG_OUT);
		}
	}
	private void getForms(){
		if(!new Network(getActivity()).isConnectingToInternet()){
			showNoInternetConncetAlert_1();
		}else{
			new GetForm().execute(new String[]{settings.getString(Admin.COMPANY_ID, "")});
		}
	}
	public void showNoInternetConncetAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");
 
        // Setting Dialog Message
        alertDialog.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");
 
        // On pressing Settings button
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	fillForms();
            }
        });
        // Showing Alert Message
        alertDialog.show();
	}
	public void showNoInternetConncetAlert_1(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");
 
        // Setting Dialog Message
        alertDialog.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");
 
        // On pressing Settings button
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	getForms();
            }
        });
        // Showing Alert Message
        alertDialog.show();
	}
	JSONObject form_left=null;
	JSONObject form_right=null;
	private class GetForm extends AsyncTask<String, String, String>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... args) {
			try{
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair(Admin.COMPANY_ID,args[0]));
				final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForParkingIncidentForm(),
						"GET", params);
				// check log cat from response


				if(json.has(Admin.FORM_LEFT)){
					form_left = json.getJSONObject(Admin.FORM_LEFT);
				}
				if(json.has(Admin.FORM_RIGHT)){
					try {
						form_right = json.getJSONObject(Admin.FORM_RIGHT);
					}catch (Exception e){
						form_right = null;
					}
				}
/*
				Iterator<String> iterator= json.keys();
				TreeSet<String> set=new TreeSet<String>();
				while(iterator.hasNext()){
					String key=iterator.next();
					set.add(key);
				}
*/
				Iterator<String> iterator_left= form_left.keys();
				TreeSet<String> set_left=new TreeSet<String>();
				while(iterator_left.hasNext()){
					String key=iterator_left.next();
					set_left.add(key);
				}
				Iterator<String> iterator_right = null;
				if(form_right!=null) {
					iterator_right = form_right.keys();
				}
				TreeSet<String> set_right=new TreeSet<String>();
				if(iterator_right!=null) {
					while (iterator_right.hasNext()) {
						String key = iterator_right.next();
						set_right.add(key);
					}
				}
//				final Iterator<String>	iterator1=set.iterator();
				final Iterator<String>	iterator1_left=set_left.iterator();
				final Iterator<String>	iterator1_right=set_right.iterator();
				/*
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(json.has(Admin.HEADING)){
							try {
								textView.setText(json.getString(Admin.HEADING));
								json.remove(Admin.HEADING);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				});
				*/
				//json.remove("success");
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						int ID=1001;
						while(iterator1_left.hasNext()){
							try{
//								Hashtable<String, Object> data_dynamic=new Hashtable<String, Object>();
								Hashtable<String, Object[]> data_dynamic=new Hashtable<String, Object[]>();
								String str=iterator1_left.next();
								LinearLayout layout_inner=new LinearLayout(getActivity());
								layout_inner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
								layout_inner.setOrientation(LinearLayout.VERTICAL);
								layout_inner.setPadding(0, 5, 0, 5);
								//layout_inner.setBackgroundResource(R.color.bg_log_row_even);
								layout_inner.setBackgroundColor(Color.parseColor("#e4e4e4"));
								if(form_left.has(str))
								{
									String data=form_left.getString(str);
									MixedArray mixedArray=Pherialize.unserialize(data).toArray();
									if(mixedArray.getString(1).equalsIgnoreCase(Admin.TEXTFIELD)){
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										final 	EditText textfield=new EditText(getActivity());
										textfield.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
										textfield.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
										textfield.setSingleLine(true);
										textfield.setId(ID++);
										textfield.addTextChangedListener(new TextWatcher() {
											@Override
											public void onTextChanged(CharSequence s, int start, int before, int count) {
												textfield.setError(null);
											}
											@Override
											public void beforeTextChanged(CharSequence s, int start, int count,
																		  int after) {

											}
											@Override
											public void afterTextChanged(Editable s) {

											}
										});
										textfield.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(textfield);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
										data_dynamic.put(Admin.TEXTFIELD, new Object[]{textfield,mixedArray.getString(3)});
									}else if(mixedArray.getString(1).equalsIgnoreCase(Admin.TEXTAREA)){
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										final	EditText textarea=new EditText(getActivity());
										textarea.setSingleLine(false);
										textarea.setId(ID++);
										textarea.setTypeface(Admin.getFontRoboto(getActivity()));
										textarea.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
										textarea.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
										textarea.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
										textarea.addTextChangedListener(new TextWatcher() {

											@Override
											public void onTextChanged(CharSequence s, int start, int before, int count) {
												textarea.setError(null);
											}

											@Override
											public void beforeTextChanged(CharSequence s, int start, int count,
																		  int after) {

											}
											@Override
											public void afterTextChanged(Editable s) {

											}
										});
										layout_inner.addView(textarea);
//										data_dynamic.put(Admin.TEXTAREA, textarea);
										data_dynamic.put(Admin.TEXTAREA, new Object[]{textarea,mixedArray.getString(3)});

									}else if(mixedArray.getString(1).equalsIgnoreCase(Admin.SELECT)){
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										String string_array[]=mixedArray.getString(2).split("\n");
										ArrayList<String> spinnerArray=new ArrayList<String>();
										spinnerArray.add(("Select "+mixedArray.getString(0)));
										for(String s:string_array){
											spinnerArray.add(s);
										}
										Spinner spinner=new Spinner(getActivity());
										ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,spinnerArray);
										spinner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
										spinner.setAdapter(arrayAdapter);
										spinner.setId(ID++);
										layout_inner.addView(spinner);
//										data_dynamic.put(Admin.SELECT,spinner);
										data_dynamic.put(Admin.SELECT, new Object[]{spinner,mixedArray.getString(3)});
									}else if(mixedArray.getString(1).equalsIgnoreCase(Admin.CHECKBOX)){
										String string_array[]=mixedArray.getString(2).split("\n");
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										int check=0;
										Hashtable<String, CheckBox> checkboxes_Hashtable=new Hashtable<String, CheckBox>();
										for(String s:string_array){
											CheckBox checkBox=new CheckBox(getActivity());
											checkBox.setText(s.trim());
											checkBox.setId(ID++);
											checkBox.setPadding(0, 0, 10, 0);
											checkBox.setTypeface(Admin.getFontRoboto(getActivity()));
											layout_inner.addView(checkBox);
											checkboxes_Hashtable.put(String.valueOf(check++).concat(Admin.CHECKBOX), checkBox);
										}
//										data_dynamic.put(Admin.CHECKBOX,checkboxes_Hashtable);
										data_dynamic.put(Admin.CHECKBOX, new Object[]{checkboxes_Hashtable,mixedArray.getString(3)});
									}
									else if(mixedArray.getString(1).equalsIgnoreCase(Admin.RADIO)){
										String string_array[]=mixedArray.getString(2).split("\n");
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										RadioGroup radioGroup=new RadioGroup(getActivity());
										radioGroup.setOrientation(RadioGroup.VERTICAL);
										for(String s:string_array){
											RadioButton radio=new RadioButton(getActivity());
											radio.setText(s.trim());
											radio.setId(ID++);
											radio.setChecked(true);
											radio.setTypeface(Admin.getFontRoboto(getActivity()));
											radioGroup.addView(radio);
										}
										radioGroup.setId(ID++);
										layout_inner.addView(radioGroup);
//										data_dynamic.put(Admin.RADIO, radioGroup);
										data_dynamic.put(Admin.RADIO, new Object[]{radioGroup,mixedArray.getString(3)});
									}
								}
								//dynamic_form_data.put(str, data_dynamic);
								dynamic_form_data_left.put(str, data_dynamic);
								linearLayout.addView(layout_inner);
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						LinearLayout layout_=new LinearLayout(getActivity());
						layout_.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						layout_.setOrientation(LinearLayout.VERTICAL);
						layout_.setPadding(15, 5, 15, 5);
						linearLayout.addView(layout_);
						while(iterator1_right.hasNext()){
							try{
//								Hashtable<String, Object> data_dynamic=new Hashtable<String, Object>();
								Hashtable<String, Object[]> data_dynamic=new Hashtable<String, Object[]>();
								String str=iterator1_right.next();
								LinearLayout layout_inner=new LinearLayout(getActivity());
								layout_inner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
								layout_inner.setOrientation(LinearLayout.VERTICAL);
								layout_inner.setPadding(0, 5, 0, 5);
								layout_inner.setBackgroundColor(Color.parseColor("#dddcdc"));
								if(form_right.has(str))
								{
									String data=form_right.getString(str);
									MixedArray mixedArray=Pherialize.unserialize(data).toArray();
									Log.d("Mixed Array",mixedArray.toString());
									if(mixedArray.getString(1).equalsIgnoreCase(Admin.TEXTFIELD)){
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										final 	EditText textfield=new EditText(getActivity());
										textfield.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
										textfield.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
										textfield.setSingleLine(true);
										textfield.setId(ID++);
										textfield.addTextChangedListener(new TextWatcher() {
											@Override
											public void onTextChanged(CharSequence s, int start, int before, int count) {
												textfield.setError(null);
											}
											@Override
											public void beforeTextChanged(CharSequence s, int start, int count,
																		  int after) {

											}
											@Override
											public void afterTextChanged(Editable s) {

											}
										});
										textfield.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(textfield);
//										data_dynamic.put(Admin.TEXTFIELD, textfield);
										data_dynamic.put(Admin.TEXTFIELD, new Object[]{textfield,mixedArray.getString(3)});
									}else if(mixedArray.getString(1).equalsIgnoreCase(Admin.TEXTAREA)){
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										final	EditText textarea=new EditText(getActivity());
										textarea.setSingleLine(false);
										textarea.setId(ID++);
										textarea.setTypeface(Admin.getFontRoboto(getActivity()));
										textarea.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
										textarea.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
										textarea.setHint(mixedArray.getString(2).toUpperCase(Locale.getDefault()));
										textarea.addTextChangedListener(new TextWatcher() {

											@Override
											public void onTextChanged(CharSequence s, int start, int before, int count) {
												textarea.setError(null);
											}

											@Override
											public void beforeTextChanged(CharSequence s, int start, int count,
																		  int after) {

											}
											@Override
											public void afterTextChanged(Editable s) {

											}
										});
										layout_inner.addView(textarea);
//										data_dynamic.put(Admin.TEXTAREA, textarea);
										data_dynamic.put(Admin.TEXTAREA, new Object[]{textarea,mixedArray.getString(3)});

									}else if(mixedArray.getString(1).equalsIgnoreCase(Admin.SELECT)){
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										String string_array[]=mixedArray.getString(2).split("\n");
										ArrayList<String> spinnerArray=new ArrayList<String>();
										spinnerArray.add(("Select "+mixedArray.getString(0)));
										for(String s:string_array){
											spinnerArray.add(s);
										}
										Spinner spinner=new Spinner(getActivity());
										ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,spinnerArray);
										spinner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
										spinner.setAdapter(arrayAdapter);
										spinner.setId(ID++);
										layout_inner.addView(spinner);
//										data_dynamic.put(Admin.SELECT,spinner);
										data_dynamic.put(Admin.SELECT, new Object[]{spinner,mixedArray.getString(3)});
									}else if(mixedArray.getString(1).equalsIgnoreCase(Admin.CHECKBOX)){
										String string_array[]=mixedArray.getString(2).split("\n");
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										int check=0;
										Hashtable<String, CheckBox> checkboxes_Hashtable=new Hashtable<String, CheckBox>();
										for(String s:string_array){
											CheckBox checkBox=new CheckBox(getActivity());
											checkBox.setText(s.trim());
											checkBox.setId(ID++);
											checkBox.setPadding(0, 0, 10, 0);
											checkBox.setTypeface(Admin.getFontRoboto(getActivity()));
											layout_inner.addView(checkBox);
											checkboxes_Hashtable.put(String.valueOf(check++).concat(Admin.CHECKBOX), checkBox);
										}
//										data_dynamic.put(Admin.CHECKBOX,checkboxes_Hashtable);
										data_dynamic.put(Admin.CHECKBOX, new Object[]{checkboxes_Hashtable,mixedArray.getString(3)});
									}
									else if(mixedArray.getString(1).equalsIgnoreCase(Admin.RADIO)){
										String string_array[]=mixedArray.getString(2).split("\n");
										TextView txt=new TextView(getActivity());
										txt.setText(mixedArray.getString(0));
										txt.setTypeface(Typeface.DEFAULT_BOLD);
										txt.setId(ID++);
										txt.setTypeface(Admin.getFontRoboto(getActivity()));
										layout_inner.addView(txt);
										RadioGroup radioGroup=new RadioGroup(getActivity());
										radioGroup.setOrientation(RadioGroup.VERTICAL);
										for(String s:string_array){
											RadioButton radio=new RadioButton(getActivity());
											radio.setText(s.trim());
											radio.setId(ID++);
											radio.setChecked(true);
											radio.setTypeface(Admin.getFontRoboto(getActivity()));
											radioGroup.addView(radio);
										}
										radioGroup.setId(ID++);
										layout_inner.addView(radioGroup);
//										data_dynamic.put(Admin.RADIO, radioGroup);
										data_dynamic.put(Admin.RADIO, new Object[]{radioGroup,mixedArray.getString(3)});
									}
								}
//								dynamic_form_data.put(str, data_dynamic);
								dynamic_form_data_right.put(str, data_dynamic);
								linearLayout.addView(layout_inner);
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				});
			}catch(Exception  e){
				Log.d("Error : ", e.getMessage());
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			linearLayout.addView(button);
		}
	}
	private class FillForm extends AsyncTask<List<NameValuePair>, String, String>{
		int success=0;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog=new ProgressDialog(getActivity());
			progressDialog.setMessage("Update...");
			progressDialog.setIndeterminate(false);
			progressDialog.setCancelable(true);			
			progressDialog.show();
		}
		@Override
		protected String doInBackground(List<NameValuePair>... params) {
			final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForParkingIncidentFormFill(),
					"GET", params[0]);						
			try{
				if(json.has(Admin.TAG_SUCCESS)){
					success=json.getInt(Admin.TAG_SUCCESS);
				}
			}catch(Exception e){
				Log.d("Error", e.getMessage());
			}			
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(success==1){
				progressDialog.setMessage("Successfully Update");			
				if(dynamic_form_data_left.size()>0){
					Enumeration<String> enum_key=dynamic_form_data_left.keys();
					while (enum_key.hasMoreElements()) {
						String string = (String) enum_key.nextElement();
						Hashtable<String, Object[]> dynamic_form_field=(Hashtable<String, Object[]>)dynamic_form_data_left.get(string);
						if(dynamic_form_field.size()>0){
							Enumeration<String> enum_key_field=dynamic_form_field.keys();
							while (enum_key_field.hasMoreElements()) {
								String string2 = (String) enum_key_field.nextElement();
								if(string2.equalsIgnoreCase(Admin.TEXTFIELD)){
									Object object[]=(Object[])dynamic_form_field.get(string2);
									EditText text=(EditText)object[0];
										text.setText("");
								}else if(string2.equalsIgnoreCase(Admin.TEXTAREA)){
									Object object[]=(Object[])dynamic_form_field.get(string2);
									EditText text=(EditText)object[0];
									text.setText("");
								}else if(string2.equalsIgnoreCase(Admin.SELECT)){
									Object object[]=(Object[])dynamic_form_field.get(string2);
									Spinner spinner=(Spinner)object[0];
									//spinner.setSelected(false);
									spinner.setSelection(0);
								}else if(string2.equalsIgnoreCase(Admin.RADIO)){
									//RadioGroup radioGroup=(RadioGroup)dynamic_form_field.get(string2);
									//int selected=radioGroup.getCheckedRadioButtonId();
									//RadioButton radioButton=(RadioButton)getActivity().findViewById(selected);									
								}else if(string2.endsWith(Admin.CHECKBOX)){
									Object object[]=(Object[])dynamic_form_field.get(string2);
									@SuppressWarnings("unchecked")
									Hashtable<String, CheckBox> che=(Hashtable<String, CheckBox>)object[0];
									Enumeration<CheckBox> e=che.elements();
									while (e.hasMoreElements()) {
										CheckBox checkBox = (CheckBox) e.nextElement();											
										if(checkBox.isChecked()){
											checkBox.setChecked(false);
										}
									}
								}
							}
						}
					}	
				}			
			}else{
				progressDialog.setMessage("Unsuccessfully Update");				
			}
			progressDialog.dismiss();
		}
	}
	public void refreshFrom(){
		/*
		Toast toast =new Toast(getActivity().getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setView(toast_view);
		toast.show();
		*/

		if(dynamic_form_data_left.size()>0){
			Enumeration<String> enum_key=dynamic_form_data_left.keys();
			while (enum_key.hasMoreElements()) {
				String string = (String) enum_key.nextElement();
				Hashtable<String, Object[]> dynamic_form_field=(Hashtable<String, Object[]>)dynamic_form_data_left.get(string);
				if(dynamic_form_field.size()>0){
					Enumeration<String> enum_key_field=dynamic_form_field.keys();
					while (enum_key_field.hasMoreElements()) {
						String string2 = (String) enum_key_field.nextElement();
						if(string2.equalsIgnoreCase(Admin.TEXTFIELD)){
							Object object[]=(Object[])dynamic_form_field.get(string2);
							EditText text=(EditText)object[0];
								text.setText("");
						}else if(string2.equalsIgnoreCase(Admin.TEXTAREA)){
							Object object[]=(Object[])dynamic_form_field.get(string2);
							EditText text=(EditText)object[0];
							text.setText("");
						}else if(string2.equalsIgnoreCase(Admin.SELECT)){
							Object object[]=(Object[])dynamic_form_field.get(string2);
							Spinner spinner=(Spinner)object[0];
							//spinner.setSelected(false);
							spinner.setSelection(0);
							
						}else if(string2.equalsIgnoreCase(Admin.RADIO)){
							//RadioGroup radioGroup=(RadioGroup)dynamic_form_field.get(string2);
							//int selected=radioGroup.getCheckedRadioButtonId();
							//RadioButton radioButton=(RadioButton)getActivity().findViewById(selected);									
						}else if(string2.endsWith(Admin.CHECKBOX)){
							Object object[]=(Object[])dynamic_form_field.get(string2);
							@SuppressWarnings("unchecked")
							Hashtable<String, CheckBox> che=(Hashtable<String, CheckBox>)object[0];
							Enumeration<CheckBox> e=che.elements();
							while (e.hasMoreElements()) {
								CheckBox checkBox = (CheckBox) e.nextElement();											
								if(checkBox.isChecked()){
									checkBox.setChecked(false);
								}
							}
						}
					}
				}
			}			
		}
		if(dynamic_form_data_right!=null){
			if(dynamic_form_data_right.size()>0){
				Enumeration<String> enum_key=dynamic_form_data_right.keys();
				while (enum_key.hasMoreElements()) {
					String string = (String) enum_key.nextElement();
					Hashtable<String, Object[]> dynamic_form_field=(Hashtable<String, Object[]>)dynamic_form_data_right.get(string);
					if(dynamic_form_field.size()>0){
						Enumeration<String> enum_key_field=dynamic_form_field.keys();
						while (enum_key_field.hasMoreElements()) {
							String string2 = (String) enum_key_field.nextElement();
							if(string2.equalsIgnoreCase(Admin.TEXTFIELD)){
								Object object[]=(Object[])dynamic_form_field.get(string2);
								EditText text=(EditText)object[0];
									text.setText("");
							}else if(string2.equalsIgnoreCase(Admin.TEXTAREA)){
								Object object[]=(Object[])dynamic_form_field.get(string2);
								EditText text=(EditText)object[0];
								text.setText("");
							}else if(string2.equalsIgnoreCase(Admin.SELECT)){
								Object object[]=(Object[])dynamic_form_field.get(string2);
								Spinner spinner=(Spinner)object[0];
								//spinner.setSelected(false);
								spinner.setSelection(0);

							}else if(string2.equalsIgnoreCase(Admin.RADIO)){
								//RadioGroup radioGroup=(RadioGroup)dynamic_form_field.get(string2);
								//int selected=radioGroup.getCheckedRadioButtonId();
								//RadioButton radioButton=(RadioButton)getActivity().findViewById(selected);
							}else if(string2.endsWith(Admin.CHECKBOX)){
								Object object[]=(Object[])dynamic_form_field.get(string2);
								@SuppressWarnings("unchecked")
								Hashtable<String, CheckBox> che=(Hashtable<String, CheckBox>)object[0];
								Enumeration<CheckBox> e=che.elements();
								while (e.hasMoreElements()) {
									CheckBox checkBox = (CheckBox) e.nextElement();
									if(checkBox.isChecked()){
										checkBox.setChecked(false);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//super.onActivityResult(requestCode, resultCode, data);
		refreshFrom();
	}
}
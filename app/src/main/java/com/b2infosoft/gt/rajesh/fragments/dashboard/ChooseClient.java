package com.b2infosoft.gt.rajesh.fragments.dashboard;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.SlideMenu;
import com.b2infosoft.gt.server.JSONParser;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChooseClient extends Fragment {
    public static final String TAG = ChooseClient.class.getSimpleName();
    public static SharedPreferences settings;
    private Spinner client_name;
    JSONParser jsonParser = new JSONParser();
    private View mView;
    private ProgressBar progressBar;
    private Button start;
    private Hashtable<String, String> treeMap;

    private void navigate(final Fragment paramFragment, final String paramString) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                SlideMenu.setActiveFragment(ChooseClient.this.getFragmentManager(), paramFragment, paramString);
            }
        });
    }

    public static ChooseClient newInstance() {
        return new ChooseClient();
    }

    private void parseResult(JSONObject paramJSONObject) {
        try {
            this.treeMap = new Hashtable();
            if ((paramJSONObject.has("success")) && (paramJSONObject.getInt("success") == 1) && (paramJSONObject.has("client"))) {
                Log.d("client list response", paramJSONObject.toString());
                JSONArray localJSONArray = paramJSONObject.getJSONArray("client");
                for (int i = 0; i < localJSONArray.length(); i++) {
                    JSONObject localJSONObject = localJSONArray.getJSONObject(i);
                    if ((localJSONObject.has("client_id")) && (localJSONObject.has("client_name"))) {
                        this.treeMap.put(localJSONObject.getString("client_id"), localJSONObject.getString("client_name"));
                    }
                }
            }
            return;
        } catch (Exception localException) {
            Log.d("Error", localException.getMessage());
        }
    }

    private void showMessage(String paramString) {
        Toast.makeText(getActivity(), paramString, Toast.LENGTH_SHORT).show();
    }

    public String getClientId() {
        String value = this.client_name.getSelectedItem().toString().trim();
        for (Entry<String, String> entry : this.treeMap.entrySet()) {
            if (value.equals(entry.getValue().trim())) {
                return (String) entry.getKey();
            }
        }
        return null;
    }

    public String getClientName() {
        return this.client_name.getSelectedItem().toString().trim();
    }

    public boolean isClientSelected() {
        if (this.client_name.getSelectedItemPosition() != 0) {
            return true;
        }
        ((TextView) this.client_name.getSelectedView()).setError("Please Choose Client");
        return false;
    }

    public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {
        settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
        View view = paramLayoutInflater.inflate(R.layout.fragment_dashboard_new_choose_client_layout, paramViewGroup, false);
        this.progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        this.mView = view.findViewById(R.id.choose_client_view);
        this.start = (Button) view.findViewById(R.id.client_start);
        this.client_name = (Spinner) view.findViewById(R.id.client_name_list);
        this.start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                if (!ChooseClient.this.isClientSelected()) {
                    return;
                }
                new ChooseClient.StartSchedule().execute(new String[0]);
            }
        });
        new GetClient().execute(new String[0]);
        ChooseClient.this.getActivity().setTitle("dashboard".toUpperCase(Locale.getDefault()));
        return view;
    }

    private class EmpStatus
            extends AsyncTask<String, String, JSONObject> {
        private EmpStatus() {
        }

        protected JSONObject doInBackground(String... paramVarArgs) {
            List<NameValuePair> pairs = new ArrayList();
            pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
            return ChooseClient.this.jsonParser.makeHttpRequest(Admin.getUrlForEmpStatus(), "POST", pairs);
        }

        protected void onPostExecute(JSONObject object) {
            ChooseClient.this.progressBar.setVisibility(View.GONE);
            ChooseClient.this.mView.setVisibility(View.VISIBLE);

            int schedule = 0;
            int punch = 0;
            int client = 0;
            if (object != null) {
                try {
                    if (object.has(Admin.TAG_SUCCESS)) {
                        int success = object.getInt(Admin.TAG_SUCCESS);
                    }
                    if (object.has(Admin.SCHEDULE_STATUS)) {
                        schedule = object.getInt(Admin.SCHEDULE_STATUS);
                    }
                    if (object.has(Admin.CLIENT_STATUS)) {
                        client = object.getInt(Admin.CLIENT_STATUS);
                    }
                    if (object.has(Admin.PUNCH_STATUS)) {
                        punch = object.getInt(Admin.PUNCH_STATUS);
                    }

                    String client_name_ = "";
                    String client_id_ = "";
                    if (client == 1) {
                        if (object.has(Admin.CLIENT_NAME)) {
                            client_name_ = object.getString(Admin.CLIENT_NAME);
                            MySharedPreferences.setSharedPreferences(ChooseClient.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        }
                        if (object.has(Admin.CLIENT_ID)) {
                            MySharedPreferences.setSharedPreferences(ChooseClient.this.getActivity(), Admin.CLIENT_ID, object.getString(Admin.CLIENT_ID));
                        }
                        ChooseClient.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));

                    } else {
                        MySharedPreferences.setSharedPreferences(ChooseClient.this.getActivity(), Admin.CLIENT_ID, client_id_);
                        MySharedPreferences.setSharedPreferences(ChooseClient.this.getActivity(), Admin.CLIENT_NAME, client_name_);

                        ChooseClient.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON", e.toString());
                }
            }
            /*
            System.out.println("SCHEDULE  "+TAG+schedule);
            System.out.println("PUNCH "+TAG+punch);
            System.out.println("CLIENT "+TAG+client);
            */
            if (schedule == 1 && client == 0) {
                //ChooseClient.this.navigate(ScheduleActive.newInstance(), ScheduleActive.TAG);
            } else if (schedule != 0 || client != 0) {
                switch (punch) {
                    case 0:
                        ChooseClient.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                        break;
                    case 1:
                        ChooseClient.this.navigate(SessionStart.newInstance(), SessionStart.TAG);
                        break;
                    case 2:
                        ChooseClient.this.navigate(SessionBreak.newInstance(), SessionBreak.TAG);
                        break;
                    case 3:
                        if (client != 1) {
                            //ChooseClient.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
                            break;
                        } else {
                            ChooseClient.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                            break;
                        }
                    default:
                        break;
                }
            } else {

                //ChooseClient.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
            }
            if (punch == 1) {
                MySharedPreferences.setSharedPreferences(ChooseClient.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_START);
            } else {
                MySharedPreferences.setSharedPreferences(ChooseClient.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_END);
            }
        }

        protected void onPreExecute() {
            ChooseClient.this.progressBar.setVisibility(View.VISIBLE);
            ChooseClient.this.mView.setVisibility(View.GONE);
        }
    }

    public class GetClient
            extends AsyncTask<String, Void, Integer> {
        public GetClient() {
        }

        protected Integer doInBackground(String... paramVarArgs) {
            List<NameValuePair> pairs = new ArrayList<>();
            pairs.add(new BasicNameValuePair("company_id", settings.getString("company_id", "")));
            JSONObject localJSONObject = ChooseClient.this.jsonParser.makeHttpRequest(Admin.getUrlForGetAllClient(), "POST", pairs);
            try {
                if (localJSONObject.getInt("success") == 1) {
                    ChooseClient.this.parseResult(localJSONObject);
                    return Integer.valueOf(1);
                }
                Integer localInteger = Integer.valueOf(0);
                return localInteger;
            } catch (JSONException localJSONException) {
                localJSONException.printStackTrace();
                Log.e("My Exception", localJSONException.toString());
            }
            return Integer.valueOf(0);
        }

        protected void onPostExecute(Integer paramInteger) {
            ChooseClient.this.progressBar.setVisibility(View.GONE);
            ChooseClient.this.mView.setVisibility(View.VISIBLE);
            if ((paramInteger.intValue() == 1) && (ChooseClient.this.treeMap != null) && (ChooseClient.this.treeMap.size() > 0)) {
                ArrayAdapter localArrayAdapter = new ArrayAdapter(ChooseClient.this.getActivity(),android.R.layout.simple_spinner_dropdown_item);
                localArrayAdapter.add("Choose Client");
                Enumeration localEnumeration = ChooseClient.this.treeMap.elements();
                while (localEnumeration.hasMoreElements()) {
                    localArrayAdapter.add((String) localEnumeration.nextElement());
                }
                ChooseClient.this.client_name.setAdapter(localArrayAdapter);
            }
        }

        protected void onPreExecute() {
            ChooseClient.this.progressBar.setVisibility(View.VISIBLE);
            ChooseClient.this.mView.setVisibility(View.GONE);
        }
    }

    private class StartSchedule extends AsyncTask<String, String, JSONObject> {
        private StartSchedule() {
        }

        protected JSONObject doInBackground(String... paramVarArgs) {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(new BasicNameValuePair("user_id",settings.getString("user_id", "")));
            localArrayList.add(new BasicNameValuePair("company_id", settings.getString("company_id", "")));
            localArrayList.add(new BasicNameValuePair("client_id", ChooseClient.this.getClientId()));
            return ChooseClient.this.jsonParser.makeHttpRequest(Admin.getUrlForChooseClient(), "POST", localArrayList);
        }

        protected void onPostExecute(JSONObject paramJSONObject) {
            ChooseClient.this.progressBar.setVisibility(View.GONE);
            ChooseClient.this.mView.setVisibility(View.VISIBLE);
            String str1 = "";
            String str2 = "";
            try {
                boolean bool = paramJSONObject.has("success");
                int i = 0;
                if (bool) {
                    i = paramJSONObject.getInt("success");
                }
                if (i == 1) {
                    if (paramJSONObject.has("client_id")) {
                        str1 = paramJSONObject.getString("client_id");
                    }
                    if (paramJSONObject.has("client_name")) {
                        str2 = paramJSONObject.getString("client_name");
                    }
                    MySharedPreferences.setSharedPreferences(ChooseClient.this.getActivity(), "client_name", str2);
                    MySharedPreferences.setSharedPreferences(ChooseClient.this.getActivity(), "client_id", str1);
                    ChooseClient.this.getActivity().setTitle(str2);
                    new ChooseClient.EmpStatus().execute(new String[0]);
                }
                return;
            } catch (JSONException localJSONException) {
                Log.e("GetSchedule", localJSONException.toString());
            }
        }

        protected void onPreExecute() {
            ChooseClient.this.progressBar.setVisibility(View.VISIBLE);
            ChooseClient.this.mView.setVisibility(View.GONE);
        }
    }
}
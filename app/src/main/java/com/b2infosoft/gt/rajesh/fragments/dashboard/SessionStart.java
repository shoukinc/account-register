package com.b2infosoft.gt.rajesh.fragments.dashboard;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.SlideMenu;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.rajesh.fragments.fragment_list_option;
import com.b2infosoft.gt.rajesh.sign_2.CaptureSignature;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.services.LocationUpdate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class SessionStart extends Fragment implements LocationListener {
    public static final String TAG = SessionStart.class.getSimpleName();
    private static TextView clock;
    private static SharedPreferences settings;
    private Handler handler = new Handler();
    private int hours_1;
    JSONParser jsonParser = new JSONParser();
    private double latitude;
    private String location = "";
    private LocationManager locationManager;
    private double longitude;
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            update();
            handler.postDelayed(mUpdateResults, 1000L);
        }
    };
    private View mView;
    private int minutes_1;
    private ProgressBar progressBar;
    private String provider;
    private int seconds_1;
    private Button session_end;
    private Button take_a_break;
    private TextView mCompanyName;

    private void navigate(final Fragment paramFragment, final String paramString) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                SlideMenu.setActiveFragment(SessionStart.this.getFragmentManager(), paramFragment, paramString);
            }
        });
    }

    public static SessionStart newInstance() {
        return new SessionStart();
    }

    private void update() {
        this.seconds_1++;
        if (this.seconds_1 >= 60) {
            this.seconds_1 = 0;
            if (this.minutes_1 < 59) {
                this.minutes_1++;
            } else if (this.hours_1 < 23) {
                this.minutes_1 = 0;
                this.hours_1++;
            } else {
                this.minutes_1 = 0;
                this.hours_1 = 0;
            }
        }
        if (this.seconds_1 % 2 == 0) {
            clock.setText(String.format("%02d:%02d", new Object[]{Integer.valueOf(this.hours_1), Integer.valueOf(this.minutes_1)}));
            return;
        }
        clock.setText(String.format("%02d %02d", new Object[]{Integer.valueOf(this.hours_1), Integer.valueOf(this.minutes_1)}));
    }

    public String getCompleteAddressString(double paramDouble1, double paramDouble2) {
        String localObject = "";
        Geocoder localGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List localList = localGeocoder.getFromLocation(paramDouble1, paramDouble2, 1);
            if (localList != null) {
                Address localAddress = (Address) localList.get(0);
                StringBuilder localStringBuilder = new StringBuilder("");
                for (int i = 0; i < localAddress.getMaxAddressLineIndex(); i++) {
                    localStringBuilder.append(localAddress.getAddressLine(i)).append("\n");
                }
                String str = localStringBuilder.toString();
                localObject = str;
            }
            return localObject;
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return localObject;
    }

    public boolean isServiceRunning(String paramString) {
        Iterator localIterator = ((ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE)).getRunningServices(Integer.MAX_VALUE).iterator();
        while (localIterator.hasNext()) {
            if (((ActivityManager.RunningServiceInfo) localIterator.next()).service.getClassName().equals(paramString)) {
                return true;
            }
        }
        return false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle paramBundle) {
        settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
        this.locationManager = ((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE));
        Criteria localCriteria = new Criteria();
        localCriteria.setSpeedRequired(false);
        localCriteria.setAccuracy(1);
        localCriteria.setCostAllowed(true);
        localCriteria.setBearingAccuracy(3);
        localCriteria.setAltitudeRequired(false);
        this.provider = this.locationManager.getBestProvider(localCriteria, false);
        View view = inflater.inflate(R.layout.fragment_dashboard_new_session_start_layout, container, false);
        this.progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        this.mView = view.findViewById(R.id.punch_in_view);
        clock = (TextView) view.findViewById(R.id.dashboard_clock);
        this.take_a_break = (Button) view.findViewById(R.id.take_a_break_button);
        this.mCompanyName = (TextView) view.findViewById(R.id.mCompanyName);

        if(!TextUtils.isEmpty(MySharedPreferences.getInstance(getActivity()).getCompanyName())){
            mCompanyName.setText(MySharedPreferences.getInstance(getActivity()).getCompanyName());
        }else{
            mCompanyName.setVisibility(View.GONE);
        }

        //Call to the Edit Form clicked on Edit Icon
        view.findViewById(R.id.icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, fragment_list_option.newInstance(),
                                DashboardNew.TAG).commit();*/
                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.content_frame, fragment_list_option.newInstance(),
                                DashboardNew.TAG).addToBackStack(null).commit();
                }
        });
         this.take_a_break.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                if (SessionStart.this.locationManager.isProviderEnabled("gps")) {
                    Location localLocation = SessionStart.this.locationManager.getLastKnownLocation(SessionStart.this.provider);
                    if (localLocation != null) {
                        SessionStart.this.latitude = localLocation.getLatitude();
                        SessionStart.this.longitude = localLocation.getLongitude();
                        SessionStart.this.location = SessionStart.this.getCompleteAddressString(SessionStart.this.latitude, SessionStart.this.longitude);
                        AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(SessionStart.this.getActivity());
                        localBuilder2.setTitle("Alert");
                        localBuilder2.setMessage("Do you want to Take a Break?");
                        localBuilder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                SessionStart.CheckIn localCheckIn = new SessionStart.CheckIn();
                                String[] arrayOfString = new String[7];
                                arrayOfString[0] = settings.getString("user_id", "");
                                arrayOfString[1] = String.valueOf(SessionStart.this.latitude);
                                arrayOfString[2] = String.valueOf(SessionStart.this.longitude);
                                arrayOfString[3] = "PUNCH OUT";
                                arrayOfString[4] = SessionStart.this.location;
                                arrayOfString[5] = MySharedPreferences.getSharedPreferences(SessionStart.this.getActivity(), "client_id");
                                arrayOfString[6] = "2";
                                localCheckIn.execute(arrayOfString);
                            }
                        });
                        localBuilder2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                paramAnonymous2DialogInterface.dismiss();
                            }
                        });
                        localBuilder2.show();
                        return;
                    }
                    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(SessionStart.this.getActivity());
                    localBuilder1.setTitle("Alert");
                    localBuilder1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
                    localBuilder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            paramAnonymous2DialogInterface.dismiss();
                        }
                    });
                    localBuilder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            SessionStart.CheckIn localCheckIn = new SessionStart.CheckIn();
                            String[] arrayOfString = new String[7];
                            arrayOfString[0] = settings.getString("user_id", "");
                            arrayOfString[1] = String.valueOf(SessionStart.this.latitude);
                            arrayOfString[2] = String.valueOf(SessionStart.this.longitude);
                            arrayOfString[3] = "PUNCH OUT";
                            arrayOfString[4] = SessionStart.this.location;
                            arrayOfString[5] = MySharedPreferences.getSharedPreferences(SessionStart.this.getActivity(), "client_id");
                            arrayOfString[6] = "2";
                            localCheckIn.execute(arrayOfString);
                        }
                    });
                    localBuilder1.create().show();
                    return;
                }
                SessionStart.this.showSettingsAlert();
            }
        });
        this.session_end = ((Button) view.findViewById(R.id.session_end_button));
        this.session_end.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                if (SessionStart.this.locationManager.isProviderEnabled("gps")) {
                    Location localLocation = SessionStart.this.locationManager.getLastKnownLocation(SessionStart.this.provider);
                    if (localLocation != null) {
                        SessionStart.this.latitude = localLocation.getLatitude();
                        SessionStart.this.longitude = localLocation.getLongitude();
                        SessionStart.this.location = SessionStart.this.getCompleteAddressString(SessionStart.this.latitude, SessionStart.this.longitude);
                        AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(SessionStart.this.getActivity());
                        localBuilder2.setTitle("Alert");
                        localBuilder2.setMessage("Do you want to End Session?");
                        localBuilder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                Intent localIntent = new Intent(SessionStart.this.getActivity(), CaptureSignature.class);
                                String[] arrayOfString = new String[7];
                                arrayOfString[0] = settings.getString("user_id", "");
                                arrayOfString[1] = String.valueOf(SessionStart.this.latitude);
                                arrayOfString[2] = String.valueOf(SessionStart.this.longitude);
                                arrayOfString[3] = "PUNCH OUT";
                                arrayOfString[4] = SessionStart.this.location;
                                arrayOfString[5] = MySharedPreferences.getSharedPreferences(SessionStart.this.getActivity(), "client_id");
                                arrayOfString[6] = "3";
                                Bundle localBundle = new Bundle();
                                localBundle.putString("user_id", settings.getString("user_id", ""));
                                localBundle.putString("company_id", settings.getString("company_id", ""));
                                localBundle.putString("latitude", SessionStart.this.latitude + "");
                                localBundle.putString("longitude", SessionStart.this.longitude + "");
                                localBundle.putString("check", "PUNCH OUT");
                                localBundle.putString("location", SessionStart.this.location);
                                localBundle.putString("client_id", MySharedPreferences.getSharedPreferences(SessionStart.this.getActivity(), "client_id"));
                                localBundle.putString("check_out_status", "3");
                                localIntent.putExtra("data", localBundle);
                                localIntent.putExtra("url", Admin.getUrlForCheck_In_Out());
                                SessionStart.this.getActivity().startActivityForResult(localIntent, 1);
                            }
                        });
                        localBuilder2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                                paramAnonymous2DialogInterface.dismiss();
                            }
                        });
                        localBuilder2.show();
                        return;
                    }
                    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(SessionStart.this.getActivity());
                    localBuilder1.setTitle("Alert");
                    localBuilder1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
                    localBuilder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            paramAnonymous2DialogInterface.dismiss();
                        }
                    });
                    localBuilder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
                            Intent localIntent = new Intent(SessionStart.this.getActivity(), CaptureSignature.class);
                            Bundle localBundle = new Bundle();
                            localBundle.putString("user_id", settings.getString("user_id", ""));
                            localBundle.putString("company_id", settings.getString("company_id", ""));
                            localBundle.putString("latitude", SessionStart.this.latitude + "");
                            localBundle.putString("longitude", SessionStart.this.longitude + "");
                            localBundle.putString("check", "PUNCH OUT");
                            localBundle.putString("location", SessionStart.this.location);
                            localBundle.putString("client_id", MySharedPreferences.getSharedPreferences(SessionStart.this.getActivity(), "client_id"));
                            localBundle.putString("check_out_status", "3");
                            localIntent.putExtra("data", localBundle);
                            localIntent.putExtra("url", Admin.getUrlForCheck_In_Out());
                            SessionStart.this.getActivity().startActivityForResult(localIntent, 1);
                        }
                    });
                    localBuilder1.create().show();
                    return;
                }
                SessionStart.this.showSettingsAlert();
            }
        });
        return view;
    }

    public void onLocationChanged(Location paramLocation) {
        this.latitude = paramLocation.getLatitude();
        this.longitude = paramLocation.getLongitude();
        this.location = getCompleteAddressString(this.latitude, this.longitude);
    }

    public void onPause() {
        super.onPause();
        this.handler.removeCallbacks(this.mUpdateResults);
    }

    public void onProviderDisabled(String paramString) {
    }

    public void onProviderEnabled(String paramString) {
        onLocationChanged(this.locationManager.getLastKnownLocation(paramString));
    }

    public void onResume() {
        super.onResume();
        if(getActivity()!=null) {
            new EmpStatus().execute(new String[0]);
        }
    }

    public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle) {
    }

    public void onStop() {
        super.onStop();
        this.handler.removeCallbacks(this.mUpdateResults);
    }

    public void showNoInternetConncetAlert() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle("No Internet Connection");
        localBuilder.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");
        localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                if (!new Network(SessionStart.this.getActivity()).isConnectingToInternet()) {
                    SessionStart.this.showNoInternetConncetAlert();
                }
            }
        });
        localBuilder.show();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle("GPS is settings");
        localBuilder.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        localBuilder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                Intent localIntent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                SessionStart.this.getActivity().startActivityForResult(localIntent, 1);
            }
        });
        localBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                paramAnonymousDialogInterface.cancel();
            }
        });
        localBuilder.show();
    }

    private class CheckIn extends AsyncTask<String, String, String> {
        private String action = "";
        private String message = "";

        private CheckIn() {
        }

        protected String doInBackground(String... paramVarArgs) {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(new BasicNameValuePair("user_id", paramVarArgs[0]));
            localArrayList.add(new BasicNameValuePair("company_id", settings.getString("company_id", "")));
            localArrayList.add(new BasicNameValuePair("latitude", paramVarArgs[1]));
            localArrayList.add(new BasicNameValuePair("longitude", paramVarArgs[2]));
            localArrayList.add(new BasicNameValuePair("check", paramVarArgs[3]));
            localArrayList.add(new BasicNameValuePair("location", paramVarArgs[4]));
            localArrayList.add(new BasicNameValuePair("client_id", paramVarArgs[5]));
            localArrayList.add(new BasicNameValuePair("check_out_status", paramVarArgs[6]));
            JSONObject localJSONObject = SessionStart.this.jsonParser.makeHttpRequest(Admin.getUrlForCheck_In_Out(), "POST", localArrayList);
          //  Log.d("Response Holi", localJSONObject.toString());
            try {
                if (localJSONObject.getInt("success") == 1) {
                    if (localJSONObject.has("action")) {
                        this.action = localJSONObject.getString("action");
                    }
                    if (localJSONObject.has("client_id")) {
                        SessionStart.this.getActivity().getSharedPreferences("rajesh", 0).edit().putString("client_id", localJSONObject.getString("client_id")).commit();
                    }
                    if (localJSONObject.has("message")) {
                        this.message = localJSONObject.getString("message");
                    }
                }
            } catch (JSONException localJSONException) {
                localJSONException.printStackTrace();
                Log.d("My Exception", localJSONException.toString());
            }
            return null;
        }

        protected void onPostExecute(String paramString) {
            SessionStart.this.progressBar.setVisibility(View.GONE);
            SessionStart.this.mView.setVisibility(View.VISIBLE);
            new SessionStart.EmpStatus().execute(new String[0]);
            if (!this.action.equalsIgnoreCase("PUNCH IN") && !this.action.equalsIgnoreCase("PUNCH OUT")) {
                Toast.makeText(SessionStart.this.getActivity(), this.message, Toast.LENGTH_SHORT).show();
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
            SessionStart.this.progressBar.setVisibility(View.VISIBLE);
            SessionStart.this.mView.setVisibility(View.GONE);
        }
    }

    private class EmpStatus extends AsyncTask<String, String, JSONObject> {
        private EmpStatus() {
        }

        protected JSONObject doInBackground(String... paramVarArgs) {
            List<NameValuePair> pairs = new ArrayList();
            pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
            return jsonParser.makeHttpRequest(Admin.getUrlForEmpStatus(), "POST", pairs);
        }
        protected void onPostExecute(JSONObject object) {
            SessionStart.this.progressBar.setVisibility(View.GONE);
            SessionStart.this.mView.setVisibility(View.VISIBLE);
         //   Log.d("RESULT",object.toString());
            int schedule = 0;
            int punch = 0;
            int client = 0;
            if (object != null) {
                try {
                    if (object.has(Admin.TAG_SUCCESS)) {
                        int success = object.getInt(Admin.TAG_SUCCESS);
                    }
                    if (object.has(Admin.SCHEDULE_STATUS)) {
                        schedule = object.getInt(Admin.SCHEDULE_STATUS);
                    }
                    if (object.has(Admin.CLIENT_STATUS)) {
                        client = object.getInt(Admin.CLIENT_STATUS);
                    }
                    if (object.has(Admin.PUNCH_STATUS)) {
                        punch = object.getInt(Admin.PUNCH_STATUS);
                    }

                    String client_name_ = "";
                    String client_id_ = "";
                    if (client == 1) {
                        if (object.has(Admin.CLIENT_NAME)) {
                            client_name_ = object.getString(Admin.CLIENT_NAME);
                            MySharedPreferences.setSharedPreferences(SessionStart.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        }
                        if (object.has(Admin.CLIENT_ID)) {
                            MySharedPreferences.setSharedPreferences(SessionStart.this.getActivity(), Admin.CLIENT_ID, object.getString(Admin.CLIENT_ID));
                        }
                        SessionStart.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                    } else {
                        MySharedPreferences.setSharedPreferences(SessionStart.this.getActivity(), Admin.CLIENT_ID, client_id_);
                        MySharedPreferences.setSharedPreferences(SessionStart.this.getActivity(), Admin.CLIENT_NAME, client_name_);
                        SessionStart.this.getActivity().setTitle(client_name_.toUpperCase(Locale.getDefault()));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON", e.toString());
                }
            }

            System.out.println("SCHEDULE  "+TAG+schedule);
            System.out.println("PUNCH "+TAG+punch);
            System.out.println("CLIENT "+TAG+client);


            if (schedule == 1 && client == 0) {
                SessionStart.this.navigate(ScheduleActive.newInstance(), ScheduleActive.TAG);
            } else if (schedule != 0 || client != 0) {
                switch (punch) {
                    case 0:
                        SessionStart.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                        break;
                    case 1:
                        new TimerTime().execute(new String[0]);
                        if (!SessionStart.this.isServiceRunning("com.example.securitygard.services.LocationUpdate")) {
                            SessionStart.this.getActivity().startService(new Intent(SessionStart.this.getActivity(), LocationUpdate.class));
                            break;
                        }
                        //SessionStart.this.navigate(SessionStart.newInstance(), SessionStart.TAG);
                        break;
                    case 2:
                        SessionStart.this.navigate(SessionBreak.newInstance(), SessionBreak.TAG);
                        break;
                    case 3:
                        if (client != 1) {
                            SessionStart.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
                            break;
                        } else {
                            SessionStart.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                            break;
                        }
                    default:
                        break;
                }
            } else {
                SessionStart.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
            }
            if (punch == 1) {
                MySharedPreferences.setSharedPreferences(SessionStart.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_START);
            } else {
                MySharedPreferences.setSharedPreferences(SessionStart.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_END);
            }
        }

        protected void onPreExecute() {
            SessionStart.this.progressBar.setVisibility(View.VISIBLE);
            SessionStart.this.mView.setVisibility(View.GONE);
        }
    }

    private class TimerTime extends AsyncTask<String, String, String> {
        private TimerTime() {

        }

        protected String doInBackground(String... paramVarArgs) {
            ArrayList localArrayList = new ArrayList();

            localArrayList.add(new BasicNameValuePair("user_id", settings.getString("user_id", "")));
            localArrayList.add(new BasicNameValuePair("company_id", settings.getString("company_id", "")));
            localArrayList.add(new BasicNameValuePair("client_id",settings.getString("client_id", "")));
            final JSONObject localJSONObject = SessionStart.this.jsonParser.makeHttpRequest(Admin.getUrlForTotalTime(), "POST", localArrayList);
            try {
              //  Log.d(TAG+" DATA",localJSONObject.toString());
                if ((localJSONObject.has("success")) && (localJSONObject.getInt("success") == 1) && (localJSONObject.has("total_time"))) {
                    final String str = localJSONObject.getString("total_time");
                 //   Log.d(TAG+" DATA",localJSONObject.toString());
                    if(SessionStart.this.getActivity()!=null) {
                        SessionStart.this.getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                String[] arrayOfString = str.split(":");
                                int i = Integer.parseInt(arrayOfString[0]);
                                int j = Integer.parseInt(arrayOfString[1]);
                                int k = Integer.parseInt(arrayOfString[2]);
                                hours_1 = i;
                                minutes_1 = j;
                                seconds_1 = k;
                                try {
                                    if (localJSONObject.has("status")) {
                                        int m = localJSONObject.getInt("status");
                                        SessionStart.this.update();
                                        if (m == 1) {
                                            SessionStart.this.handler.postDelayed(mUpdateResults, 1000L);
                                        }
                                    }
                                    return;
                                } catch (JSONException localJSONException) {
                                    Log.e(TAG + " ERROR", localJSONException.toString());
                                }
                            }
                        });
                    }
                }
                return null;
            } catch (JSONException localJSONException) {
                Log.e("Exception  : ", localJSONException.getMessage());
            }
            return null;
        }

        protected void onPostExecute(String paramString) {
            super.onPostExecute(paramString);
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
}

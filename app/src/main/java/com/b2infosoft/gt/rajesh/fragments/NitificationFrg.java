package com.b2infosoft.gt.rajesh.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.MainActivity;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.custom.FileUtils;
import com.b2infosoft.gt.rajesh.modals.NotificationBean;
import com.b2infosoft.gt.rajesh.sign_2.CaptureSignature;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class NitificationFrg extends Fragment implements View.OnClickListener {

    private ListView mListView;
    private static NitificationFrg notificationPage;
    private ArrayList<NotificationBean> beanArrayList;
    private PnAdapter adapter;
    public static final String TAG = "NitificationFrg";


    public static NitificationFrg getInstance() {

        return notificationPage;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        notificationPage = this;

        View view = inflater.inflate(R.layout.nitification_frg, container, false);

        intiateView(view);
        return view;
    }

    public void intiateView(View v) {
        mListView = (ListView) v.findViewById(R.id.list);


       /* ArrayList<NotificationBean> oldArrayList = MySharedPreferences.getInstance(getActivity()).getNotificationList();
        if (oldArrayList == null||oldArrayList.size()==0) {
            oldArrayList = new ArrayList<>();

            NotificationBean bean = new NotificationBean();
            bean.setImage_url("");
            bean.setTitle("Titlejvgbgf");
            bean.setMessage("Message jkasfbsbddjfk");
            bean.setNotification_id("45");
            long time= System.currentTimeMillis();
            bean.setTime(time);
            oldArrayList.add(bean);
            oldArrayList.add(bean);
            oldArrayList.add(bean);
            oldArrayList.add(bean);
            oldArrayList.add(bean);
            oldArrayList.add(bean);
            oldArrayList.add(bean);
            oldArrayList.add(bean);
            MySharedPreferences.getInstance(getActivity()).saveNotificationList(oldArrayList);
        }
*/

        beanArrayList = MySharedPreferences.getInstance(getActivity()).getNotificationList();

        if (beanArrayList != null) {
            adapter = new PnAdapter(getActivity(), beanArrayList);
            adapter.setOnClickListener(this);
            mListView.setAdapter(adapter);
        }
        // MySharedPreferences.getInstance(getActivity()).saveNotificationList(new ArrayList<NotificationBean>());

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.mCloseBtn:
                int pos = (int) view.getTag();
                beanArrayList.remove(pos);
                adapter.setArrayList(beanArrayList);
                adapter.notifyDataSetChanged();
                MySharedPreferences.getInstance(getActivity()).saveNotificationList(beanArrayList);
                break;
            case R.id.mClickRowBtn:
                int pos2 = (int) view.getTag();

                NotificationBean bean = beanArrayList.get(pos2);

                if (!(TextUtils.isEmpty(bean.getNotification_id()) || bean.getNotification_id().equalsIgnoreCase("null"))) {
                    Intent intent = new Intent(getActivity(), CaptureSignature.class);
                    intent.putExtra("title", bean.getTitle());
                    intent.putExtra("notification_id", bean.getNotification_id());
                    startActivity(intent);

                    beanArrayList.remove(pos2);
                    adapter.setArrayList(beanArrayList);
                    adapter.notifyDataSetChanged();
                    MySharedPreferences.getInstance(getActivity()).saveNotificationList(beanArrayList);
                }

                break;
        }

    }


    class PnAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater inflater;
        private View.OnClickListener onClickListener;
        private ArrayList<NotificationBean> arrayList;

        public PnAdapter(Activity context, ArrayList<NotificationBean> beanArrayList) {
            this.context = context;
            inflater = context.getLayoutInflater();
            this.arrayList = beanArrayList;
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view1, ViewGroup viewGroup) {

            View view = view1;
            ViewHolder holder = null;
            if (view == null) {

                view = inflater.inflate(R.layout.row_notification_item, null);
                holder = new PnAdapter.ViewHolder();
                holder.mClickRowBtn = (RelativeLayout) view.findViewById(R.id.mClickRowBtn);
                holder.mFullImage = (ImageView) view.findViewById(R.id.mFullImage);
                holder.mFullImageNo = (ImageView) view.findViewById(R.id.mFullImageNo);
                holder.mCloseBtn = (ImageView) view.findViewById(R.id.mCloseBtn);
                holder.mTitle = (TextView) view.findViewById(R.id.mTitle);
                holder.mMessage = (TextView) view.findViewById(R.id.mMessage);
                holder.mTimeTxt = (TextView) view.findViewById(R.id.mTimeTxt);

                view.setTag(R.layout.row_notification_item, holder);
            } else {
                holder = (ViewHolder) view.getTag(R.layout.row_notification_item);
            }
            holder.mCloseBtn.setOnClickListener(onClickListener);
            holder.mClickRowBtn.setOnClickListener(onClickListener);
            holder.mCloseBtn.setTag(i);
            holder.mClickRowBtn.setTag(i);

            NotificationBean bean = arrayList.get(i);
            holder.mTitle.setText(bean.getTitle());
            holder.mMessage.setText(bean.getMessage());
            holder.mTimeTxt.setText(FileUtils.parsedlastseen(String.valueOf((bean.getTime()))));

            if (!TextUtils.isEmpty(bean.getImage_url())) {
                try {
                    holder.mFullImage.setVisibility(View.VISIBLE);
                    Picasso.with(context)
                            .load(bean.getImage_url())
                            .placeholder(R.drawable.menu_schedule)
                            .into(holder.mFullImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                holder.mFullImage.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(bean.getLogo_url())) {
                try {

                    Picasso.with(context)
                            .load(bean.getLogo_url())
                            .placeholder(R.drawable.ic_launcher)
                            .into(holder.mFullImageNo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                holder.mFullImageNo.setImageResource(R.drawable.ic_launcher);
            }


            return view;
        }

        public ArrayList<NotificationBean> getArrayList() {
            return arrayList;
        }

        public void setArrayList(ArrayList<NotificationBean> arrayList) {
            this.arrayList = arrayList;
        }

        class ViewHolder {

            ImageView mFullImageNo;
            ImageView mFullImage;
            ImageView mCloseBtn;
            TextView mTitle;
            TextView mMessage;
            TextView mTimeTxt;
            RelativeLayout mClickRowBtn;

        }

        public View.OnClickListener getOnClickListener() {
            return onClickListener;
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
        }


    }
}

package com.b2infosoft.gt.rajesh.fragments;

import java.util.Locale;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.rajesh.adapter.TabPagerAdapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
public class LogReport extends Fragment  implements OnPageChangeListener,OnTabChangeListener{
	public static final String TAG=LogReport.class.getSimpleName();
	public static LogReport newInstance() {
		return new LogReport();
	}
	private	ViewPager viewPager;
	private  TabPagerAdapter tabPagerAdapter;
	// Tab titles
	private String []tabs={"LOG","MONTH","YEAR"};
	TabHost tabHost;
	@SuppressLint("NewApi") @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			View view=inflater.inflate(R.layout.fragments_outer_log_report_layout, container, false);
			// initialization
			getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));

			tabHost=view.findViewById(android.R.id.tabhost);
	    	tabHost.setup();
	    	for (int i=0;i<tabs.length;i++){
	    		TabHost.TabSpec tabSpec;
	    		tabSpec=tabHost.newTabSpec(tabs[i]);
	    		tabSpec.setIndicator(tabs[i]);	    	
	    		tabSpec.setContent(new FakeContent(getActivity().getApplicationContext()));
	    		tabHost.addTab(tabSpec);
	    	}
	    	tabHost.setOnTabChangedListener(this);			
			tabPagerAdapter=new TabPagerAdapter(getChildFragmentManager());			
			viewPager=(ViewPager)view.findViewById(R.id.view_pager);			
			viewPager.setAdapter(tabPagerAdapter);
			viewPager.setOnPageChangeListener(this);
			setSelectedTabColor();
			return view;
	}
	public class FakeContent implements TabContentFactory{    	
    	Context context;
    	public FakeContent(Context context) {
    		this.context=context;
    	}
    	@Override
		public View createTabContent(String tag) {
    		View view=new View(context);
    		view.setMinimumHeight(0);
    		view.setMinimumWidth(0);
			return view;
		}    	
    }
    
	@Override
	public void onTabChanged(String tabId) {
		int selected_item=tabHost.getCurrentTab();
		viewPager.setCurrentItem(selected_item);
	}
	@Override
	public void onPageScrollStateChanged(int arg0) {

	}
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		
	}
	@Override
	public void onPageSelected(int selected) {
		tabHost.setCurrentTab(selected);
		setSelectedTabColor();
	}
	private void setSelectedTabColor() {
	        for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)  
	        {  
	            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#0ab6db")); 
	            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
	            tv.setTextColor(Color.parseColor("#ffffff"));
	            tv.setTypeface(Admin.getFontRoboto(getActivity()),Typeface.BOLD);
	        }  
	        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#ffffff"));
            TextView tv = (TextView) tabHost.getCurrentTabView().findViewById(android.R.id.title);
            tv.setTypeface(Admin.getFontRoboto(getActivity()),Typeface.BOLD);
            tv.setTextColor(Color.parseColor("#838181"));
	}
}

package com.b2infosoft.gt.rajesh.fragments;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.Admin.MyHandler;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.custom.DigitalClock;
import com.b2infosoft.gt.gps.GPSTracker;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.services.LocationUpdate;
import com.b2infosoft.gt.rajesh.sign_2.CaptureSignature;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
public class DashBoard extends Fragment implements LocationListener {

	private static final int TIME_INTERVAL = 10000;
	private long mBackPressed=0;
	private static SharedPreferences settings;
	private LocationManager locationManager;
	private String provider;
	private double latitude;
	private double longitude;
	private String location = "";
	GPSTracker tracker=new GPSTracker(getActivity());
	public static final String TAG = DashBoard.class.getSimpleName();
	private ProgressDialog progressDialog;
	JSONParser jsonParser = new JSONParser();		
	Timestamp time_punch_in,time_punch_out;	
	static	TextView hour_minute, seconds;
	Button check_In_out,take_a_break,end_session,end_session_1,resume_session,choose_client;
	Spinner spinner,spinner2;
	LinearLayout deshboard_clock_layout;
	public static DashBoard newInstance() {
		return new DashBoard();
	}	
	private	Hashtable<String, String> treeMap ;
	private static DigitalClock digitalClock;
	int setview=0;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {					
		View view = inflater.inflate(R.layout.fragments_outer_deshboard_layout,
				container, false);
		//mBackPressed = System.currentTimeMillis();
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);                
        spinner = (Spinner)view.findViewById(R.id.deshboard_client_name);         
        deshboard_clock_layout = (LinearLayout)view.findViewById(R.id.deshboard_clock_layout);         
        choose_client = (Button)view.findViewById(R.id.deshboard_choose_client);        
        take_a_break=(Button)view.findViewById(R.id.deshboard_take_a_break);
        end_session=(Button)view.findViewById(R.id.deshboard_end_session);
        end_session_1=(Button)view.findViewById(R.id.deshboard_end_session_1);        
        resume_session=(Button)view.findViewById(R.id.deshboard_resume_session);
		digitalClock = (DigitalClock)view.findViewById(R.id.dashboard_total_time);
        Criteria criteria = new Criteria();
        criteria.setSpeedRequired(false);
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setCostAllowed(true);
        criteria.setBearingAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setAltitudeRequired(false);
        provider = locationManager.getBestProvider(criteria, false);
//      provider = locationManager.getBestProvider(criteria, true);        
		settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
		hour_minute = (TextView) view.findViewById(R.id.deshboard_hour_minutes);
		seconds = (TextView) view.findViewById(R.id.deshboard_seconds);
		
		hour_minute.setTypeface(Admin.getFontRoboto(getActivity()));
		seconds.setTypeface(Admin.getFontRoboto(getActivity()));

		choose_client.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(!isClientSelected()){
					return;								
				}
				MySharedPreferences.setSharedPreferences(getActivity(), Admin.CLIENT_DATE, Admin.getDateTodayString());
				MySharedPreferences.setSharedPreferences(getActivity(), Admin.CLIENT_NAME, getClientName());
				MySharedPreferences.setSharedPreferences(getActivity(), Admin.CLIENT_ID, getClientId());

				getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));				
				if(choose_client.getVisibility()==View.VISIBLE){
					choose_client.setVisibility(View.GONE);
				}				
				if(spinner.getVisibility()==View.VISIBLE){
					spinner.setVisibility(View.GONE);
				}
				mBackPressed = System.currentTimeMillis();
				checkInStatus();
				/*
				if(check_In_out.getVisibility()==View.GONE){
					check_In_out.setVisibility(View.VISIBLE);
				}
				*/
			}
		});		
		check_In_out = (Button) view.findViewById(R.id.deshboard_check_in_out);
		check_In_out.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {				
				/*
				if(!isClientSelected()){
					return;								
				}					
				*/
				/*
				AlertDialog.Builder         builder1 = new AlertDialog.Builder(getActivity());
			    builder1.setTitle("Test");
			    builder1
			            .setMessage("test");
			    builder1.setPositiveButton("Call Now",
			            new DialogInterface.OnClickListener() {
			                public void onClick(DialogInterface dialog, int id) {
			                    dialog.cancel();
			                }
			            });
			    builder1.setNeutralButton("Setup",
			            new DialogInterface.OnClickListener() {
			                public void onClick(DialogInterface dialog, int id) {
			                    //dialog.cancel();
			                }
			            });
			    builder1.setNegativeButton("Exit",
			            new DialogInterface.OnClickListener() {
			                public void onClick(DialogInterface dialog, int id) {
			                    dialog.cancel();
			                }
			            });
			    builder1.create().show();
			    */
								
				
				if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
					Location location_11=locationManager.getLastKnownLocation(provider);
					if(location_11!=null){
						latitude=location_11.getLatitude();
						longitude=location_11.getLongitude();
						location=getCompleteAddressString(latitude, longitude);		
						//Toast.makeText(getActivity(), "Working Punch", Toast.LENGTH_SHORT).show();										
						if ((check_In_out.getText().toString()).equalsIgnoreCase("PUNCH OUT")) {						
							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
							builder.setTitle("Alert");
							builder.setMessage("Do you want to Punch Out?");
							builder.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog,
												int which) {
											new CheckIn().execute(
													new String[] {
															settings.getString(Admin.USER_ID, ""),
															String.valueOf(latitude),String.valueOf(longitude),
															String.valueOf(check_In_out.getText().toString().toUpperCase(Locale.getDefault())),
															location,
															MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
															getEmployeeBreakType()
														});
										}
									});
							builder.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog,
												int which) {
											dialog.dismiss();
										}
									});
							builder.show();
						} else {					
							new CheckIn().execute(
									new String[] {
											settings.getString(Admin.USER_ID, ""),
											String.valueOf(latitude),String.valueOf(longitude),
											String.valueOf(check_In_out.getText().toString().toUpperCase(Locale.getDefault())),
											location,
											MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
											getEmployeeBreakType()
										});
						}
					}else{				
						AlertDialog.Builder builder_1=new AlertDialog.Builder(getActivity());
						builder_1.setTitle("Alert");
						builder_1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
						builder_1.setNegativeButton("No", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						builder_1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								new CheckIn().execute(
										new String[] {
												settings.getString(Admin.USER_ID, ""),
												String.valueOf(latitude),String.valueOf(longitude),
												String.valueOf(check_In_out.getText().toString().toUpperCase(Locale.getDefault())),
												location,
												MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
												getEmployeeBreakType()
											});									
							}
						});
						builder_1.create().show();
					}
				}else{
					showSettingsAlert();
				}				
			}
		});
		take_a_break.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
					Location location_11=locationManager.getLastKnownLocation(provider);
					if(location_11!=null){
						latitude=location_11.getLatitude();
						longitude=location_11.getLongitude();
						location=getCompleteAddressString(latitude, longitude);							
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setTitle("Alert");
						builder.setMessage("Do you want to Take a Break?");
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										new CheckIn().execute(
												new String[] {
														settings.getString(Admin.USER_ID, ""),
														String.valueOf(latitude),String.valueOf(longitude),
														"PUNCH OUT",
														location,
														MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
														"2"
												});
									}
								});
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								});
						builder.show();
					}else{				
						AlertDialog.Builder builder_1=new AlertDialog.Builder(getActivity());
						builder_1.setTitle("Alert");
						builder_1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
						builder_1.setNegativeButton("No", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						builder_1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								new CheckIn().execute(
										new String[] {
												settings.getString(Admin.USER_ID, ""),
												String.valueOf(latitude),String.valueOf(longitude),
												"PUNCH OUT",
												location,
												MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
												"2"											
									});
							}
						});
						builder_1.create().show();
					}
				}else{
					showSettingsAlert();
				}
			}
		});
		
		resume_session.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
					Location location_11=locationManager.getLastKnownLocation(provider);
					if(location_11!=null){
						latitude=location_11.getLatitude();
						longitude=location_11.getLongitude();
						location=getCompleteAddressString(latitude, longitude);							
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setTitle("Alert");
						builder.setMessage("Do you want to Resume session?");
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										new CheckIn().execute(
												new String[] {
														settings.getString(Admin.USER_ID, ""),
														String.valueOf(latitude),String.valueOf(longitude),
														"PUNCH IN",
														location,
														MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
														"1"
												});
									}
								});
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								});
						builder.show();
					}else{				
						AlertDialog.Builder builder_1=new AlertDialog.Builder(getActivity());
						builder_1.setTitle("Alert");
						builder_1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
						builder_1.setNegativeButton("No", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						builder_1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								new CheckIn().execute(
										new String[] {
												settings.getString(Admin.USER_ID, ""),
												String.valueOf(latitude),String.valueOf(longitude),
												"PUNCH IN",
												location,
												MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
												"1"											
									});
							}
						});
						builder_1.create().show();
					}
				}else{
					showSettingsAlert();
				}
			}
		});
		
		end_session.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
					Location location_11=locationManager.getLastKnownLocation(provider);
					if(location_11!=null){
						latitude=location_11.getLatitude();
						longitude=location_11.getLongitude();
						location=getCompleteAddressString(latitude, longitude);							
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setTitle("Alert");
						builder.setMessage("Do you want to End Session?");
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										
										/*
										new CheckIn().execute(
												new String[] {
														settings.getString(Admin.USER_ID, ""),
														String.valueOf(latitude),String.valueOf(longitude),
														"PUNCH OUT",
														location,
														MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
														"3"
												});
										*/								
										Intent  intent= new Intent(getActivity(),CaptureSignature.class);
										
										
										String []data = new String[] {
												settings.getString(Admin.USER_ID, ""),
												String.valueOf(latitude),String.valueOf(longitude),
												"PUNCH OUT",
												location,
												MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
												"3"
										};
										
										Bundle bundle =new Bundle();
										bundle.putString(Admin.USER_ID,settings.getString(Admin.USER_ID, ""));
										bundle.putString(Admin.COMPANY_ID,settings.getString(Admin.COMPANY_ID, ""));
										bundle.putString(Admin.LATITUDE, latitude+"");
										bundle.putString(Admin.LONGITUDE, longitude+"");
										bundle.putString(Admin.CHECK, "PUNCH OUT");
										bundle.putString(Admin.LOCATION, location);
										bundle.putString(Admin.CLIENT_ID,MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID));
										bundle.putString(Admin.PUNCH_OUT_STATUS, "3");
										//bundle.putStringArray("DATA",data);							
										intent.putExtra(Admin.DATA,bundle);
										intent.putExtra(Admin.URL,Admin.getUrlForCheck_In_Out());
										getActivity().startActivityForResult(intent,Admin.REQUEST_CODE_LOG_OUT);	
									
									}
								});
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								});
						builder.show();
					}else{				
						AlertDialog.Builder builder_1=new AlertDialog.Builder(getActivity());
						builder_1.setTitle("Alert");
						builder_1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
						builder_1.setNegativeButton("No", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						builder_1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								/*
								new CheckIn().execute(
										new String[] {
												settings.getString(Admin.USER_ID, ""),
												String.valueOf(latitude),String.valueOf(longitude),
												"PUNCH OUT",
												location,
												MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
												"3"											
									});
								*/
								
								Intent  intent= new Intent(getActivity(),CaptureSignature.class);
								Bundle bundle =new Bundle();
								bundle.putString(Admin.USER_ID,settings.getString(Admin.USER_ID, ""));
								bundle.putString(Admin.COMPANY_ID,settings.getString(Admin.COMPANY_ID, ""));
								bundle.putString(Admin.LATITUDE, latitude+"");
								bundle.putString(Admin.LONGITUDE, longitude+"");
								bundle.putString(Admin.CHECK, "PUNCH OUT");
								bundle.putString(Admin.LOCATION, location);
								bundle.putString(Admin.CLIENT_ID,MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID));
								bundle.putString(Admin.PUNCH_OUT_STATUS, "3");
								//bundle.putStringArray("DATA",data);							
								intent.putExtra(Admin.DATA,bundle);
								intent.putExtra(Admin.URL,Admin.getUrlForCheck_In_Out());								
								getActivity().startActivityForResult(intent,Admin.REQUEST_CODE_LOG_OUT);
							}
						});
						builder_1.create().show();
					}
				}else{
					showSettingsAlert();
				}
			}
		});		
		end_session_1.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
					Location location_11=locationManager.getLastKnownLocation(provider);
					if(location_11!=null){
						latitude=location_11.getLatitude();
						longitude=location_11.getLongitude();
						location=getCompleteAddressString(latitude, longitude);							
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setTitle("Alert");
						builder.setMessage("Do you want to End Session?");
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										
										/*
										new CheckIn().execute(
												new String[] {
														settings.getString(Admin.USER_ID, ""),
														String.valueOf(latitude),String.valueOf(longitude),
														"PUNCH OUT",
														location,
														MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
														"4"
												});
										*/
										Intent  intent= new Intent(getActivity(),CaptureSignature.class);
										Bundle bundle =new Bundle();
										bundle.putString(Admin.USER_ID,settings.getString(Admin.USER_ID, ""));
										bundle.putString(Admin.COMPANY_ID,settings.getString(Admin.COMPANY_ID, ""));
										bundle.putString(Admin.LATITUDE, latitude+"");
										bundle.putString(Admin.LONGITUDE, longitude+"");
										bundle.putString(Admin.CHECK, "PUNCH OUT");
										bundle.putString(Admin.LOCATION, location);
										bundle.putString(Admin.CLIENT_ID,MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID));
										bundle.putString(Admin.PUNCH_OUT_STATUS, "4");
										intent.putExtra(Admin.DATA,bundle);
										intent.putExtra(Admin.URL,Admin.getUrlForCheck_In_Out());										
										getActivity().startActivityForResult(intent,Admin.REQUEST_CODE_LOG_OUT);										
									}
								});
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								});
						builder.show();
					}else{				
						AlertDialog.Builder builder_1=new AlertDialog.Builder(getActivity());
						builder_1.setTitle("Alert");
						builder_1.setMessage("Your Current Location Not found. Do you want to Punch without location ?");
						builder_1.setNegativeButton("No", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						builder_1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								/*
								new CheckIn().execute(										
										new String[] {
												settings.getString(Admin.USER_ID, ""),
												String.valueOf(latitude),String.valueOf(longitude),
												"PUNCH OUT",
												location,
												MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID),
												"4"											
									});
							*/
								Intent  intent= new Intent(getActivity(),CaptureSignature.class);
								Bundle bundle =new Bundle();
								bundle.putString(Admin.USER_ID,settings.getString(Admin.USER_ID, ""));
								bundle.putString(Admin.COMPANY_ID,settings.getString(Admin.COMPANY_ID, ""));
								bundle.putString(Admin.LATITUDE, latitude+"");
								bundle.putString(Admin.LONGITUDE, longitude+"");
								bundle.putString(Admin.CHECK, "PUNCH OUT");
								bundle.putString(Admin.LOCATION, location);
								bundle.putString(Admin.CLIENT_ID,MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_ID));
								bundle.putString(Admin.PUNCH_OUT_STATUS, "4");
								intent.putExtra(Admin.DATA,bundle);
								intent.putExtra(Admin.URL,Admin.getUrlForCheck_In_Out());								
								getActivity().startActivityForResult(intent,Admin.REQUEST_CODE_LOG_OUT);
							}
						});
						builder_1.create().show();
					}
				}else{
					showSettingsAlert();
				}
			}
		});
		new GetTodaySchedule().execute();
		init();
		checkUpdates();			
		return view;
	}
	private void init(){
		if(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_DATE).equalsIgnoreCase(Admin.getDateTodayString())){
			checkInStatus();
		}else{
			if(choose_client.getVisibility()==View.GONE){
				choose_client.setVisibility(View.VISIBLE);
			}if(spinner.getVisibility()==View.GONE){
				spinner.setVisibility(View.VISIBLE);
			}
		}
		getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));
	}
	public void actionCheckOutIn(){
		
	}	
	public boolean isClientSelected(){
		if(spinner.getSelectedItemPosition()!=0){
			return true;
		}
		((TextView)spinner.getSelectedView()).setError("Please Choose Client");
		return false;
	}
	public String getClientId(){
	       String key= null;
	        String value=spinner.getSelectedItem().toString().trim();
	        for(Map.Entry<String,String> entry: treeMap.entrySet()){
	            if(value.equals(entry.getValue())){
	                key = entry.getKey();
	                break;
	            }
	        }
		return key;
	}
	public String getClientName(){
		return spinner.getSelectedItem().toString().trim();
	}
	public String getEmployeeBreakType(){	
	/*					
		if(spinner2.getVisibility()==View.VISIBLE)
		{
			if(spinner2.getSelectedItemPosition()==0){
				return "2";
			}else{
				return "3";			
			}
		}else{
			return "1";
		}
		*/
		return "1";
	}
	private void checkUpdates(){
		if(!new Network(getActivity()).isConnectingToInternet()){
			showNoInternetConncetAlert();
		}else{
			//new ClockTimeManage().execute(new String[] { settings.getString(Admin.USER_ID, "") });
			new TimerTime().execute();
			new GetClient().execute(new String[] { settings.getString(Admin.COMPANY_ID, "") });
		}
	}
	public void checkInStatus(){
		if(!new Network(getActivity()).isConnectingToInternet()){
			showNoInternetConncetAlert();
		}else{
			new CheckInStatus().execute(new String[] { settings.getString(
					Admin.USER_ID, "") });
		}
	}
	private class CheckIn extends AsyncTask<String, String, String> {
		private String action = "", message = "";		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage("Wait...");
			progressDialog.setIndeterminate(false);
			progressDialog.setCancelable(true);
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			progressDialog.show();
		}
		
		@Override
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(Admin.USER_ID, args[0]));
			params.add(new BasicNameValuePair(Admin.COMPANY_ID, settings.getString(Admin.COMPANY_ID, "")));
			params.add(new BasicNameValuePair(Admin.LATITUDE, args[1]));
			params.add(new BasicNameValuePair(Admin.LONGITUDE, args[2]));
			params.add(new BasicNameValuePair(Admin.CHECK, args[3]));
			params.add(new BasicNameValuePair(Admin.LOCATION, args[4]));
			params.add(new BasicNameValuePair(Admin.CLIENT_ID,args[5]));
			params.add(new BasicNameValuePair(Admin.PUNCH_OUT_STATUS, args[6]));
			// getting JSON Object
			// Note that create product url accepts POST method			
			JSONObject json = jsonParser.makeHttpRequest(
					Admin.getUrlForCheck_In_Out(), "POST", params);
			// check log cat from response
			Log.d("Response Holi", json.toString());
			// check for success tag
			try {
				int success = json.getInt(Admin.TAG_SUCCESS);
				if (success == 1) {
					// successfully created product
					// closing this screen
					if(json.has("action")) {
						action = json.getString("action");
					}
					if(json.has(Admin.CLIENT_ID)){
						SharedPreferences settings1=getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
						settings1.edit()
						.putString(Admin.CLIENT_ID,json.getString(Admin.CLIENT_ID))
						.commit();
					}
					if (json.has("message")) {
						message = json.getString("message");
					}
				} else {
					
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.d("My Exception", e.toString());
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (action.equalsIgnoreCase("PUNCH IN")) {
				//checkInSuccessfully();
				new CheckInStatus().execute(new String[] { settings.getString(
						Admin.USER_ID, "") });
			} else if (action.equalsIgnoreCase("PUNCH OUT")) {
				new CheckInStatus().execute(new String[] { settings.getString(
						Admin.USER_ID, "") });
			} else {
				Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT)
						.show();
			}
			checkUpdates();			
		}
	}
	private class ClockTimeManage extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(Admin.USER_ID, params[0]));
			JSONObject jsonObject = jsonParser.makeHttpRequest(
					Admin.getUrlForTimeUpdate(), "POST", param);
			String time_new = "", time_old = "";
			try {
				if (jsonObject.has(Admin.TIME_NEW)) {
					time_new = jsonObject.getString(Admin.TIME_NEW);
				}
				if (jsonObject.has(Admin.TIME_OLD)) {
					time_old=jsonObject.getString(Admin.TIME_OLD);
				}
				getActivity().getSharedPreferences(Admin.MYSECRATE, 0)
				.edit().putString(Admin.TIME_NEW, time_new)
				.putString(Admin.TIME_OLD, time_old)
				.commit();
			} catch (JSONException e) {
				Log.d("Exception  : ", e.getMessage());
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			MyHandler.HANDLER.postDelayed(clock, 1000);
		}
	}

	private class TimerTime extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair(Admin.USER_ID,settings.getString(Admin.USER_ID, "")));
			param.add(new BasicNameValuePair(Admin.COMPANY_ID,settings.getString(Admin.COMPANY_ID, "")));
			param.add(new BasicNameValuePair(Admin.CLIENT_ID,settings.getString(Admin.CLIENT_ID, "")));
		final JSONObject jsonObject = jsonParser.makeHttpRequest(Admin.getUrlForTotalTime(), "POST", param);
			try {
				if (jsonObject.has(Admin.TAG_SUCCESS)) {
					int success = jsonObject.getInt(Admin.TAG_SUCCESS);
					if(success==1){
						if (jsonObject.has(Admin.TOTAL_TIME)) {
							final String total_time =jsonObject.getString(Admin.TOTAL_TIME);
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									String s[] = total_time.split(":");
									int hours = Integer.parseInt(s[0]);
									int minutes = Integer.parseInt(s[1]);
									int seconds = Integer.parseInt(s[2]);
									//digitalClock.setTime(hours, minutes, seconds);
									hours_1 = hours;
									minutes_1 = minutes;
									seconds_1 = seconds;
									try{
									if(jsonObject.has(Admin.STATUS)){
										int status = jsonObject.getInt(Admin.STATUS);
										if(status==1){
											//digitalClock.startClock();
											MyHandler.HANDLER_1.postDelayed(mUpdateResults,1000);
										} else {
											MyHandler.HANDLER_1.removeCallbacks(mUpdateResults);
											//digitalClock.stopClock();
											//digitalClock.setText(String.format("%02d:%02d", hours, minutes));
										}
									}
									}catch (JSONException e){

									}
								}
							});
						}
					}else{

					}
				}
			} catch (JSONException e) {
				Log.d("Exception  : ", e.getMessage());
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			//Admin.HANDLER.postDelayed(clock, 1000);
		}
	}
	final static Runnable mUpdateResults = new Runnable() {
		public void run() {
			update();
			MyHandler.HANDLER_1.postDelayed(mUpdateResults, 1000);
		}
	};
	private static int hours_1;
	private static int minutes_1;
	private static int seconds_1;
	private static void  update() {
		seconds_1++;
		if (seconds_1 >= 60) {
			seconds_1 = 0;
			if (minutes_1 < 59) {
				minutes_1++;
			} else if (hours_1 < 23) {
				minutes_1 = 0;
				hours_1++;
			} else {
				minutes_1 = 0;
				hours_1 = 0;
			}
		}
		if (seconds_1 % 2 == 0) {
			digitalClock.setText(String.format("%02d:%02d", hours_1, minutes_1));
		} else {
			digitalClock.setText(String.format("%02d %02d", hours_1, minutes_1));
		}
	}
	private class CheckInStatus extends AsyncTask<String, String, Integer> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected Integer doInBackground(String... args) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(Admin.USER_ID, args[0]));
			JSONObject json = jsonParser.makeHttpRequest(
					Admin.getUrlForChack_In_Status_1(), "POST", params);			
			try {
				//Log.d("Response My", json.toString());
				int success = json.getInt(Admin.TAG_SUCCESS);

				Log.d("success", json + "");
				if (success == 1) {
					if(json.has(Admin.USER_ACTION)){					
						return Integer.parseInt(json.getString(Admin.USER_ACTION).trim());												
					}
				} else if(success == 0){
					// failed to create product
					//Log.d("Status", "Not Find");
					return 0;
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.d("My Exception", e.toString());
			}
			return 0;
		}
		@Override
		protected void onPostExecute(Integer result) {			
			switch (result) {
			case 0:			
				guardNotPunchIn();
				break;
			case 1:				
				guardPunchIn();
				break;
			case 2:	
				guardTakeBreak();
				break;
			case 3:
				guardEndSession();
				break;
			}	
		}
	}
	private void guardPunchIn(){
		getActivity().runOnUiThread(new Runnable() {			
			@Override
			public void run() {
				MySharedPreferences.setSharedPreferences(getActivity(), Admin.SESSION_STATUS, Admin.SESSION_START);				
				if(deshboard_clock_layout.getVisibility()==View.GONE){
					deshboard_clock_layout.setVisibility(View.VISIBLE);
				}
				if(take_a_break.getVisibility()==View.GONE){
					take_a_break.setVisibility(View.VISIBLE);
				}
				if(end_session.getVisibility()==View.GONE){
					end_session.setVisibility(View.VISIBLE);
				}				
				if(check_In_out.getVisibility() == View.VISIBLE){
					check_In_out.setVisibility(View.GONE);			
				}
				if(end_session_1.getVisibility()==View.VISIBLE){
					end_session_1.setVisibility(View.GONE);
				}
				if(resume_session.getVisibility()==View.VISIBLE){
					resume_session.setVisibility(View.GONE);
				}
				if(choose_client.getVisibility()==View.VISIBLE){
					choose_client.setVisibility(View.GONE);
				}if(spinner.getVisibility()==View.VISIBLE){
					spinner.setVisibility(View.GONE);
				}				
				if(!isServiceRunning("com.example.securitygard.services.LocationUpdate")){
					getActivity().startService(new Intent(getActivity(),LocationUpdate.class));
				}				
			}
		});
	}
	private void guardTakeBreak(){
		MySharedPreferences.setSharedPreferences(getActivity(), Admin.SESSION_STATUS, Admin.SESSION_BREAK);
		if(deshboard_clock_layout.getVisibility()==View.GONE){
			deshboard_clock_layout.setVisibility(View.VISIBLE);
		}
		if(resume_session.getVisibility()==View.GONE){
			resume_session.setVisibility(View.VISIBLE);
		}
		if(end_session_1.getVisibility()==View.GONE){
			end_session_1.setVisibility(View.VISIBLE);
		}
		if(check_In_out.getVisibility()==View.VISIBLE){
			check_In_out.setVisibility(View.GONE);
		}
		if(take_a_break.getVisibility()==View.VISIBLE){
			take_a_break.setVisibility(View.GONE);
		}
		if(end_session.getVisibility()==View.VISIBLE){
			end_session.setVisibility(View.GONE);
		}if(choose_client.getVisibility()==View.VISIBLE){
			choose_client.setVisibility(View.GONE);
		}if(spinner.getVisibility()==View.VISIBLE){
			spinner.setVisibility(View.GONE);
		}
		if(isServiceRunning("com.example.securitygard.services.LocationUpdate")){
			getActivity().stopService(new Intent(getActivity(),LocationUpdate.class));
		}
	}
	private void guardEndSession(){
		MySharedPreferences.setSharedPreferences(getActivity(), Admin.SESSION_STATUS, Admin.SESSION_END);
		if(mBackPressed!=0){
			if(mBackPressed + TIME_INTERVAL > System.currentTimeMillis()){
				if(spinner.getVisibility()==View.VISIBLE){
					spinner.setVisibility(View.GONE);
				}if(choose_client.getVisibility()==View.VISIBLE){
					choose_client.setVisibility(View.GONE);
				}
				if(check_In_out.getVisibility()==View.GONE){
					check_In_out.setVisibility(View.VISIBLE);
				}
			}else{
				if(spinner.getVisibility()==View.GONE){
					spinner.setVisibility(View.VISIBLE);
				}if(choose_client.getVisibility()==View.GONE){
					choose_client.setVisibility(View.VISIBLE);
				}
				if(check_In_out.getVisibility()==View.VISIBLE){
					check_In_out.setVisibility(View.GONE);
				}
			}
		}else{
			if(spinner.getVisibility()==View.GONE){
				spinner.setVisibility(View.VISIBLE);
			}if(choose_client.getVisibility()==View.GONE){
				choose_client.setVisibility(View.VISIBLE);
			}
			if(check_In_out.getVisibility()==View.VISIBLE){
				check_In_out.setVisibility(View.GONE);
			}
		}
		if(deshboard_clock_layout.getVisibility()==View.VISIBLE){
			deshboard_clock_layout.setVisibility(View.GONE);
		}if(take_a_break.getVisibility()==View.VISIBLE){
			take_a_break.setVisibility(View.GONE);
		}if(resume_session.getVisibility()==View.VISIBLE){
			resume_session.setVisibility(View.GONE);
		}if(end_session.getVisibility()==View.VISIBLE){
			end_session.setVisibility(View.GONE);
		}if(end_session_1.getVisibility()==View.VISIBLE){
			end_session_1.setVisibility(View.GONE);
		}if(isServiceRunning("com.example.securitygard.services.LocationUpdate")){
			getActivity().stopService(new Intent(getActivity(),LocationUpdate.class));
		}
	}
	private void guardEndSession_old(){
		MySharedPreferences.setSharedPreferences(getActivity(), Admin.SESSION_STATUS, Admin.SESSION_BREAK);
		if(deshboard_clock_layout.getVisibility()==View.GONE){
			deshboard_clock_layout.setVisibility(View.VISIBLE);
		}
		if(check_In_out.getVisibility()==View.VISIBLE){
			check_In_out.setVisibility(View.GONE);
		}if(spinner.getVisibility()==View.VISIBLE){
			spinner.setVisibility(View.GONE);
		}
		if(take_a_break.getVisibility()==View.VISIBLE){
			take_a_break.setVisibility(View.GONE);
		}
		if(resume_session.getVisibility()==View.VISIBLE){
			resume_session.setVisibility(View.GONE);
		}if(end_session.getVisibility()==View.VISIBLE){
			end_session.setVisibility(View.GONE);
		}
		if(end_session_1.getVisibility()==View.VISIBLE){
			end_session_1.setVisibility(View.GONE);
		}
		if(isServiceRunning("com.example.securitygard.services.LocationUpdate")){
			getActivity().stopService(new Intent(getActivity(),LocationUpdate.class));
		}
		Log.d(Admin.SESSION_STATUS,Admin.SESSION_END);
	}
	private void guardNotPunchIn(){
		getActivity().runOnUiThread(new Runnable() {			
			@Override
			public void run() {
				MySharedPreferences.setSharedPreferences(getActivity(), Admin.SESSION_STATUS, Admin.SESSION_READY_START);
				if(check_In_out.getVisibility()==View.GONE){
					check_In_out.setVisibility(View.VISIBLE);
				}
				if(deshboard_clock_layout.getVisibility()==View.VISIBLE){
					deshboard_clock_layout.setVisibility(View.GONE);
				}		
				if(resume_session.getVisibility()==View.VISIBLE){
					resume_session.setVisibility(View.GONE);
				}
				if(end_session_1.getVisibility()==View.VISIBLE){
					end_session_1.setVisibility(View.GONE);
				}					
				if(take_a_break.getVisibility()==View.VISIBLE){
					take_a_break.setVisibility(View.GONE);
				}
				if(end_session.getVisibility()==View.VISIBLE){
					end_session.setVisibility(View.GONE);
				}if(choose_client.getVisibility()==View.VISIBLE){
					choose_client.setVisibility(View.GONE);
				}if(spinner.getVisibility()==View.VISIBLE){
					spinner.setVisibility(View.GONE);
				}				
				Log.d(Admin.SESSION_STATUS,Admin.SESSION_READY_START);
				if(isServiceRunning("com.example.securitygard.services.LocationUpdate")){
					getActivity().stopService(new Intent(getActivity(),LocationUpdate.class));
				}
			}
		});		
	}
	public boolean isServiceRunning(String serviceClassName) {
		final ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
		final List<RunningServiceInfo> services = activityManager
				.getRunningServices(Integer.MAX_VALUE);
		for (RunningServiceInfo runningServiceInfo : services) {
			if (runningServiceInfo.service.getClassName().equals(
					serviceClassName)) {
				return true;
			}
		}
		return false;
	}
	private void updatePunchOut(final boolean status){
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(status){
					/*
					if(spinner2.getVisibility()==View.GONE){
						spinner2.setVisibility(View.VISIBLE);
					}
					*/
					
					if(end_session.getVisibility()==View.GONE){
						end_session.setVisibility(View.VISIBLE);
					}
					if(take_a_break.getVisibility()==View.GONE){
						take_a_break.setVisibility(View.VISIBLE);
					}
					if(spinner.getVisibility()==View.VISIBLE){
						spinner.setVisibility(View.GONE);
					}
					if(check_In_out.getVisibility()==View.VISIBLE){
						check_In_out.setVisibility(View.GONE);
					}			
				}else{
					/*
					if(spinner2.getVisibility()==View.VISIBLE){
						spinner2.setVisibility(View.GONE);
					}
					*/
				}
			}
		});		
	}
	public static Runnable clock =new Runnable(){		
		@Override
		public void run() {
			String time_old=settings.getString(Admin.TIME_OLD, "");			
			java.sql.Date date= new java.sql.Date(new java.util.Date().getTime());
			java.sql.Timestamp timestamp2=null;			
			String new_time_stamp=settings.getString(Admin.TIME_NEW, "");			
			if(!new_time_stamp.endsWith("0000-00-00 00:00:00") && !time_old.endsWith("00:00:00")){
				timestamp2= java.sql.Timestamp.valueOf(new_time_stamp);			
				updateTime( date.getTime(),timestamp2.getTime(),time_old);
				MyHandler.HANDLER.postDelayed(clock, 1000);
			}else if(!new_time_stamp.endsWith("0000-00-00 00:00:00")){		
				timestamp2= java.sql.Timestamp.valueOf(new_time_stamp);				
				updateTime( date.getTime(),timestamp2.getTime());
				MyHandler.HANDLER.postDelayed(clock, 1000);
			}else if(!time_old.endsWith("00:00:00")){
				updateTime(time_old);
				MyHandler.HANDLER.removeCallbacks(clock);
			}else{
				MyHandler.HANDLER.removeCallbacks(clock);
			}
		}
	};	
	
	private static void updateTime(Long first,Long secong){		
		long diff = first-secong;
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		//long diffDays = diff / (24 * 60 * 60 * 1000);				
								
		hour_minute.setText(
				String.format("%02d", diffHours )+":"+
				String.format("%02d",diffMinutes)
				);				
		seconds.setText(String.format("%02d", diffSeconds));	
	}
	
	private static void updateTime(String old_time){
		String hh=old_time.substring(0,old_time.indexOf(":"));
		String mm=old_time.substring(old_time.indexOf(":")+1,old_time.lastIndexOf(":"));
		String ss=old_time.substring(old_time.lastIndexOf(":")+1,old_time.length());
		long h=Long.parseLong(hh);
		long m=Long.parseLong(mm);
		long s=Long.parseLong(ss);		
		hour_minute.setText(
				String.format("%02d", h )+":"+
				String.format("%02d", m)
		);
		seconds.setText(String.format("%02d", s));	
	}
	private static void updateTime(Long first,Long secong,String old_time){
		String hh=old_time.substring(0,old_time.indexOf(":"));
		String mm=old_time.substring(old_time.indexOf(":")+1,old_time.lastIndexOf(":"));
		String ss=old_time.substring(old_time.lastIndexOf(":")+1,old_time.length());
		
		long diff = first-secong;	
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		//long diffDays = diff / (24 * 60 * 60 * 1000);				
		
		long h=Long.parseLong(hh);
		long m=Long.parseLong(mm);
		long s=Long.parseLong(ss);		
		
		long second_total=0;
		second_total+=h*3600;
		second_total+=m*60;
		second_total+=s;
		
		second_total+=diffHours*3600;
		second_total+=diffMinutes*60;
		second_total+=diffSeconds;
		
		long hours=second_total/3600;
		second_total=second_total%3600;
		long minutes=second_total/60;
		second_total=second_total%60;

		hour_minute.setText(
				String.format("%02d", hours )+":"+
				String.format("%02d", minutes)
				);				
		seconds.setText(String.format("%02d", second_total));	
	}
	private void checkInSuccessfully(){
		if (check_In_out.getVisibility() == View.VISIBLE)
			check_In_out.setVisibility(View.GONE);
		if(deshboard_clock_layout.getVisibility()==View.GONE){
			deshboard_clock_layout.setVisibility(View.VISIBLE);
		}if(end_session.getVisibility()==View.GONE){
			end_session.setVisibility(View.VISIBLE);
		}if(take_a_break.getVisibility()==View.GONE){
			take_a_break.setVisibility(View.VISIBLE);
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		locationManager.requestLocationUpdates(provider, 400, 1, this);
	}
	@Override
	public void onPause() {
		super.onPause();
	    locationManager.removeUpdates(this);
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==1){
			super.onResume();
			checkInStatus();
			checkUpdates();
		}
	}
	@Override
	public void onLocationChanged(Location location_1) {
		latitude=location_1.getLatitude();
		longitude=location_1.getLongitude();
		this.location=getCompleteAddressString(latitude, longitude);
	}
	@Override
	public void onProviderDisabled(String provider) {
	}	
	@Override
	public void onProviderEnabled(String provider) {
		onLocationChanged(locationManager.getLastKnownLocation(provider));
	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}	
	public void showSettingsAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
 
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            	getActivity().startActivityForResult(intent,1);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {            	
            	dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
	}	
	public void showNoInternetConncetAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");
 
        // Setting Dialog Message
        alertDialog.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");
 
        // On pressing Settings button
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	checkUpdates();
            }
        });
        // Showing Alert Message
        alertDialog.show();
	}
	public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());        
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
   }
	
	public class GetClient extends AsyncTask<String, Void, Integer>{
		@Override
		protected Integer doInBackground(String... args) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(Admin.COMPANY_ID, args[0]));			
			final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForGetAllClient(), "POST", params);
			try {
				int success = json.getInt(Admin.TAG_SUCCESS);
				if (success == 1) {
					parseResult(json);
					return 1;
				} else {
					return 0;
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.d("My Exception", e.toString());
			}	return 0;
		}
		@Override
		protected void onPostExecute(Integer result) {
			if(result==1){
				if(treeMap!=null){
					if(treeMap.size()>0){
				        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item);
				        adp.add("Choose Client");
				        Enumeration<String> enumeration=treeMap.elements();				        
				        while (enumeration.hasMoreElements()) {
							String string = (String) enumeration.nextElement();
							adp.add(string);
						}
				        spinner.setAdapter(adp);
					}
				}
			}
		}
	}
	private void parseResult(JSONObject json){
		try{
			treeMap = new Hashtable<String, String>();
			if(json.has(Admin.TAG_SUCCESS)){
				int success=json.getInt(Admin.TAG_SUCCESS);
				if(success==1){
					if(json.has(Admin.CLIENT)){
						JSONArray jsonArray = json.getJSONArray(Admin.CLIENT);						
						for(int i=0;i<jsonArray.length();i++){
							JSONObject jsonObject=jsonArray.getJSONObject(i);
							if(jsonObject.has(Admin.CLIENT_ID)&&jsonObject.has(Admin.CLIENT_NAME)){
								treeMap.put(jsonObject.getString(Admin.CLIENT_ID),jsonObject.getString(Admin.CLIENT_NAME));
							}
						}
					}
				}
			}
		}catch(Exception e){
			Log.d("Error", e.getMessage());
		}
	}
	private class GetTodaySchedule extends AsyncTask<String, String, JSONObject>{
		@Override
		protected JSONObject doInBackground(String... params) {
			List<NameValuePair> pairs=new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
			final JSONObject jsonObject = jsonParser.makeHttpRequest(Admin.getUrlForScheduleToday(),"POST",pairs);
			try{
				int success=0;
				JSONArray jsonArray=null;
				if(jsonObject.has(Admin.TAG_SUCCESS))
					success=jsonObject.getInt(Admin.TAG_SUCCESS);
				if(success==1){
					if(jsonObject.has(Admin.SCHEDULE))
						jsonArray =jsonObject.getJSONArray(Admin.SCHEDULE);
					if(jsonArray!=null){
						return jsonObject;
					}
				}
			}catch(JSONException e){
				Log.e("GetSchedule",e.toString());
			}
			return jsonObject;
		}
		@Override
		protected void onPostExecute(JSONObject object) {
			super.onPostExecute(object);
			try {
				if (object != null) {
					JSONArray jsonArray=null;
					int success = 0;
					if (object.has(Admin.TAG_SUCCESS))
						success = object.getInt(Admin.TAG_SUCCESS);
					if (success == 1) {
						if (object.has(Admin.SCHEDULE))
							jsonArray = object.getJSONArray(Admin.SCHEDULE);
						if (jsonArray != null) {
							for(int i=0;i<jsonArray.length();i++){
								JSONObject c = jsonArray.getJSONObject(i);
								int j=0;
								if(c.has(Admin.CLIENT_NAME)){
									MySharedPreferences.setSharedPreferences(getActivity(), Admin.CLIENT_NAME, c.getString(Admin.CLIENT_NAME));
									i++;
								}
								if(c.has(Admin.CLIENT_ID)) {
									MySharedPreferences.setSharedPreferences(getActivity(), Admin.CLIENT_ID, c.getString(Admin.CLIENT_ID));
									i++;
								}
								if(i==2){
									MySharedPreferences.setSharedPreferences(getActivity(), Admin.CLIENT_DATE, Admin.getDateTodayString());
								}
								break;
							}
						}
						init();
					}
				}else{
					init();
				}
			}catch (JSONException e){
				e.printStackTrace();
				Log.e("JSON",e.toString());
			}
		}
	}
}
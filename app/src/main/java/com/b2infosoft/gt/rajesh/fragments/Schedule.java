package com.b2infosoft.gt.rajesh.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.R;
import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.server.JSONParser;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Schedule extends Fragment {
    private SharedPreferences settings;
    public static final String TAG = Schedule.class.getSimpleName();
    private final JSONParser jsonParser = new JSONParser();
    private JSONArray jsonArray;
    private TableLayout tableLayout;
    private TextView textYear;
    private ImageButton button_year_next, button_year_previous;
    private TableLayout.LayoutParams layout_params_table_row;

    public static Schedule newInstance() {
        return new Schedule();
    }

    public static Schedule newInstance(String year, String month) {
        return new Schedule();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragments_outer_schedule_layout, container, false);
        getActivity().setTitle(MySharedPreferences.getSharedPreferences(getActivity(), Admin.CLIENT_NAME).toUpperCase(Locale.getDefault()));
      /*  view.findViewById(R.id.icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Second PAge Call Now",Toast.LENGTH_LONG).show();
            }
        });*/
        settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
        tableLayout = (TableLayout) view.findViewById(R.id.log_month_table_1);
        layout_params_table_row = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
            /*
            textYear=(TextView)view.findViewById(R.id.month_selected_1);
			textYear.setTypeface(Admin.getFontRoboto(getActivity()), Typeface.BOLD);		
			button_year_next=(ImageButton)view.findViewById(R.id.month_next_1);
		 	button_year_next.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					
					if(Arrays.asList(foundMonths).contains(String.valueOf(SELECT_MONTH))){						
						int index=Arrays.asList(foundMonths).indexOf(String.valueOf(SELECT_MONTH));							
						if(0<=index&&index<foundMonths.length){							
							
							new GetMonthLogs().execute(new String[]{		 			
									foundMonths[index+1],
						 			String.valueOf(SELECT_YEAR)		 					 			
						 	});							
						 	updateMonth(String.valueOf(SELECT_YEAR), foundMonths[index+1]);						 	
						}						
					}
					
				}
			});
		 	button_year_previous=(ImageButton)view.findViewById(R.id.month_previous_1);
		 	button_year_previous.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
				
					if(Arrays.asList(foundMonths).contains(String.valueOf(SELECT_MONTH))){						
						int index=Arrays.asList(foundMonths).indexOf(String.valueOf(SELECT_MONTH));							
						if(0 < index && index<=foundMonths.length){							
							new GetMonthLogs().execute(new String[]{		 			
									foundMonths[index-1],
						 			String.valueOf(SELECT_YEAR)		 					 			
						 	});
						 	updateMonth(String.valueOf(SELECT_YEAR), foundMonths[index-1]);							
						}
					}	
			
				}
			});
		 	*/
             /*
		 	new GetMonthLogs().execute(new String[]{
		 			
		 			String.valueOf(SELECT_MONTH),
		 			String.valueOf(SELECT_YEAR)		 					 			
		 	});
		 	*//*
		 		updateMonth(String.valueOf(SELECT_YEAR), String.valueOf(SELECT_MONTH));
		 	*/
        new GetSchedule().execute();
        return view;
    }

    private void updateLog(JSONArray jsonArray) {
        if (tableLayout.getChildCount() > 0) {
            tableLayout.removeAllViews();
        }
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject c = jsonArray.getJSONObject(i);
                TableRow tr_row = new TableRow(getActivity());
                tr_row.setLayoutParams(layout_params_table_row);
                tr_row.setPadding(0, 10, 0, 10);
                if ((i % 2) == 0)
                    tr_row.setBackgroundResource(R.color.bg_log_row_odd);
                else
                    tr_row.setBackgroundResource(R.color.bg_log_row_even);
                TextView text = new TextView(getActivity());
                text.setText(c.getString(Admin.DATE) + "\n" + c.getString(Admin.START) + " - " + c.getString(Admin.END));
                text.setGravity(Gravity.CENTER_HORIZONTAL);
                text.setTypeface(Admin.getFontRoboto(getActivity()));

                TextView text1 = new TextView(getActivity());
                text1.setText(c.getString(Admin.NAME));
                text1.setGravity(Gravity.CENTER_HORIZONTAL);
                text1.setTypeface(Admin.getFontRoboto(getActivity()));
				/*
				TextView text2=new TextView(getActivity());														
				text2.setText(c.getString(Admin.START));		
				text2.setGravity(Gravity.CENTER_HORIZONTAL);
				text2.setTypeface(Admin.getFontRoboto(getActivity()));																
				TextView text3=new TextView(getActivity());														
				text3.setText(c.getString(Admin.END));		
				text3.setGravity(Gravity.CENTER_HORIZONTAL);
				text3.setTypeface(Admin.getFontRoboto(getActivity()));																
				TextView text4=new TextView(getActivity());														
				text4.setText(c.getString(Admin.CONTACT));		
				text4.setGravity(Gravity.CENTER_HORIZONTAL);
				text4.setTypeface(Admin.getFontRoboto(getActivity()));																
				TextView text5=new TextView(getActivity());														
				text5.setText(c.getString(Admin.ADDRESS));		
				text5.setGravity(Gravity.CENTER_HORIZONTAL);
				text5.setTypeface(Admin.getFontRoboto(getActivity()));																				
				*/
                tr_row.addView(text);
                tr_row.addView(text1);
                final TableRow address = new TableRow(getActivity());
                address.setLayoutParams(layout_params_table_row);
                address.setPadding(0, 10, 0, 10);
                address.setBackgroundResource(R.color.bg_info);
                /*
                if ((i % 2) == 0)
                    address.setBackgroundResource(R.color.bg_log_row_odd);
                else
                    address.setBackgroundResource(R.color.bg_log_row_even);
                */
                TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT,1);
                layoutParams.span = 2;
                TextView addr = new TextView(getActivity());
                addr.setText(c.getString(Admin.ADDRESS)+"\n"+c.getString(Admin.CONTACT));
                addr.setGravity(Gravity.CENTER_HORIZONTAL);
                addr.setTypeface(Admin.getFontRoboto(getActivity()));
                //TableRow.LayoutParams params = (TableRow.LayoutParams) addr.getLayoutParams();
                //params.span = 2;
                addr.setLayoutParams(layoutParams);
                address.addView(addr);
                address.setVisibility(View.GONE);
                tr_row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        address.setVisibility(address.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    }
                });
                address.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        address.setVisibility(address.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    }
                });
				/*
				tr_row.addView(text2);
				tr_row.addView(text3);				
				tr_row.addView(text4);
				tr_row.addView(text5);				
				*/
                tableLayout.addView(tr_row);
                tableLayout.addView(address);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Exception", e.toString());
            }
        }
    }

    private class GetSchedule extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
            final JSONObject jsonObject = jsonParser.makeHttpRequest(Admin.getUrlForSchedule(), "POST", pairs);
            try {
                int success = 0;
                JSONArray jsonArray = null;
                if (jsonObject.has(Admin.TAG_SUCCESS))
                    success = jsonObject.getInt(Admin.TAG_SUCCESS);
                if (success == 1) {
                    if (jsonObject.has(Admin.SCHEDULE))
                        jsonArray = jsonObject.getJSONArray(Admin.SCHEDULE);
                    if (jsonArray != null) {
                        Schedule.this.jsonArray = jsonArray;
                    }
                }
            } catch (JSONException e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (Schedule.this.jsonArray != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateLog(Schedule.this.jsonArray);
                    }
                });
            }
        }
    }


}
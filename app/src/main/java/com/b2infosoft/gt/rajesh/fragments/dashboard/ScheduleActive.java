package com.b2infosoft.gt.rajesh.fragments.dashboard;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.R;
import com.b2infosoft.gt.SlideMenu;
import com.b2infosoft.gt.server.JSONParser;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ScheduleActive
        extends Fragment {
    public static final String TAG = ScheduleActive.class.getSimpleName();
    private static SharedPreferences settings;
    private String client_id;
    private TextView client_name;
    JSONParser jsonParser = new JSONParser();
    private View mView;
    private ProgressBar progressBar;
    private Button start, choose_client;

    private void navigate(final Fragment paramFragment, final String paramString) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                SlideMenu.setActiveFragment(ScheduleActive.this.getFragmentManager(), paramFragment, paramString);
            }
        });
    }

    public static ScheduleActive newInstance() {
        return new ScheduleActive();
    }

    private void showMessage(String paramString) {
        Toast.makeText(getActivity(), paramString, Toast.LENGTH_SHORT).show();
    }

    private boolean isClientName(String str) {
        if(str==null)
            return false;

        if (str.length() > 0) {
            return true;
        }
        return false;
    }

    private boolean isClientId(String str) {
        if(str==null)
            return false;

        if (str.length() > 0) {
            try {
                int i = Integer.parseInt(str);
                if(i!=0){
                    return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle paramBundle) {
        settings = getActivity().getSharedPreferences(Admin.MYSECRATE, 0);
        View view = inflater.inflate(R.layout.fragment_dashboard_new_schedule_layout, container, false);
        this.progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        this.mView = view.findViewById(R.id.schedule_view);
        this.start = (Button) view.findViewById(R.id.schedule_start);
        this.start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                if(isClientId(client_id) && isClientName(client_name.getText().toString())) {
                    new ScheduleActive.StartSchedule().execute(new String[0]);
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Alert");
                    builder.setMessage("Something went wrong. Please Choose Client");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.create().show();
                }
            }
        });
        this.choose_client = (Button) view.findViewById(R.id.schedule_change);
        this.choose_client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleActive.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
            }
        });
        this.client_name = (TextView) view.findViewById(R.id.schedule_client_name);
        new GetTodaySchedule().execute(new String[0]);
        return view;
    }

    private class EmpStatus extends AsyncTask<String, String, JSONObject> {
        private EmpStatus() {
        }

        protected JSONObject doInBackground(String... paramVarArgs) {
            List<NameValuePair> pairs = new ArrayList();
            pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
            return ScheduleActive.this.jsonParser.makeHttpRequest(Admin.getUrlForEmpStatus(), "POST", pairs);
        }

        protected void onPostExecute(JSONObject object) {
            ScheduleActive.this.progressBar.setVisibility(View.GONE);
            ScheduleActive.this.mView.setVisibility(View.VISIBLE);

            int schedule = 0;
            int punch = 0;
            int client = 0;
            if (object != null) {
                try {
                    if (object.has(Admin.TAG_SUCCESS)) {
                        int success = object.getInt(Admin.TAG_SUCCESS);
                    }
                    if (object.has(Admin.SCHEDULE_STATUS)) {
                        schedule = object.getInt(Admin.SCHEDULE_STATUS);
                    }
                    if (object.has(Admin.CLIENT_STATUS)) {
                        client = object.getInt(Admin.CLIENT_STATUS);
                    }
                    if (object.has(Admin.PUNCH_STATUS)) {
                        punch = object.getInt(Admin.PUNCH_STATUS);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON", e.toString());
                }
            }
        /*
            System.out.println("SCHEDULE  "+TAG+schedule);
            System.out.println("PUNCH "+TAG+punch);
            System.out.println("CLIENT "+TAG+client);
        */
            if (schedule == 1 && client == 0) {
                //ScheduleActive.this.navigate(ScheduleActive.newInstance(), ScheduleActive.TAG);
            } else if (schedule != 0 || client != 0) {
                switch (punch) {
                    case 0:
                        ScheduleActive.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                        break;
                    case 1:
                        ScheduleActive.this.navigate(SessionStart.newInstance(), SessionStart.TAG);
                        break;
                    case 2:
                        ScheduleActive.this.navigate(SessionBreak.newInstance(), SessionBreak.TAG);
                        break;
                    case 3:
                        if (client != 1) {
                            ScheduleActive.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
                            break;
                        } else {
                            ScheduleActive.this.navigate(PunchIn.newInstance(), PunchIn.TAG);
                            break;
                        }
                    default:
                        break;
                }
            } else {
                ScheduleActive.this.navigate(ChooseClient.newInstance(), ChooseClient.TAG);
            }
            if (punch == 1) {
                MySharedPreferences.setSharedPreferences(ScheduleActive.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_START);
            } else {
                MySharedPreferences.setSharedPreferences(ScheduleActive.this.getActivity().getApplicationContext(), Admin.SESSION_STATUS, Admin.SESSION_END);
            }

        }

        protected void onPreExecute() {
            ScheduleActive.this.progressBar.setVisibility(View.VISIBLE);
            ScheduleActive.this.mView.setVisibility(View.GONE);
        }
    }

    private class GetTodaySchedule
            extends AsyncTask<String, String, JSONObject> {
        private GetTodaySchedule() {
        }

        protected JSONObject doInBackground(String... paramVarArgs) {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(new BasicNameValuePair("user_id", settings.getString("user_id", "")));
            JSONObject localJSONObject = ScheduleActive.this.jsonParser.makeHttpRequest(Admin.getUrlForScheduleToday(), "POST", localArrayList);
            try {
                boolean bool1 = localJSONObject.has("success");
                int i = 0;
                if (bool1) {
                    i = localJSONObject.getInt("success");
                }
                if (i == 1) {
                    boolean bool2 = localJSONObject.has("schedule");
                    Object localObject = null;
                    if (bool2) {
                        JSONArray localJSONArray = localJSONObject.getJSONArray("schedule");
                        localObject = localJSONArray;
                    }
                    if (localObject == null) {
                    }
                }
                return localJSONObject;
            } catch (JSONException localJSONException) {
                Log.e("GetSchedule", localJSONException.toString());
            }
            return localJSONObject;
        }

        protected void onPostExecute(JSONObject paramJSONObject) {
            ScheduleActive.this.progressBar.setVisibility(View.GONE);
            ScheduleActive.this.mView.setVisibility(View.VISIBLE);
            if (paramJSONObject != null) {
            }
            try {
                boolean bool1 = paramJSONObject.has("success");
                int i = 0;
                if (bool1) {
                    i = paramJSONObject.getInt("success");
                }
                if (i == 1) {
                    boolean bool2 = paramJSONObject.has("schedule");
                    JSONArray localJSONArray = null;
                    if (bool2) {
                        localJSONArray = paramJSONObject.getJSONArray("schedule");
                    }
                    if ((localJSONArray != null) && (localJSONArray.length() < 0)) {
                        JSONObject localJSONObject = localJSONArray.getJSONObject(0);
                        boolean bool3 = localJSONObject.has("client_name");
                        int j = 0;
                        if (bool3) {
                            j++;
                            ScheduleActive.this.client_name.setText(localJSONObject.getString("client_name"));
                        }
                        if (localJSONObject.has("client_id")) {
                            j++;
                            ScheduleActive.this.client_id = localJSONObject.getString("client_id");
                        }
                        if (j != 2) {

                        }
                    }
                }
                return;
            } catch (JSONException localJSONException) {
                localJSONException.printStackTrace();
                Log.e("JSON", localJSONException.toString());
            }
        }

        protected void onPreExecute() {
            ScheduleActive.this.progressBar.setVisibility(View.VISIBLE);
            ScheduleActive.this.mView.setVisibility(View.GONE);
        }
    }

    private class StartSchedule extends AsyncTask<String, String, JSONObject> {
        private StartSchedule() {

        }

        protected JSONObject doInBackground(String... paramVarArgs) {
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(new BasicNameValuePair("user_id", settings.getString("user_id", "")));
            localArrayList.add(new BasicNameValuePair("company_id", settings.getString("company_id", "")));
            localArrayList.add(new BasicNameValuePair("client_id", ScheduleActive.this.client_id));
            return ScheduleActive.this.jsonParser.makeHttpRequest(Admin.getUrlForChooseClient(), "POST", localArrayList);
        }

        protected void onPostExecute(JSONObject paramJSONObject) {
            ScheduleActive.this.progressBar.setVisibility(View.GONE);
            ScheduleActive.this.mView.setVisibility(View.VISIBLE);
            String str1 = "";
            String str2 = "";
            try {
                boolean bool = paramJSONObject.has("success");
                int i = 0;
                if (bool) {
                    i = paramJSONObject.getInt("success");
                }
                if (i == 1) {
                    if (paramJSONObject.has("client_id")) {
                        str1 = paramJSONObject.getString("client_id");
                    }
                    if (paramJSONObject.has("client_name")) {
                        str2 = paramJSONObject.getString("client_name");
                    }
                    MySharedPreferences.setSharedPreferences(ScheduleActive.this.getActivity(), "client_name", str2);
                    MySharedPreferences.setSharedPreferences(ScheduleActive.this.getActivity(), "client_id", str1);
                    new ScheduleActive.EmpStatus().execute(new String[0]);
                }
                return;
            } catch (JSONException localJSONException) {
                Log.e("GetSchedule", localJSONException.toString());
            }
        }

        protected void onPreExecute() {
            ScheduleActive.this.progressBar.setVisibility(View.VISIBLE);
            ScheduleActive.this.mView.setVisibility(View.GONE);
        }
    }
}
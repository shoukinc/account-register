package com.b2infosoft.gt;
import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
@SuppressLint("ClickableViewAccessibility") 
public class UploadSign extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		/*
		SignetureView signetureView = new SignetureView(this);
		setContentView(signetureView);
		signetureView.requestFocus();
		*/
		/*
		CreateSurfaceView createSurfaceView=new CreateSurfaceView(this);
		setContentView(createSurfaceView);
		createSurfaceView.requestFocus();
		*/
	}	
	public class SignetureView extends View implements OnTouchListener {
		private final String TAG = SignetureView.class.getName();
		List<Point> points = new ArrayList<Point>();
		Paint paint = new Paint();		
		public SignetureView(Context context) {
			super(context);			
			setFocusable(true);
			setFocusableInTouchMode(true);
			this.setOnTouchListener(this);
			paint.setColor(Color.RED);
			paint.setAntiAlias(true);
		}		
		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			for(Point point:points){
//				canvas.drawCircle(point.x,point.y, 10, paint);
				canvas.drawPoint(point.x,point.y, paint);
			}
		}
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			Point point=new Point();
			point.x =	event.getX();
			point.y =	event.getY();
			points.add(point);
			invalidate();
			Log.d(TAG, "Point : "+point);
			return true;
		}
	}
	private class Point{
		float x,y;
		@Override
		public String toString() {
			return  x +" : "+y;
		}		
	}
}

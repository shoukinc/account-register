package com.b2infosoft.gt;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.gps.Network;
import com.b2infosoft.gt.image.FileCache;
import com.b2infosoft.gt.rajesh.sign_2.CaptureSignature;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.validator.Validator;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.ImageView;



public class MainActivity extends Activity {
    private ProgressDialog progressDialog;
    JSONParser jsonParser = new JSONParser();
    EditText email, password, company;
    Button login;
    FileCache cache;
    private static MainActivity mainActivity;

    public static MainActivity getInstance() {

        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cache = new FileCache(this);

        boolean suu = Admin.LOGIN.equals(MySharedPreferences.getSharedPreferences(getApplicationContext(), Admin.USER));

        ErrorReporter errReporter = new ErrorReporter();
        errReporter.Init(this);
        errReporter.CheckErrorAndSendMail(this);

        String title = getIntent().getStringExtra("title");

        if (!TextUtils.isEmpty(title)) {


            String notification_id = getIntent().getStringExtra("notification_id");
            String message = getIntent().getStringExtra("message");
            String image = getIntent().getStringExtra("image");

            if (suu) {
                Intent intent = new Intent(MainActivity.this, SlideMenu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("title", title);
                intent.putExtra("notification_id", notification_id);
                startActivity(intent);
                finish();
                return;
            } else {

            }
            //}
        }

        setContentView(R.layout.activity_main);


        mainActivity = this;

        ImageView company_logo = findViewById(R.id.company_logo);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int WIDTH = metrics.widthPixels;
        int HEIGHT = metrics.heightPixels;

        if (WIDTH <= 320 && HEIGHT <= 480) {
            company_logo.setImageResource(R.drawable.company_logo_320_480);
        } else if ((WIDTH > 320 && WIDTH <= 480) && (HEIGHT > 480 && HEIGHT <= 800)) {
            company_logo.setImageResource(R.drawable.company_logo_480_800);
        } else if ((WIDTH > 480 && WIDTH <= 600)
                && (HEIGHT > 800 && HEIGHT <= 1024)) {
            company_logo.setImageResource(R.drawable.company_logo_600_1024);
        } else if ((WIDTH > 600 && WIDTH <= 800)
                && (HEIGHT > 1024 && HEIGHT <= 1280)) {
            company_logo.setImageResource(R.drawable.company_logo_800_1280);
        } else if ((WIDTH > 800 && WIDTH <= 1080)
                && (HEIGHT > 1280 && HEIGHT <= 1920)) {
            company_logo.setImageResource(R.drawable.company_logo_1080_1920);
        } else {
            company_logo.setImageResource(R.drawable.company_logo_800_1280);
        }


        if (suu) {
            loginSuccess();
        } else {
            final Validator validator = new Validator(this);
            company = (EditText) findViewById(R.id.userCompany);
            company.setTypeface(Admin.getFontRoboto(getBaseContext()));
            email = (EditText) findViewById(R.id.userId);
            email.setTypeface(Admin.getFontRoboto(getBaseContext()));
            email.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    email.setError(null);
                    company.setText(null);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (new Network(MainActivity.this).isConnectingToInternet()) {
                        //new CompanyName().execute(new String[]{email.getText().toString().trim()});
                    }
                }
            });
            email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        //Toast.makeText(MainActivity.this,"FOCUS ON",Toast.LENGTH_SHORT).show();
                    } else {
                        //Toast.makeText(MainActivity.this,"FOCUS LOST",Toast.LENGTH_SHORT).show();
                        new CompanyName().execute(new String[]{email.getText().toString().trim()});
                    }
                }
            });
            password = (EditText) findViewById(R.id.userPass);
            password.setTypeface(Admin.getFontRoboto(getBaseContext()));
            password.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    password.setError(null);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            login = (Button) findViewById(R.id.login);
            login.setTypeface(Admin.getFontRoboto(getBaseContext()));
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String user_id = email.getText().toString();
                    user_id = user_id.trim();

                    String user_pass = password.getText().toString();
                    user_pass = user_pass.trim();
                    if (user_id.length() == 0) {
                        email.setError("Invalid User Name");
                    } else if (user_pass.length() == 0) {
                        password.setError("Invalid Password");
                    } else if (user_id.length() > 0 && user_pass.length() > 0) {
                        userLogin();
                    }

					/*
					if(!validator.emailCheckEmail(user_id)){						
						email.setError("Invaild User Name");
					}if(!validator.passwordChack(user_pass)){
						password.setError("Invalid Password");
					}if(validator.emailCheckEmail(user_id)&&validator.passwordChack(user_pass)){
						userLogin();					
					}
					*/

                }
            });
        }
    }

    private void userLogin() {
        if (!new Network(this).isConnectingToInternet()) {
            showNoInternetConncetAlert();
        } else {
            String params[] = {email.getText().toString().trim(), password.getText().toString().trim()};
            new UserLogin().execute(params);
        }
    }

    private class CompanyName extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", args[0]));
            final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForUserCompany(),
                    "POST", params);
            try {
                if (json.has(Admin.TAG_SUCCESS)) {
                    int success = json.getInt(Admin.TAG_SUCCESS);
                    if (success == 1) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (json.has(Admin.COMPANY_NAME)) {
                                    try {
                                        company.setText(json.getString(Admin.COMPANY_NAME));
                                        MySharedPreferences.getInstance(getApplicationContext()).saveCompanyName(json.getString(Admin.COMPANY_NAME));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                company.setText("");
                            }
                        });
                    }
                }
            } catch (Exception e) {

            }
            return null;
        }
    }

    private boolean str = false;

    private class UserLogin extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            // Building Parameters

          String device_token= MySharedPreferences.getInstance(getApplicationContext()).getDeviceToken();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("USER_ID", args[0]));
            params.add(new BasicNameValuePair("PASSWORD", args[1]));
            params.add(new BasicNameValuePair("device_token",device_token));

            // getting JSON Object
            // Note that create product url accepts POST method

            JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForUserRegistration(),
                    "GET", params);

            Log.d("JSON DATA : ", json.toString());

            // check log cat from response

            // check for success tag


            try {
                int success = json.getInt(Admin.TAG_SUCCESS);
                Log.d("success value", success + "");
                if (success == 1) {
                    // successfully created product
                    // closing this screen

                    JSONArray jsonArray = json.getJSONArray(Admin.USER_INFO);

                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String user_id = "";
                    String emp_first_name = "";
                    String emp_last_name = "";
                    String emp_profile_pic = "";
                    String company_id = "";
                    String client_id = "";
                    if (jsonObject.has(Admin.COMPANY_ID)) {
                        company_id = jsonObject.getString(Admin.COMPANY_ID);
                    }
                    if (jsonObject.has(Admin.USER_ID)) {
                        user_id = jsonObject.getString(Admin.USER_ID);
                    }
                    if (jsonObject.has(Admin.EMP_FIRST_NAME)) {
                        emp_first_name = jsonObject.getString(Admin.EMP_FIRST_NAME);
                    }
                    if (jsonObject.has(Admin.EMP_LAST_NAME)) {
                        emp_last_name = jsonObject.getString(Admin.EMP_LAST_NAME);
                    }
                    if (jsonObject.has(Admin.CLIENT_ID)) {
                        client_id = jsonObject.getString(Admin.CLIENT_ID);
                    }
                    if (jsonObject.has(Admin.EMP_PROFILE_PIC)) {
                        emp_profile_pic = jsonObject.getString(Admin.EMP_PROFILE_PIC);
                    }
					/*
					SharedPreferences settings=getSharedPreferences(Admin.MYSECRATE, 0);
					settings.edit()
					.putString(Admin.COMPANY_ID, company_id)
					.putString(Admin.USER_ID, user_id)
					.putString(Admin.CLIENT_ID, client_id)
					.putString(Admin.USER_NAME, emp_first_name+" "+emp_last_name)
					.putString(Admin.USER, Admin.LOGIN)
					.putString(Admin.EMP_PROFILE_PIC, emp_profile_pic)
					.commit();
					*/

                    MySharedPreferences.setSharedPreferences(getApplicationContext(), Admin.COMPANY_ID, company_id);
                    MySharedPreferences.setSharedPreferences(getApplicationContext(), Admin.USER_ID, user_id);
                    MySharedPreferences.setSharedPreferences(getApplicationContext(), Admin.CLIENT_ID, client_id);
                    MySharedPreferences.setSharedPreferences(getApplicationContext(), Admin.USER_NAME, emp_first_name + " " + emp_last_name);
                    MySharedPreferences.setSharedPreferences(getApplicationContext(), Admin.USER, Admin.LOGIN);
                    MySharedPreferences.setSharedPreferences(getApplicationContext(), Admin.EMP_PROFILE_PIC, emp_profile_pic);

                    cache.clear();
//					Log.d("CLIENT ID ", client_id);
                    loginSuccess();
                    str = true;
                } else {
                    // failed to create product
                    Log.d("Status value", "Not Find");
                    str = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("My Exception", e.toString());
            }
            return null;
        }

        protected void onPostExecute(String unused) {
            progressDialog.dismiss();
            if (str) {
                //Toast.makeText(getApplicationContext(), "Successfully Login", Toast.LENGTH_SHORT).show();
            } else {
                showUserErrorAlert();
            }
        }
    }

    private void loginSuccess() {
        //Intent intent=new Intent(MainActivity.this,SlideMenu.class);
        Intent intent = new Intent(MainActivity.this, SlideMenu.class);
        startActivity(intent);
        finish();
    }

    public void showUserErrorAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Incorrect Username/Password");

        // Setting Dialog Message
        alertDialog.setMessage("The username / password you entered is incorrect. Please try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public void showNoInternetConncetAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("No Internet Connection");

        // Setting Dialog Message
        alertDialog.setMessage("Sorry, no Internet connectivity detected. Please reconnect and try again.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                userLogin();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
}

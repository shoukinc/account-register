package com.b2infosoft.gt.validator;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class Validator {
	final static String emailPattern="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
	Context context;	
	boolean status=false;
	public Validator(Context context){
		this.context=context;
	}	
	
	public boolean emailCheckEditText(final EditText email){		
		
		email.addTextChangedListener(new TextWatcher() {			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {			
			}			
			@Override
			public void afterTextChanged(Editable s) {
				if((email.getText().toString().trim()).matches(emailPattern)&&s.length()>0){
					//Toast.makeText(context.getApplicationContext(), "Valid Email", Toast.LENGTH_SHORT).show();
					status=true;
				}else{
					//Toast.makeText(context.getApplicationContext(), "Invalid Email", Toast.LENGTH_SHORT).show();					
					status=false;
				}
			}
		});				
		return status;
	}
	public boolean emailCheckEmail(final String email){		
		return email.matches(emailPattern)&&email.length()>0;
	}
	public boolean passwordChack(final String password){
		return password.length()>7;		
	}
}

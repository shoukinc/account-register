package com.b2infosoft.gt.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.gps.GPSTracker;
import com.b2infosoft.gt.server.JSONParser;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class LocationUpdate extends Service {
	JSONParser jsonParser = new JSONParser();
	private GPSTracker gpsTracker;
	private Context context;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		context = getApplication().getBaseContext();
		gpsTracker = new GPSTracker(context);		
		handler.postDelayed(runnable, 0);
		return START_STICKY;
	}	
	@Override
	public void onDestroy() {
		super.onDestroy();
		handler.removeCallbacks(runnable);
	}
	final Handler handler = new Handler();
	final private Runnable runnable = new Runnable() {
		@Override
		public void run() {
			Location location = gpsTracker.getLocation();
			String latitude = "0.0";
			String longitude = "0.0";
			if (location != null) {
				latitude = String.valueOf(location.getLatitude());
				longitude = String.valueOf(location.getLongitude());
			}			
			if (!MySharedPreferences.getSharedPreferences(
					context, Admin.LAST_LATITUDE)
					.equalsIgnoreCase(latitude)
					|| !MySharedPreferences.getSharedPreferences(
							context, Admin.LAST_LONGITUDE)
							.equalsIgnoreCase(longitude)) {
				new SendLocation(MySharedPreferences.getSharedPreferences(context, Admin.USER_ID),MySharedPreferences.getSharedPreferences(context, Admin.CLIENT_ID), latitude, longitude).execute();
				MySharedPreferences.setSharedPreferences(
						getApplicationContext(), Admin.LAST_LATITUDE, latitude);
				MySharedPreferences.setSharedPreferences(
						getApplicationContext(), Admin.LAST_LONGITUDE,
						longitude);
			}
			Log.d(Admin.SESSION_STATUS,Admin.SESSION_START);
			handler.postDelayed(runnable, 10000);
		}
	};	
	class SendLocation extends AsyncTask<String, Void, Integer> {

		private String client_id;
		private String user_id;
		private String latitude;
		private String longitude;

		public SendLocation(String user_id, String client_id, String latitude,
				String longitude) {
			this.client_id = client_id;
			this.user_id = user_id;
			this.latitude = latitude;
			this.longitude = longitude;
		}

		@Override
		protected Integer doInBackground(String... params) {
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair(Admin.USER_ID, user_id));
			pairs.add(new BasicNameValuePair(Admin.CLIENT_ID, client_id));
			pairs.add(new BasicNameValuePair(Admin.LATITUDE, latitude));
			pairs.add(new BasicNameValuePair(Admin.LONGITUDE, longitude));

			// TODO: here automatic error application are  unfortunately stop Guard tab
			// java.lang.ExceptionInInitializerError name
			//	final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForLocationUpdate(), "POST", pairs);
			try {
				final JSONObject json = jsonParser.makeHttpRequest(Admin.getUrlForLocationUpdate(), "POST", pairs);
				int success = json.getInt(Admin.TAG_SUCCESS);
				if (success == 1) {
					return 1;
				} else {
					return 0;
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("My Exception", e.toString());
			}
			return 0;
		}
	}
}

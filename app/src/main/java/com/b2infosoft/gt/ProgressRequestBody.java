package com.b2infosoft.gt;

import android.widget.ProgressBar;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;

import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

/**
 * Created by root on 6/7/16.
 */
public class ProgressRequestBody extends RequestBody {

    protected RequestBody mDelegate;
    protected Listener mListener;
    protected CountingSink mCountingSink;
    protected ProgressBar progressBar=null;
  static String timeupdatefirst="";
    static   int  prodreggint=0;
    public ProgressRequestBody(RequestBody delegate, Listener listener, ProgressBar progressBar, String timeupdate) {
        mDelegate = delegate;
        mListener = listener;
        this.progressBar=progressBar;
        this.timeupdatefirst=timeupdate;

    }



    @Override
    public MediaType contentType() {
        return mDelegate.contentType();
    }

    @Override
    public long contentLength() {
        try {
            return mDelegate.contentLength();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        mCountingSink = new CountingSink(sink);
        BufferedSink bufferedSink = Okio.buffer(mCountingSink);
        mDelegate.writeTo(bufferedSink);
        bufferedSink.flush();
    }

    protected final class CountingSink extends ForwardingSink {
        private long bytesWritten = 0;
        public CountingSink(Sink delegate) {
            super(delegate);
        }
        @Override
        public void write(Buffer source, long byteCount) throws IOException {
            super.write(source, byteCount);
            bytesWritten += byteCount;
            prodreggint=(int) (100F * bytesWritten / contentLength());

            mListener.onProgress((int) (100F * bytesWritten / contentLength()),progressBar,timeupdatefirst);


        }
    }




    public interface Listener {
        void onProgress(int progress, ProgressBar progressBar, String timeupdate);
    }
}
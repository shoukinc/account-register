package com.b2infosoft.gt;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * Created by Kartik Sharma @ B.R. Softech on 21/3/16.
 */
public class ApiConnection implements Callback {
    public static final String MEDIA_TYPE_PNG = "image/png";
    public static final String MEDIA_TYPE_JPG = "image/jpg";
    public static final String MEDIA_TYPE_JPEG = "image/jpeg";
    public static final String MEDIA_TYPE_UNKNOWN = "application/octet-stream";
    private static final String UNKNOWN_ERROR_MSG = "Unknown Error Response";
    private static final String TAG = "ApiConnection";
    private static final String DEBUG = "debug";
    private OkHttpClient mOkHttpClient;
    private Call mCall;
    private OnApiConnectListener mOnApiConnectListener;

    public ApiConnection(OnApiConnectListener onApiConnectListener) {
        this.mOnApiConnectListener = onApiConnectListener;
    }

    public void connect(Request request) {
        mOkHttpClient = new OkHttpClient();
        mOkHttpClient.setConnectTimeout(240, TimeUnit.SECONDS); // connect timeout
        mOkHttpClient.setReadTimeout(240, TimeUnit.SECONDS);    // socket timeout
        mOkHttpClient.setWriteTimeout(240, TimeUnit.SECONDS);    // socket timeout
        mCall = mOkHttpClient.newCall(request);
        mCall.enqueue(this);
    }


    @Override
    public void onFailure(Request request, final IOException e) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                String errorMsg = e.getMessage();
                Log.e(TAG, "" + errorMsg);
                mOnApiConnectListener.onFailure(errorMsg);
            }
        });

    }

    @Override
    public void onResponse(final Response response) throws IOException {
        final String jsonString = response.body().string();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (response.isSuccessful()) {
                    /*if (TextUtils.equals(BuildConfig.BUILD_TYPE, DEBUG)) {
                        Log.e(TAG, jsonString);
                    }*/
                    Log.e(TAG, jsonString);
                    mOnApiConnectListener.onSuccess(jsonString);
                } else {
                    String errorMsg = UNKNOWN_ERROR_MSG + "-" + response.message() + "\n" + jsonString;
                    Log.e(TAG, errorMsg);
                    mOnApiConnectListener.onFailure(errorMsg);
                }
            }
        });
    }
}

package com.b2infosoft.gt.custom;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by rajesh on 5/25/2016.
 */

public class DigitalClock extends TextView {
    private int hours;
    private int minutes;
    private int seconds;
    private Timer clockTimer;
    final Handler mHandler = new Handler();
    private boolean isSchedule=false;
    private final TimerTask clockTask = new TimerTask() {
        @Override
        public void run() {
            mHandler.post(mUpdateResults);
        }
    };
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            update();
            //mHandler.postDelayed(mUpdateResults,1000);
        }
    };

    public DigitalClock(Context context) {
        super(context);
        init();
    }
    private void update() {
        seconds++;
        if (seconds >= 60) {
            seconds = 0;
            if (minutes < 59) {
                minutes++;
            } else if (hours < 23) {
                minutes = 0;
                hours++;
            } else {
                minutes = 0;
                hours = 0;
            }
        }
        if (seconds % 2 == 0) {
            setText(String.format("%02d:%02d", hours, minutes));
        } else {
            setText(String.format("%02d %02d", hours, minutes));
        }
    }
    private void init() {
    /*
        Calendar mCalendar = Calendar.getInstance();
        hours = mCalendar.get(Calendar.HOUR_OF_DAY);
        minutes = mCalendar.get(Calendar.MINUTE);
        seconds = mCalendar.get(Calendar.SECOND);
    */
        hours = 0;
        minutes = 0;
        seconds = 0;
        //clockTimer.scheduleAtFixedRate(clockTask, 0, 1000);
    }
    public DigitalClock(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public DigitalClock(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public void setTime(int hours,int minutes,int seconds){
        this.hours = hours;
        this.minutes=minutes;
        this.seconds=seconds;
    }
    public void startClock(){
        //mHandler.postDelayed(mUpdateResults,1000);
        clockTimer = new Timer();
        //clockTimer.scheduleAtFixedRate(clockTask, 0, 1000);
        clockTimer.schedule(clockTask,0,1000);
    }
    public void stopClock(){
        if(clockTimer!=null) {
            clockTimer.cancel();
        }
    }
    public boolean isSchedule() {
        return isSchedule;
    }
    public void setIsSchedule(boolean isSchedule) {
        this.isSchedule = isSchedule;
    }
}

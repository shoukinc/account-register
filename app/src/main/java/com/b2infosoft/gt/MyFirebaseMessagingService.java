package com.b2infosoft.gt;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mahesh on 1/4/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                sendPushNotification(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    //this method will display the notification
    //We are passing the JSONObject that is received from
    //firebase cloud messaging
    private void sendPushNotification(JSONObject json) {
        //optionally we can display the json into log
        Log.e(TAG, "Notification JSON " + json.toString());
        try {
            //getting the json data
            JSONObject data = json.getJSONObject("data");

            //parsing json data
            String title = "";
            String message = "";
            String imageUrl = "";
            String notification_id = "";
            String icon = "";

            if (data != null) {
                if (data.has("title")) {
                    title = data.getString("title");
                }
                if (data.has("message")) {
                    message = data.getString("message");
                }
                if (data.has("image")) {
                    imageUrl = data.getString("image");
                }
                if (data.has("notification_id")) {
                    notification_id = data.getString("notification_id");
                }
                if (data.has("icon")) {
                    icon = data.getString("icon");
                }


                //creating MyNotificationManager object
                MyNotificationManager mNotificationManager = new MyNotificationManager(getApplicationContext());

                //creating an intent for the notification
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("notification_id", notification_id);
                intent.putExtra("message", message);
                intent.putExtra("image", imageUrl);
                intent.putExtra("icon", icon);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                //if there is no image
                if (imageUrl.equalsIgnoreCase("null")) {
                    //displaying small notification
                    mNotificationManager.showSmallNotification(title, message, intent, notification_id, icon);
                } else {
                    //if there is an image
                    //displaying a big notification
                    mNotificationManager.showBigNotification(title, message, imageUrl, intent, notification_id, icon);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

    }


}
package com.b2infosoft.gt;

import android.app.Application;

/**
 * Created by mahesh on 1/5/17.
 */

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ApplicationLifecycleHandler handler = new ApplicationLifecycleHandler();
        registerActivityLifecycleCallbacks(handler);
        registerComponentCallbacks(handler);
    }
}

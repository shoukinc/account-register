package com.b2infosoft.gt;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.image.ImageLoader;
import com.b2infosoft.gt.rajesh.adapter.NavItemAdapter;
import com.b2infosoft.gt.rajesh.adapter.RoundImage;
import com.b2infosoft.gt.rajesh.fragments.DailyLog;
import com.b2infosoft.gt.rajesh.fragments.DailyReport;
import com.b2infosoft.gt.rajesh.fragments.DailyReport_1;
import com.b2infosoft.gt.rajesh.fragments.DashBoard;
import com.b2infosoft.gt.rajesh.fragments.IncidentReport;
import com.b2infosoft.gt.rajesh.fragments.LogReport1;
import com.b2infosoft.gt.rajesh.fragments.NitificationFrg;
import com.b2infosoft.gt.rajesh.fragments.ParkingViolation;
import com.b2infosoft.gt.rajesh.fragments.Schedule;
import com.b2infosoft.gt.rajesh.fragments.dashboard.DashboardNew;
import com.b2infosoft.gt.rajesh.fragments.dashboard.SessionBreak;
import com.b2infosoft.gt.rajesh.fragments.fragment_list_option;
import com.b2infosoft.gt.rajesh.modals.NavItems;
import com.b2infosoft.gt.rajesh.modals.NotificationBean;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class SlideMenu extends ActionBarActivity {
	private static SharedPreferences settings;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private RelativeLayout relativeLayout;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mDrawerItmes;
	private List<NavItems> navItems;

	View view;
	TextView title;
	boolean drawer_status = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide_menu);
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();


        ErrorReporter errReporter = new ErrorReporter();
        errReporter.Init(this);
        errReporter.CheckErrorAndSendMail(this);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		int WIDTH = metrics.widthPixels;
		int HEIGHT = metrics.heightPixels;

		// actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar));
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		view = getLayoutInflater().inflate(R.layout.action_bar, null);
		android.support.v7.app.ActionBar.LayoutParams p = new android.support.v7.app.ActionBar.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		p.gravity = Gravity.CENTER;
		actionBar.setCustomView(view, p);
		actionBar.setDisplayShowCustomEnabled(true);
		title = (TextView) view.findViewById(R.id.menu_title);
		title.setTypeface(Admin.getFontRoboto(this), Typeface.NORMAL);
		ImageView imageButton = (ImageView) view.findViewById(R.id.menu_drawer);
		imageButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (drawer_status) {
					drawer_status = false;
					mDrawerLayout.closeDrawer(relativeLayout);
				} else {
					drawer_status = true;
					mDrawerLayout.openDrawer(relativeLayout);
				}
			}
		});

		Toolbar parent =(Toolbar) view.getParent();
		parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
		parent.setContentInsetsAbsolute(0,0);

		settings = getSharedPreferences(Admin.MYSECRATE, 0);
		mTitle = mDrawerTitle = getTitle();

		mDrawerItmes = getResources().getStringArray(R.array.drawer_titles_1);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		relativeLayout = (RelativeLayout) findViewById(R.id.drawer_pane);

		if (WIDTH <= 320 && HEIGHT <= 480) {
			relativeLayout.getLayoutParams().width = 110;
		} else if ((WIDTH > 320 && WIDTH <= 480)
				&& (HEIGHT > 480 && HEIGHT <= 800)) {
			relativeLayout.getLayoutParams().width = 160;
		} else if ((WIDTH > 480 && WIDTH <= 600)
				&& (HEIGHT > 800 && HEIGHT <= 1024)) {
			relativeLayout.getLayoutParams().width = 200;
		} else if ((WIDTH > 600 && WIDTH <= 800)
				&& (HEIGHT > 1024 && HEIGHT <= 1280)) {
			relativeLayout.getLayoutParams().width = 240;
		} else if ((WIDTH > 800 && WIDTH <= 1080)
				&& (HEIGHT > 1280 && HEIGHT <= 1920)) {
			relativeLayout.getLayoutParams().width = 350;
		} else {
			relativeLayout.getLayoutParams().width = (int) Math
					.round(WIDTH / 2.5);
		}
		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// Add items to the ListView
		navItems = new ArrayList<NavItems>();
		ImageLoader imageLoader = new ImageLoader(this);
		relativeLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.content_frame, DashboardNew.newInstance(),
								DashboardNew.TAG).commit();
				setTitle(DashboardNew.TAG.toUpperCase(Locale.getDefault()));
				mDrawerLayout.closeDrawer(relativeLayout);
			}
		});
		ImageView emp_profile_pic = (ImageView) findViewById(R.id.emp_profile_pic);
		if (settings.getString(Admin.EMP_PROFILE_PIC, "").length() > 0) {
			String url = Admin.EMPLOYEE_PROFILE_ADDRESS.concat(settings
					.getString(Admin.EMP_PROFILE_PIC, ""));
			imageLoader.DisplayImage(url, emp_profile_pic);
		} else {
			emp_profile_pic
					.setImageBitmap(RoundImage.getRoundedShape(RoundImage
							.decodeFile(this, R.drawable.ic_profile_pic), 200));
			// emp_profile_pic.setBackgroundResource(R.drawable.frame);
		}
		emp_profile_pic.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.content_frame, DashboardNew.newInstance(),
								DashboardNew.TAG).commit();
				setTitle(DashboardNew.TAG.toUpperCase(Locale.getDefault()));
				mDrawerLayout.closeDrawer(relativeLayout);
			}
		});
		TextView emp_profile_name = (TextView) findViewById(R.id.emp_profile_name);
		emp_profile_name.setTypeface(Admin.getFontRoboto(this));
		emp_profile_name.setTextSize(9);
		emp_profile_name.setText(settings.getString(Admin.USER_NAME, "")
				.toUpperCase(Locale.getDefault()));
		emp_profile_name.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.content_frame, DashboardNew.newInstance(),
								DashboardNew.TAG).commit();
				setTitle(DashboardNew.TAG.toUpperCase(Locale.getDefault()));
				mDrawerLayout.closeDrawer(relativeLayout);
			}
		});

		// navItems.add(new NavItems(settings.getString(Admin.USER_NAME,
		// "").toUpperCase(Locale.getDefault()),R.drawable.menu_user_profile));
		navItems.add(new NavItems(mDrawerItmes[0].toUpperCase(Locale
				.getDefault()), R.drawable.menu_timesheet));
		navItems.add(new NavItems(mDrawerItmes[1].toUpperCase(Locale
				.getDefault()), R.drawable.menu_schedule));
		navItems.add(new NavItems(mDrawerItmes[2].toUpperCase(Locale
				.getDefault()), R.drawable.menu_incident));
		/*navItems.add(new NavItems(mDrawerItmes[2].toUpperCase(Locale
				.getDefault()), R.drawable.menu_log));*/
		/*navItems.add(new NavItems(mDrawerItmes[3].toUpperCase(Locale
				.getDefault()), R.drawable.menu_parking));
		navItems.add(new NavItems(mDrawerItmes[4].toUpperCase(Locale
				.getDefault()), R.drawable.menu_incident));
		navItems.add(new NavItems(mDrawerItmes[5].toUpperCase(Locale
				.getDefault()), R.drawable.menu_daily_report));*/
		navItems.add(new NavItems(mDrawerItmes[3].toUpperCase(Locale
				.getDefault()), R.drawable.menu_logout));

		NavItemAdapter navItemAdapter = new NavItemAdapter(
				getApplicationContext(), R.layout.drawer_nav_menu_layout,
				navItems, metrics);
		mDrawerList.setAdapter(navItemAdapter);

		// mDrawerList.setAdapter(new ArrayAdapter<String>(this,
		// R.layout.drawer_list_item, mDrawerItmes));
		// Set the OnItemClickListener so something happens when a
		// user clicks on an item.

		mDrawerList.setItemChecked(0, true);
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// Enable ActionBar app icon to behave as action to toggle nav drawer
		// getActionBar().setDisplayHomeAsUpEnabled(true);
		// getActionBar().setHomeButtonEnabled(true);
		// getActionBar().setDisplayShowTitleEnabled(false);
		// getActionBar().setDisplayUseLogoEnabled(false);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_menu_white_24dp, R.string.drawer_open, R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
			}
			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// Set the default content area to item 0
		// when the app opens for the first time

		String message = getIntent().getStringExtra("title");

		if (!TextUtils.isEmpty(message)) {
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.content_frame, new NitificationFrg(),
							NitificationFrg.TAG).commit();
			setTitle("Notifications".toUpperCase(Locale.getDefault()));
			mDrawerLayout.closeDrawer(relativeLayout);
		}else {

			if (savedInstanceState == null) {
				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.content_frame, DashboardNew.newInstance(),
								DashboardNew.TAG).commit();
				setTitle(DashboardNew.TAG.toUpperCase(Locale.getDefault()));
				mDrawerLayout.closeDrawer(relativeLayout);
				// navigateTo(0);
			}
		}





	}

	/*
	 * If you do not have any menus, you still need this function in order to
	 * open or close the NavigationDrawer when the user clicking the ActionBar
	 * app icon.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private class DrawerItemClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			navigateTo(position);
		}
	}

	private void navigateTo(int position) {
				getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		fragment_list_option.FORM_TYPE=0;
		switch (position) {

		case 0:
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.content_frame, LogReport1.newInstance(),
							LogReport1.TAG).commit();
			break;
		case 1:
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.content_frame, Schedule.newInstance(),
							Schedule.TAG).commit();
			break;case 2:
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.content_frame, new NitificationFrg(),
							NitificationFrg.TAG).commit();
			break;
		case 6:
			if (MySharedPreferences.getSharedPreferences(getApplicationContext(), Admin.SESSION_STATUS).equalsIgnoreCase(Admin.SESSION_START)) {
				//getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, DailyReport.newInstance(), DailyReport.TAG).commit();
				getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, DailyReport_1.newInstance(), DailyReport_1.TAG).commit();
			}else{
				getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.content_frame, DashboardNew.newInstance(),
						DashboardNew.TAG).commit();
			}
			break;
		/*case 3:
			if (MySharedPreferences.getSharedPreferences(
					getApplicationContext(), Admin.SESSION_STATUS)
					.equalsIgnoreCase(Admin.SESSION_START)) {
				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.content_frame,
								ParkingViolation.newInstance(),
								ParkingViolation.TAG).commit();
			}else{
				getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.content_frame, DashboardNew.newInstance(),
						DashboardNew.TAG).commit();
			}
			break;*/
		case 4:
			if (MySharedPreferences.getSharedPreferences(
					getApplicationContext(), Admin.SESSION_STATUS)
					.equalsIgnoreCase(Admin.SESSION_START)) {

				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.content_frame,
								IncidentReport.newInstance(),
								IncidentReport.TAG).commit();
			}else{
				getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.content_frame, DashboardNew.newInstance(),
						DashboardNew.TAG).commit();
			}
			break;
		case 5:
			 if (MySharedPreferences.getSharedPreferences(
					getApplicationContext(), Admin.SESSION_STATUS)
					.equalsIgnoreCase(Admin.SESSION_START)) {
				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.content_frame, DailyLog.newInstance(),
								DailyLog.TAG).commit();
			}else{
				getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.content_frame, DashboardNew.newInstance(),
						DashboardNew.TAG).commit();
			}
			break;
		case 3:
			SharedPreferences settings = getSharedPreferences(Admin.MYSECRATE,
					0);
			String punchStatus=settings.getString(Admin.PUNCH_STATUS,"");
			if(punchStatus.equalsIgnoreCase("1")) {
				logoutDialog();
			}else{
				settings.edit().clear().commit();
				MySharedPreferences.getInstance(SlideMenu.this).saveNotificationList(new ArrayList<NotificationBean>());
				startActivity(new Intent(this, MainActivity.class));
				finish();
			}

			break;
		}
		setTitle(mDrawerItmes[position].toUpperCase(Locale.getDefault()));
		mDrawerLayout.closeDrawer(relativeLayout);
	}


	public void logoutDialog(){
		AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(this);
		localBuilder1.setTitle("Alert");
		localBuilder1.setMessage("Please End Session!");
		/*localBuilder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {
				paramAnonymous2DialogInterface.dismiss();
			}
		});*/
		localBuilder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {

			}
		});
		localBuilder1.create().show();
	}

	@Override
	public void setTitle(CharSequence menu_title) {
		menu_title = menu_title.toString().trim();
		if (menu_title.length() == 0) {
			this.mTitle = "DASHBOARD";
		} else {
			this.mTitle = menu_title;
		}
		this.title.setText(this.mTitle);
		// getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Admin.REQUEST_CODE_LOG_OUT||requestCode == 101||requestCode == 1888||requestCode == 11111||requestCode == 1800||requestCode == 9090) {
			if (resultCode == RESULT_OK) {
				for (Fragment fragment : getSupportFragmentManager()
						.getFragments()) {
					if(fragment!=null) {
						fragment.onActivityResult(requestCode, resultCode, data);
					}
				}
			}
		}else if (resultCode == RESULT_OK) {
			if (ParkingViolation.newInstanceWithCurrent() != null) {
				for (Fragment fragment : getSupportFragmentManager()
						.getFragments()) {
					if (fragment != null) {
						fragment.onActivityResult(requestCode, resultCode, data);
					}
				}
			}
		}
	}
	public static void setActiveFragment(FragmentManager fragmentManager, Fragment fragment, String tag) {
		try {
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, tag).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.b2infosoft.gt.Admin;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.b2infosoft.gt.rajesh.modals.NotificationBean;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MySharedPreferences {
    private static final String SHARED_PREF_NAME = "FCMSharedPref";
    private static final String TAG_TOKEN = "tagtoken";

    private static final String USER_CREDENTIALS="rajesh";
    private static MySharedPreferences mInstance;
    private static Context mCtx;

    public MySharedPreferences(Context context) {
        mCtx = context;
    }

    public static synchronized MySharedPreferences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySharedPreferences(context);
        }
        return mInstance;
    }


    public static void setSharedPreferences(Context context,String key,String value){
        SharedPreferences setting= context.getSharedPreferences(USER_CREDENTIALS,0);
        setting.edit()
                .putString(key,value)
                .commit();
    }
    public static String getSharedPreferences(Context context,String key){
        SharedPreferences setting= context.getSharedPreferences(USER_CREDENTIALS,0);
        return setting.getString(key,"");
    }
    public static void logOut(Context context){
        SharedPreferences setting= context.getSharedPreferences(USER_CREDENTIALS,0);
        setting.edit()
                .clear()
                .commit();
    }

    //this method will save the device token to shared preferences
    public boolean saveDeviceToken(String token){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TAG_TOKEN, token);
        editor.apply();
        return true;
    }


    //this method will fetch the device token from shared preferences
    public String getCompanyName(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return  sharedPreferences.getString("CompanyName", null);
    } //this method will save the device token to shared preferences
    public boolean saveCompanyName(String token){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("CompanyName", token);
        editor.apply();
        return true;
    }


    //this method will fetch the device token from shared preferences
    public String getDeviceToken(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return  sharedPreferences.getString(TAG_TOKEN, null);
    }

    public static void saveNotificationList(ArrayList<NotificationBean> arrayList) {
        try {
            android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
            android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(arrayList);
            prefsEditor.putString("NotificationList", json);
            prefsEditor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static ArrayList<NotificationBean> getNotificationList() {

        try {
            android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
            android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
            Gson gson = new Gson();
            String json = mPrefs.getString("NotificationList", null);
            Type type = new TypeToken<ArrayList<NotificationBean>>() {}.getType();
            ArrayList<NotificationBean> obj = gson.fromJson(json, type);
            prefsEditor.commit();
            return obj;
        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }


}

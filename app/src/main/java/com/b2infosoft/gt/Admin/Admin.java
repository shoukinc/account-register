package com.b2infosoft.gt.Admin;
import java.util.Arrays;
import org.json.JSONArray;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
public class Admin {
	public static final String DATA = "data";
	public static final String URL = "url";
	public static final String SESSION_STATUS = "session_status";
	public static final String SESSION_START = "session_start";
	public static final String SESSION_BREAK = "session_break";
	public static final String SESSION_END = "session_end";
	public static final String SESSION_RESUME = "session_resume";
	public static final String SESSION_READY_START = "session_ready_start";
	public static final String ACTION="action";
	public static final String VEHICLE_NO="vehicle_no";	
	public static final String LAST_LATITUDE = "last_latitude";
	public static final String LAST_LONGITUDE = "last_longitude";
	public static final int REQUEST_CODE_LOG_OUT = 1;
	public static final String COMPANY_ID = "company_id";
	public static final String CLIENT_ID = "client_id";
	public static final String USER_ID = "user_id";
	public static final String FORM_ID = "form_id";
	public static final String CLIENT_DATE = "client_date";
	public static final String CLIENT_NAME = "client_name";
	public static final String TAG_SUCCESS = "success";
	public static final String CLIENT = "client";
	public static final String USER = "admin";
	public static final String LOGIN = "success";
	public static final String EMP_FIRST_NAME = "emp_first_name";
	public static final String EMP_LAST_NAME = "emp_last_name";
	public static final String EMP_PROFILE_PIC = "emp_profile_pic";
	public static final String USER_INFO = "userinfo";
	public static boolean isClient=false;
	public static final String SCHEDULE_STATUS = "schedule";
	public static final String CLIENT_STATUS = "client";
	public static final String PUNCH_STATUS = "punch";
	// JSON Node names
	public static final String USER_NAME = "user_name";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String LOCATION = "location";
	public static final String CHECK_IN_STATUS = "check_in";
	public static final String CHECK_OUT_STATUS = "check_out";
	public static final String CHECK = "check";
	public static final String COMPANY_NAME = "company_name";
	public static final String MY_FONT = "fonts/Roboto-Regular.ttf";
		
	public static final String MAX_FORM_FIELD = "max_form_field";
	public static final String MAX_FORM_FIELD_1 = "max_form_field_1";
	public static final String TIME_NEW = "new_time";
	public static final String TIME_OLD = "old_time";
	public static final String TIME_DEFAULT_NEW = "0000-00-00 00:00:00";
	public static final String TIME_DEFAULT_OLD = "00:00:00";
	
	public static final String LOG = "log";
	public static final String LOG_DATE = "date";
	public static final String LOG_YEAR = "year";
	public static final String LOG_MONTH = "month";
	public static final String LOG_MONTH_ARRAY = "month_array";
	public static final String SCHEDULE = "schedule";

	public static final String LOG_FROM_TO = "from_to";
	public static final String LOG_TOTAL_TIME = "total_time";
	public static final String PUNCH_OUT_STATUS = "check_out_status";

	public static final String PUNCH_OUT_TAKE_SESSION_START = "1";
	public static final String PUNCH_OUT_TAKE_BREAK = "2";
	public static final String PUNCH_OUT_TAKE_SESSION_END = "3";

	public static final String GUARD_ID = "guard_id";
	public static final String STATUS = "status";
	public static final String VEHICLE_NUMBER = "vehicle_number";
	public static final String USER_ACTION = "user_action";

	/* Form Fields Type */
	public static final String HEADING = "heading";
	public static final String TEXTFIELD = "textfield";
	public static final String TEXT_IMAGE = "image";
	public static final String TEXT_AUDIO = "audio";
	public static final String TEXT_VIDEO = "video";
	public static final String TEXTAREA = "textarea";
	public static final String SELECT = "select";
	public static final String CHECKBOX = "checkbox";
	public static final String RADIO = "radio";
	public static final String MYSECRATE = "rajesh";
	public static final String DATE = "date";
	public static final String NAME = "name";
	public static final String START = "start";
	public static final String END = "end";
	public static final String CONTACT = "contact";
	public static final String ADDRESS = "address";
	public static final String FORM_LEFT="left_form";
	public static final String FORM_RIGHT="right_form";

	public static final String TOTAL_TIME = "total_time";

/*
	public final static String EMPLOYEE_PROFILE_ADDRESS = "http://192.168.0.55/security_guard2/images/employee_profile/";
	public final static String SERVER_ADDRESS = "http://192.168.0.55/security_guard2/android_services/";
*/

/*
	public final static String EMPLOYEE_PROFILE_ADDRESS="http://b2infosoft.in/guardtab/images/employee_profile/";
	public final static String SERVER_ADDRESS="http://b2infosoft.in/guardtab/android_services/";
*/

	public final static String EMPLOYEE_PROFILE_ADDRESS="http://guardtab.com/images/employee_profile/";
	public final static String SERVER_ADDRESS="http://guardtab.com/android_services/";

	//public final static String SERVER_ADDRESS="http://192.168.0.55/rajesh/security_guard/";
	//final static String SERVER_ADDRESS="http://10.0.2.2/rajesh/security_guard/";

	public static String MONTHS[] = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
			"JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
	public static int getMonthNumber(String month) {
		return Arrays.asList(Admin.MONTHS).indexOf(month) + 1;
	}
	public static String getMonthName(int month) {
		return Arrays.asList(Admin.MONTHS).get(month - 1);
	}
	public static String[] getMonths(int months) {
		String arr[] = new String[months];
		for (int i = 0; i < months; i++) {
			arr[i] = MONTHS[i];
		}
		return null;
	}
	public static String[] getStringArray(JSONArray jsonArray) {
		String[] stringArray = null;
		int length = jsonArray.length();
		if (jsonArray != null) {
			stringArray = new String[length];
			for (int i = 0; i < length; i++) {
				stringArray[i] = jsonArray.optString(i);
			}
		}
		return stringArray;
	}
	public static String getUrlForGetAllClient() {
		return SERVER_ADDRESS.concat("GetCompanyClient.php");
	}
	public static String getUrlForChack_In_Status() {
		return SERVER_ADDRESS.concat("Background.php");
	}
	public static String getUrlForChack_In_Status_1() {
		return SERVER_ADDRESS.concat("Background_1.php");
	}
	public static String getUrlForCheck_In_Out() {
		return SERVER_ADDRESS.concat("employee_attendance.php");
	}
	public static String getUrlForUserRegistration() {
		return SERVER_ADDRESS.concat("LoginUser.php");
	}
	public static String getUrlForUserCompany() {
		return SERVER_ADDRESS.concat("GetCompanyName.php");
	}
	public static String getUrlForParkingViolationForm() {
		return SERVER_ADDRESS.concat("GetViolationForm.php");
//		return SERVER_ADDRESS.concat("get_all_forms.php");
	}
	public static String getUrlForParkingViolationFormFill() {
		return SERVER_ADDRESS.concat("FillViolationFormDSR.php");
	}
	public static String getUrlForParkingIncidentForm() {
		return SERVER_ADDRESS.concat("GetIncidentReportForm.php");
	}
	public static String getUrlForParkingIncidentFormFill() {
		return SERVER_ADDRESS.concat("FillIncidentReportForm.php");
	}
	public static String getUrlForParkingDailyForm() {
		return SERVER_ADDRESS.concat("GetDailyReportForm.php");
	}
	public static String getUrlForParkingDailyFormFill() {
		return SERVER_ADDRESS.concat("FillDailyReportForm.php");
	}
	public static String getUrlForParkingDailyFormFill_1() {
		return SERVER_ADDRESS.concat("FillDailyReportForm_1.php");
	}
	public static String getUrlForTimeUpdate() {
		return SERVER_ADDRESS.concat("TimeUpdates.php");
	}
	public static String getUrlForLogDailyInfo() {
		return SERVER_ADDRESS.concat("GetLogReport.php");
	}
	public static String getUrlForLogMonthInfo() {
		return SERVER_ADDRESS.concat("GetMonthReport.php");
	}
	public static String getUrlForLogYearInfo() {
		return SERVER_ADDRESS.concat("GetYearReport.php");
	}
	public static String getUrlForLogYearMonth() {
		return SERVER_ADDRESS.concat("GetYearMonth.php");
	}
	public static String getUrlForLogYearMonthDetails() {
		return SERVER_ADDRESS.concat("GetDetailsMonth.php");
	}
	public static String getUrlForParkingLog() {
		return SERVER_ADDRESS.concat("Parking_log.php");
	}
	public static String getUrlForDailyLog() {
		return SERVER_ADDRESS.concat("daily_log.php");
	}
	public static String getUrlForUploadSign() {
		return SERVER_ADDRESS.concat("UploadSign.php");
	}
	public static String getUrlForSchedule() {
		return SERVER_ADDRESS.concat("emp_schedule.php");
	}
	public static String getUrlForScheduleToday() {
		return SERVER_ADDRESS.concat("emp_today_schedule.php");
	}
	public static String getUrlForLocationUpdate() {
		return SERVER_ADDRESS.concat("emp_movement.php");
	}
	public static String getUrlForTotalTime() {
		return SERVER_ADDRESS.concat("timer.php");
	}
	public static Typeface getFontRoboto(Context context) {
		return Typeface.createFromAsset(context.getAssets(), MY_FONT);
	}
	public static String getDateTodayString() {
		java.sql.Date date = new java.sql.Date(new java.util.Date().getTime());
		return String.valueOf(date);
	}


	public static String getUrlForEmpStatus()
	{
		return SERVER_ADDRESS.concat("emp_status.php");
	}
	public static String getUrlForChooseClient()
	{
		return SERVER_ADDRESS.concat("choose_client.php");
	}
	public static String getUrlForDynamicFormItem() {
		return SERVER_ADDRESS.concat("get_all_forms.php");
	}
	public static String getUrlForDynamicTwoColoum() {
		return SERVER_ADDRESS.concat("create_form_two_column.php");
	}
	public static String getUrlForDynamicMultiColoum() {
		return SERVER_ADDRESS.concat("create_form_multi_column.php");
	}
	public static String getUrlForDynamicFillTwoColumnData() {
		return SERVER_ADDRESS.concat("FillTwoColumnData.php");
	}
	public static String getUrlForDynamicFillMultiColumnForm() {
		return SERVER_ADDRESS.concat("FillMultiColumnForm.php");
	}
	public static String submitSignOnNotification() {
		return SERVER_ADDRESS.concat("fill-notification.php");
	}
}
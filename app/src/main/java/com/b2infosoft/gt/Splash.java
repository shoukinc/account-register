package com.b2infosoft.gt;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.rajesh.fragments.Test;

public class Splash extends ActionBarActivity {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash);

        boolean suu = Admin.LOGIN.equals(MySharedPreferences.getSharedPreferences(getApplicationContext(), Admin.USER));
        if (suu) {
            finish();
            return;
        }

        zoom_in();



       /* FragmentManager fg = getSupportFragmentManager();
        FragmentTransaction ft = fg.beginTransaction();
        ft.add(R.id.FrameLayout1, new Test(), "Test");
        ft.commit();*/

        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {

                }
                boolean suu = Admin.LOGIN.equals(MySharedPreferences.getSharedPreferences(getApplicationContext(), Admin.USER));
                Intent intent = null;
                if (suu) {
                    intent = new Intent(Splash.this, SlideMenu.class);
                } else {
                    intent = new Intent(Splash.this, MainActivity.class);
                }
                if (intent != null)
                    startActivity(intent);
                finish();
            }
        });
        th.start();
    }

    public void zoom_in() {
        ImageView image = (ImageView) findViewById(R.id.imageView2);
        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        image.startAnimation(animation1);
    }


}

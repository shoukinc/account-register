package com.b2infosoft.gt.gps;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Network {
	private  Context context;
	public  Network(Context context) {
		this.context=context;		
	}
	public int checkNetworkConnection(){
	/*	1	Connecting
		2	Disconnected
		3	Disconnecting
		4	Suspended
		5	Unknown
	*/
		return 0;
	}
	public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
          if (connectivity != null) 
          {
              NetworkInfo[] info = connectivity.getAllNetworkInfo();
              if (info != null) 
                  for (int i = 0; i < info.length; i++) 
                      if (info[i].getState() == NetworkInfo.State.CONNECTED)
                      {
                          return true;
                      } 
          }
          return false;
    }
}

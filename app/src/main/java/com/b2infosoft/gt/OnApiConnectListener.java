package com.b2infosoft.gt;

/**
 * Created by Kartik Sharma @ B.R. Softech on 21/3/16.
 */
public interface OnApiConnectListener {
    void onSuccess(String jsonString);

    void onFailure(String message);
}

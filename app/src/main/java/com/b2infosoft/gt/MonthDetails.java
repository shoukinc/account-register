package com.b2infosoft.gt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.b2infosoft.gt.Admin.Admin;
import com.b2infosoft.gt.server.JSONParser;
import com.b2infosoft.gt.rajesh.fragments.LogReport2;

import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MonthDetails extends ActionBarActivity {
	private SharedPreferences settings;
	public static final String TAG=LogReport2.class.getSimpleName();
	private final	JSONParser jsonParser = new JSONParser();
	private JSONArray jsonArray;
	private TableLayout tableLayout;
	private TextView textYear;
	private ImageButton button_year_next,button_year_previous;
	private TableLayout.LayoutParams layout_params_table_row;
	
	private int SELECT_YEAR;
	private int SELECT_MONTH;
	private String []foundMonths;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_month_details);
		//getActionBar().setDisplayHomeAsUpEnabled(true);
		Intent intent=getIntent();		
		Bundle bundle =	intent.getExtras();
		this.SELECT_YEAR=bundle.getInt(Admin.LOG_YEAR);
		this.SELECT_MONTH=bundle.getInt(Admin.LOG_MONTH);
		this.foundMonths=bundle.getStringArray(Admin.LOG_MONTH_ARRAY);
		settings=getSharedPreferences(Admin.MYSECRATE,0);
		tableLayout=(TableLayout)findViewById(R.id.log_month_table_1);			
		layout_params_table_row=new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);			

		textYear=(TextView)findViewById(R.id.month_selected_1);
		textYear.setTypeface(Admin.getFontRoboto(this), Typeface.BOLD);
	 	button_year_next=(ImageButton)findViewById(R.id.month_next_1);
	 	button_year_next.setOnClickListener(new View.OnClickListener() {				
			@Override
			public void onClick(View v) {
				if(Arrays.asList(foundMonths).contains(String.valueOf(SELECT_MONTH))){						
					int index=Arrays.asList(foundMonths).indexOf(String.valueOf(SELECT_MONTH));							
					if(0<=index&&index<foundMonths.length){							
						new GetMonthLogs().execute(new String[]{		 			
								foundMonths[index+1],
					 			String.valueOf(SELECT_YEAR)		 					 			
					 	});
					 	updateMonth(String.valueOf(SELECT_YEAR), foundMonths[index+1]);							
					}
				}
			}
		});
	 	button_year_previous=(ImageButton)findViewById(R.id.month_previous_1);
	 	button_year_previous.setOnClickListener(new View.OnClickListener() {				
			@Override
			public void onClick(View v) {
				if(Arrays.asList(foundMonths).contains(String.valueOf(SELECT_MONTH))){						
					int index=Arrays.asList(foundMonths).indexOf(String.valueOf(SELECT_MONTH));							
					if(0 < index && index<=foundMonths.length){							
						new GetMonthLogs().execute(new String[]{		 			
								foundMonths[index-1],
					 			String.valueOf(SELECT_YEAR)		 					 			
					 	});
					 	updateMonth(String.valueOf(SELECT_YEAR), foundMonths[index-1]);							
					}
				}					
			}
		});
	 	new GetMonthLogs().execute(new String[]{		 			
	 			String.valueOf(SELECT_MONTH),
	 			String.valueOf(SELECT_YEAR)		 					 			
	 	});
	 	updateMonth(String.valueOf(SELECT_YEAR), String.valueOf(SELECT_MONTH));
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private void updateMonth(String year,String month){	
		Log.d("Update", year+" : "+month);
		SELECT_YEAR=Integer.parseInt(year);
		SELECT_MONTH=Integer.parseInt(month);
		textYear.setText(Admin.getMonthName(SELECT_MONTH)+" "+String.valueOf(SELECT_YEAR));
		if(Arrays.asList(foundMonths).contains(String.valueOf(SELECT_MONTH))){
			int index=Arrays.asList(foundMonths).indexOf(String.valueOf(SELECT_MONTH));
			if(index==0){
				button_year_previous.setEnabled(false);
			}else{
				button_year_previous.setEnabled(true);
			}
			if(index<foundMonths.length-1){
				button_year_next.setEnabled(true);
			}else{
				button_year_next.setEnabled(false);
			}
		}
	}
	private void updateLog(JSONArray jsonArray){			
		if(tableLayout.getChildCount()>0){
			tableLayout.removeAllViews();
		}		
		for(int i=0;i<jsonArray.length();i++ ){			
			try{
				JSONObject c = jsonArray.getJSONObject(i);			
				TableRow tr_row = new TableRow(this);
				tr_row.setLayoutParams(layout_params_table_row);										
				tr_row.setPadding(0, 10, 0, 10);												
				if((i%2)==0)
					tr_row.setBackgroundResource(R.color.bg_log_row_odd);
				else
					tr_row.setBackgroundResource(R.color.bg_log_row_even);										
				TextView	text=new TextView(this);	
				text.setText(c.getString(Admin.LOG_DATE)+"\n"+c.getString(Admin.LOG_FROM_TO));
				text.setGravity(Gravity.CENTER_HORIZONTAL);				
				text.setTypeface(Admin.getFontRoboto(this));																				
				TextView text1=new TextView(this);										
				
				text1.setText(c.getString(Admin.LOG_TOTAL_TIME));		
				text1.setGravity(Gravity.CENTER_HORIZONTAL);
				text1.setTypeface(Admin.getFontRoboto(this));																
							
				tr_row.addView(text);
				tr_row.addView(text1);				
				tableLayout.addView(tr_row);							
				
			}catch(Exception e){
				e.printStackTrace();
				Log.d("Exception", e.toString());
			}		
		}
	}
	private class GetMonthLogs extends AsyncTask<String, String, String>{
		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> pairs=new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair(Admin.USER_ID, settings.getString(Admin.USER_ID, "")));
			pairs.add(new BasicNameValuePair(Admin.LOG_MONTH, params[0]));
			pairs.add(new BasicNameValuePair(Admin.LOG_YEAR, params[1]));
			final JSONObject jsonObject = jsonParser.makeHttpRequest(Admin.getUrlForLogYearMonthDetails(),"POST",pairs);						
			try{
				int success=0;
				JSONArray jsonArray=null;
				if(jsonObject.has(Admin.TAG_SUCCESS))
					success=jsonObject.getInt(Admin.TAG_SUCCESS);
				if(success==1){					
					if(jsonObject.has(Admin.LOG))
						jsonArray =jsonObject.getJSONArray(Admin.LOG);					
					if(jsonArray!=null){						
						MonthDetails.this.jsonArray = jsonArray;
					}
				}
			}catch(JSONException e){
				
			}			
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(MonthDetails.this.jsonArray!=null)
			{
				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						updateLog(MonthDetails.this.jsonArray);						
					}
				});
			}
		}
	}	

}

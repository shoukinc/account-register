package com.b2infosoft.gt;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.b2infosoft.gt.Admin.MySharedPreferences;
import com.b2infosoft.gt.rajesh.modals.NotificationBean;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mahesh on 1/5/17.
 */

public class NotificationPage extends ActionBarActivity {


    private ListView mListView;
    private static NotificationPage notificationPage;

    ArrayList<NotificationBean> beanArrayList;


    public static NotificationPage getInstance() {

        return notificationPage;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();
        notificationPage = this;
        setContentView(R.layout.notification_page);
        mListView = (ListView) findViewById(R.id.list);
        beanArrayList = MySharedPreferences.getInstance(NotificationPage.this).getNotificationList();
        if (beanArrayList != null) {
            PnAdapter adapter = new PnAdapter(this);
            mListView.setAdapter(adapter);
        }
        MySharedPreferences.getInstance(this).saveNotificationList(new ArrayList<NotificationBean>());
    }



    class PnAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater inflater;

        public  PnAdapter(Context context){
            this.context=context;
            inflater=getLayoutInflater();
        }

        @Override
        public int getCount() {
            return beanArrayList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view1, ViewGroup viewGroup) {

            View view = view1;
            ViewHolder holder = null;
            if (view == null) {

                view = inflater.inflate(R.layout.row_notification_item, null);
                holder = new ViewHolder();
                holder.mFullImage = (ImageView) view.findViewById(R.id.mFullImage);
                holder.mTitle = (TextView) view.findViewById(R.id.mTitle);
                holder.mMessage = (TextView) view.findViewById(R.id.mMessage);

                view.setTag(R.layout.row_notification_item, holder);
            } else {
                holder = (ViewHolder) view.getTag(R.layout.row_notification_item);
            }

            NotificationBean bean=beanArrayList.get(i);
            holder.mTitle.setText(bean.getTitle());
            holder.mMessage.setText(bean.getMessage());
            if(!TextUtils.isEmpty(bean.getImage_url())) {
                try {
                    Picasso.with(NotificationPage.this)
                            .load(bean.getImage_url())
                            .placeholder(R.drawable.menu_schedule)
                            .into(holder.mFullImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                holder.mFullImage.setVisibility(View.GONE);
            }


            return view;
        }

        class ViewHolder {
            ImageView mFullImage;
            TextView mTitle;
            TextView mMessage;
        }
    }
}
